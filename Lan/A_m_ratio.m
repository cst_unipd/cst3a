%Plot A/m ratio
log10Am=Y;
log10Lc=X;
Am=10.^Y;
Lc=10.^X;
plot(Lc,Am,'+b')
grid on
hold on
title('A/m comparison')
xlabel('L_c [m]');
ylabel('A/m [m^2/kg]')