function[ME,FRAGMENTS,link_data]=scenario_default
% Scenario_default provides default scenario of ME, FRAGMENTS and link_data


ME=generate_ME_simple_plate; 
FRAGMENTS=generate_ME_sphere(1);
link_data=[];