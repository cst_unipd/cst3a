% Example file for testing/implementing the xml and ump files parser

% Note for XML Parser: The Data Structures regarding the link geometry and Seed distribution are
% neglected at this point since, on the one hand, the link geometry is not
% handled by the GUI for SW1 and, on the other hand, there is currently no databank
% regarding the properties of link geometries and seed distributions etc.

clc 
clear all

% The MATERIAL database as well as the ME objects are generated and stored as
% global variables whithin the function 'get_ME_Objs_From_Interface'. Thus they can be accessed 
% as global variables as applied in the 1st version of CST Solver as well 

global MATERIAL_LIST

global ME

%define the correct file path at will
xmlFilePath='Interface Files\CstDatabase_v3.xml';
dumpFilePath='Interface Files\CST_Data_Structure_Dump_01.dump';

% call the main function to read and provide the xml database content as
% well as to read the dump file and to construct the ME objects
get_ME_Objs_From_Interface(xmlFilePath,dumpFilePath);

% NOTE: The parser will convert the units and provide them as :
% 	Length is in (m)
% 	Velocity is in km/s
% 	Mass is in kg
% 	Density is in kg/m^3

% NOTE: Values in the current dump file are just 'random' examples meant for testing
% until the interface is further clarified between etamax and CISAS and some
% default values are define for parameters.




% Example of accessing the data
disp('----------------------------')
disp('Examples for accessing Data')
disp('----------------------------')

disp(['Mat ID: '            num2str(    MATERIAL_LIST(1).mat_id             )])
disp(['Mat name: ',                     MATERIAL_LIST(1).name                ])
disp(['Mat density: ',      num2str(    MATERIAL_LIST(1).density            )])
disp(['Mat young: ',        num2str(    MATERIAL_LIST(1).young              )])

disp(['ME ID: ',            num2str(    ME(1).object_ID                     )])
disp(['ME mat ID: ',        num2str(    ME(1).material_ID                   )])
disp(['ME mass0: ',         num2str(    ME(1).GEOMETRY_DATA.mass0           )])
disp(['ME c_expl: ',        num2str(    ME(1).FRAGMENTATION_DATA.c_EXPL     )])
disp(['ME threshold0: ',    num2str(    ME(1).FRAGMENTATION_DATA.threshold0 )])

% clear workspace
clear xmlFilePath dumpFilePath ME_LIST
