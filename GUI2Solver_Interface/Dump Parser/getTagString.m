% get tag string and tag length from the given string
function [tagString, tagLength]=getTagString(inputString)

 % get position of white space in the string
    whiteSpacePosition=strfind(inputString,' ');
    
    if isempty(whiteSpacePosition) 
        % if no white space found, the entire string represents the tag
        tagLength=length(inputString);
    else
        % everything before the first white space is the tag string
        tagLength=whiteSpacePosition(1)-1;
    end
    
    % get tag string
    tagString=inputString(1:tagLength);
    
   