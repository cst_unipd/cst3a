% get shape ID based on given shape name
function shapeID=getShapeIdFromName_old(shapeName)

switch shapeName
    
    case 'Box'
        
        shapeID=1;
        
    case 'Cylinder'
        
        shapeID=4;
        
    case 'Sphere'
    
        shapeID=2;
end