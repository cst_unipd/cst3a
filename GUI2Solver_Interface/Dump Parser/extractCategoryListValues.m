% Read ME values from the dump file
function extractCategoryListValues(categoryString, dumpCell)
                                  
% general list parameters
global  ME_GENERAL_PARAMETERS
global  GEOMETRY_GENERAL_PARAMETERS      
global  DYNAMICS_INT_GENERAL_PARAMETERS  
global  DYNAMICS_GENERAL_PARAMETERS       
global  FRAGMENTATION_GENERAL_PARAMETERS

% general list values
global ME_LIST
global GEOMETRY_LIST
global DYNAMIC_INT_LIST
global DYNAMIC_LIST
global FRAGMENTATION_LIST

% based on the given category, read the values from the dumpCell
switch categoryString
    
    %----------------------------------------
    %           < Extract ME Category >
    %----------------------------------------
    case 'ME_Category'
        
        nrOfItems=ME_GENERAL_PARAMETERS.nrOfItems;
        
        % initialise the structure for all items with value 0. In this
        % case, if an attribute is missing in DumpFile it will be 0.
        k=1;
        while (k<=nrOfItems)
        ME_LIST(k).me_id=0;    
        ME_LIST(k).me_name=0;
        ME_LIST(k).geom_data_id=0;
        ME_LIST(k).material_id=0;
        ME_LIST(k).dyn_init_data_id=0;
        ME_LIST(k).dyn_init_data_id=0;
        ME_LIST(k).frag_data_id=0;      
        k=k+1;
        end
   
        i=ME_GENERAL_PARAMETERS.startingLine+1;
        i_end=ME_GENERAL_PARAMETERS.endingLine;
        
        item_ID=0;
        % loop over ME List
        while i<=i_end
            % get the OCC Tag String and Tag length from the dumpCell storing the Tag 
            [tagString, ~]=getTagString(dumpCell{1, 1}{i, 1});
            
            % get number of ':' in the Tag
            dblPntPosition=strfind(tagString,':');
            dblPntQuantity=length(dblPntPosition);
            
            if dblPntQuantity==3
                % the item ID is always stored in a Tag which displays 4
                % levels i.e. 3 times ':'
                
                item_ID=item_ID+1;
                
                % item ID is stored in the Tag from the last ':' until end
                % of the tag string
                ME_LIST(item_ID).me_id=str2doubleNaN(tagString(dblPntPosition(end)+1:end));

            elseif dblPntQuantity==4
                
                % if Tag has 5 levels (i.e. 4 time ':') it shows the attributes 
                attributeTag=tagString(dblPntPosition(end)+1:end);
                
                switch attributeTag
                    % start allocating values to the corresponding
                    % attributes based on attribute tag id
                    case '1'
                        ME_LIST(item_ID).me_name=dumpCell{1, 2}{i, 1};                        
                    case '2'
                        ME_LIST(item_ID).geom_data_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '3'
                        ME_LIST(item_ID).material_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '4'
                        ME_LIST(item_ID).dyn_init_data_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '5'
                        ME_LIST(item_ID).dyn_data_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '6'
                        ME_LIST(item_ID).frag_data_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    
                end % end of switch case 'attributeTag'
                
                          
                
            else
                disp('Error during importing ME from DumpFile');
                stop
                
            end
           
            i=i+1;
        end % end of ME List loop
    
        
    %----------------------------------------
    %           < Extract Geometry Category >
    %----------------------------------------    
    case 'Geometry_Category'

     nrOfItems=GEOMETRY_GENERAL_PARAMETERS.nrOfItems;
        
        % initialise the structure for all items with value 0. In this
        % case, if an attribute is missing in DumpFile it will be 0.
        k=1;
        while (k<=nrOfItems)
        GEOMETRY_LIST(k).geom_data_id=0;    
        GEOMETRY_LIST(k).geom_data_name=0;
        GEOMETRY_LIST(k).shape_name=0;
        GEOMETRY_LIST(k).dimensions=[0,0,0];
        GEOMETRY_LIST(k).thick=0;
        GEOMETRY_LIST(k).mass0=0;
        GEOMETRY_LIST(k).mass=0;
        GEOMETRY_LIST(k).a_m_ratio=0;
        GEOMETRY_LIST(k).c_hull=[0,0,0];  
        k=k+1;
        end
   
        i=GEOMETRY_GENERAL_PARAMETERS.startingLine+1;
        i_end=GEOMETRY_GENERAL_PARAMETERS.endingLine;
        
        item_ID=0;
        % loop over Geometry List   
        while i<=i_end
            % get the OCC Tag String and Tag length from the dumpCell storing the Tag 
            [tagString, ~]=getTagString(dumpCell{1, 1}{i, 1});
            
            % get number of ':' in the Tag
            dblPntPosition=strfind(tagString,':');
            dblPntQuantity=length(dblPntPosition);
            
            if dblPntQuantity==3
                % the item ID is always stored in a Tag which displays 4
                % levels i.e. 3 times ':'
                
                item_ID=item_ID+1;
                
                % item ID is stored in the Tag from the last ':' until end
                % of the tag string
                GEOMETRY_LIST(item_ID).geom_data_id=str2doubleNaN(tagString(dblPntPosition(end)+1:end));

            elseif dblPntQuantity==4
                
                % if Tag has 5 levels (i.e. 4 time ':') it shows the attributes 
                attributeTag=tagString(dblPntPosition(end)+1:end);
                
                switch attributeTag
                    % start allocating values to the corresponding
                    % attributes based on attribute tag id
                    case '1'
                        GEOMETRY_LIST(item_ID).geom_data_name=dumpCell{1, 2}{i, 1};                        
                    case '2'
                        GEOMETRY_LIST(item_ID).shape_name=dumpCell{1, 2}{i, 1};
                    case '3'
                        GEOMETRY_LIST(item_ID).dimensions=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '4'
                        GEOMETRY_LIST(item_ID).thick=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '5'
                        GEOMETRY_LIST(item_ID).mass0=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '6'
                        GEOMETRY_LIST(item_ID).mass=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '7'
                        GEOMETRY_LIST(item_ID).a_m_ratio=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '8'
                        GEOMETRY_LIST(item_ID).c_hull=readArrayValue(dumpCell{1, 2}{i, 1});
                        
                end % end of switch case 'attributeTag' 
            else
                disp('Error during importing Geometry List from DumpFile');
                stop                
            end           
            i=i+1;
        end % end of geometry List loop
            
    %----------------------------------------
    %           < Extract Dynamics_Int Category >
    %----------------------------------------         
    case 'Dynamics_Int_Category'
     nrOfItems=DYNAMICS_INT_GENERAL_PARAMETERS.nrOfItems;
        
        % initialise the structure for all items with value 0. In this
        % case, if an attribute is missing in DumpFile it will be 0.
        k=1;
        while (k<=nrOfItems)
        DYNAMIC_INT_LIST(k).dyn_ini_data_id=0;    
        DYNAMIC_INT_LIST(k).dyn_ini_data_name=0;
        DYNAMIC_INT_LIST(k).cm_coord0=[0,0,0];
        DYNAMIC_INT_LIST(k).vel0=[0,0,0];
        DYNAMIC_INT_LIST(k).quaternions0=[0,0,0];
        DYNAMIC_INT_LIST(k).w0=[0,0,0];
        k=k+1;
        end
   
        i=DYNAMICS_INT_GENERAL_PARAMETERS.startingLine+1;
        i_end=DYNAMICS_INT_GENERAL_PARAMETERS.endingLine;
        
        item_ID=0;
        % loop over Geometry List   
        while i<=i_end
            % get the OCC Tag String and Tag length from the dumpCell storing the Tag 
            [tagString, ~]=getTagString(dumpCell{1, 1}{i, 1});
            
            % get number of ':' in the Tag
            dblPntPosition=strfind(tagString,':');
            dblPntQuantity=length(dblPntPosition);
            
            if dblPntQuantity==3
                % the item ID is always stored in a Tag which displays 4
                % levels i.e. 3 times ':'
                
                item_ID=item_ID+1;
                
                % item ID is stored in the Tag from the last ':' until end
                % of the tag string
                DYNAMIC_INT_LIST(item_ID).dyn_ini_data_id=str2doubleNaN(tagString(dblPntPosition(end)+1:end));

            elseif dblPntQuantity==4
                
                % if Tag has 5 levels (i.e. 4 time ':') it shows the attributes 
                attributeTag=tagString(dblPntPosition(end)+1:end);
                
                switch attributeTag
                    % start allocating values to the corresponding
                    % attributes based on attribute tag id
                    case '1'
                        DYNAMIC_INT_LIST(item_ID).dyn_ini_data_name=dumpCell{1, 2}{i, 1};                        
                    case '2'
                        DYNAMIC_INT_LIST(item_ID).cm_coord0=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '3'
                        DYNAMIC_INT_LIST(item_ID).vel0=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '4'
                        DYNAMIC_INT_LIST(item_ID).quaternions0=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '5'
                        DYNAMIC_INT_LIST(item_ID).w0=readArrayValue(dumpCell{1, 2}{i, 1});
                        
                end % end of switch case 'attributeTag' 
            else
                disp('Error during importing Dynamic Initial List from DumpFile');
                stop                
            end           
            i=i+1;
        end % end of dynamic int List loop        

        
    %----------------------------------------
    %           < Extract Dynamics_Int Category >
    %----------------------------------------         
    case 'Dynamics_Category'
     nrOfItems=DYNAMICS_GENERAL_PARAMETERS.nrOfItems;
        
        % initialise the structure for all items with value 0. In this
        % case, if an attribute is missing in DumpFile it will be 0.
        k=1;
        while (k<=nrOfItems)
        DYNAMIC_LIST(k).dyn_data_id=0;    
        DYNAMIC_LIST(k).dyn_data_name=0;
        DYNAMIC_LIST(k).cm_coord=[0,0,0];
        DYNAMIC_LIST(k).vel=[0,0,0];
        DYNAMIC_LIST(k).quaternions=[0,0,0];
        DYNAMIC_LIST(k).w=[0,0,0];
        DYNAMIC_LIST(k).virt_momentum=[0,0,0];
        k=k+1;
        end
   
        i=DYNAMICS_GENERAL_PARAMETERS.startingLine+1;
        i_end=DYNAMICS_GENERAL_PARAMETERS.endingLine;
        
        item_ID=0;
        % loop over Geometry List   
        while i<=i_end
            % get the OCC Tag String and Tag length from the dumpCell storing the Tag 
            [tagString, ~]=getTagString(dumpCell{1, 1}{i, 1});
            
            % get number of ':' in the Tag
            dblPntPosition=strfind(tagString,':');
            dblPntQuantity=length(dblPntPosition);
            
            if dblPntQuantity==3
                % the item ID is always stored in a Tag which displays 4
                % levels i.e. 3 times ':'
                
                item_ID=item_ID+1;
                
                % item ID is stored in the Tag from the last ':' until end
                % of the tag string
                DYNAMIC_LIST(item_ID).dyn_data_id=str2doubleNaN(tagString(dblPntPosition(end)+1:end));

            elseif dblPntQuantity==4
                
                % if Tag has 5 levels (i.e. 4 time ':') it shows the attributes 
                attributeTag=tagString(dblPntPosition(end)+1:end);
                
                switch attributeTag
                    % start allocating values to the corresponding
                    % attributes based on attribute tag id
                    case '1'
                        DYNAMIC_LIST(item_ID).dyn_data_name=dumpCell{1, 2}{i, 1};                        
                    case '2'
                        DYNAMIC_LIST(item_ID).cm_coord=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '3'
                        DYNAMIC_LIST(item_ID).vel=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '4'
                        DYNAMIC_LIST(item_ID).quaternions=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '5'
                        DYNAMIC_LIST(item_ID).w=readArrayValue(dumpCell{1, 2}{i, 1});
                    case '6'
                        DYNAMIC_LIST(item_ID).virt_momentum=readArrayValue(dumpCell{1, 2}{i, 1});    
                        
                end % end of switch case 'attributeTag' 
            else
                disp('Error during importing Dynamic List from DumpFile');
                stop                
            end           
            i=i+1;
        end % end of dynamic List loop            
        
    %----------------------------------------
    %           < Extract Fragmentation Category >
    %----------------------------------------          
    case 'Fragmentation_Category'

     nrOfItems=FRAGMENTATION_GENERAL_PARAMETERS.nrOfItems;
        
        % initialise the structure for all items with value 0. In this
        % case, if an attribute is missing in DumpFile it will be 0.
        k=1;
        while (k<=nrOfItems)
        FRAGMENTATION_LIST(k).frag_data_id=0;    
        FRAGMENTATION_LIST(k).frag_data_name=0;
        FRAGMENTATION_LIST(k).frag_fai_id=0;
        FRAGMENTATION_LIST(k).threshold0=0;
        FRAGMENTATION_LIST(k).threshold=0;
        FRAGMENTATION_LIST(k).breakup_flag=0;
        FRAGMENTATION_LIST(k).me_energy_tran_coef=0;
        FRAGMENTATION_LIST(k).cmloss=0;
        FRAGMENTATION_LIST(k).celoss=0;
        FRAGMENTATION_LIST(k).C_expl=0;
        FRAGMENTATION_LIST(k).seeds_dist_id=0;
        FRAGMENTATION_LIST(k).seeds_dist_param1=0;
        FRAGMENTATION_LIST(k).seeds_dist_param2=0;
        FRAGMENTATION_LIST(k).param_add1=0;
        FRAGMENTATION_LIST(k).param_add2=0;
        k=k+1;
        end
   
        i=FRAGMENTATION_GENERAL_PARAMETERS.startingLine+1;
        i_end=FRAGMENTATION_GENERAL_PARAMETERS.endingLine;
        
        item_ID=0;
        % loop over Geometry List   
        while i<=i_end
            % get the OCC Tag String and Tag length from the dumpCell storing the Tag 
            [tagString, ~]=getTagString(dumpCell{1, 1}{i, 1});
            
            % get number of ':' in the Tag
            dblPntPosition=strfind(tagString,':');
            dblPntQuantity=length(dblPntPosition);
            
            if dblPntQuantity==3
                % the item ID is always stored in a Tag which displays 4
                % levels i.e. 3 times ':'
                
                item_ID=item_ID+1;
                
                % item ID is stored in the Tag from the last ':' until end
                % of the tag string
                FRAGMENTATION_LIST(item_ID).frag_data_id=str2doubleNaN(tagString(dblPntPosition(end)+1:end));

            elseif dblPntQuantity==4
                
                % if Tag has 5 levels (i.e. 4 time ':') it shows the attributes 
                attributeTag=tagString(dblPntPosition(end)+1:end);
                
                switch attributeTag
                    % start allocating values to the corresponding
                    % attributes based on attribute tag id
                    case '1'
                        FRAGMENTATION_LIST(item_ID).frag_data_name=dumpCell{1, 2}{i, 1};                        
                    case '2'
                        FRAGMENTATION_LIST(item_ID).frag_fai_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '3'
                        FRAGMENTATION_LIST(item_ID).threshold0=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '4'
                        FRAGMENTATION_LIST(item_ID).threshold=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '5'
                        FRAGMENTATION_LIST(item_ID).breakup_flag=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '6'
                        FRAGMENTATION_LIST(item_ID).me_energy_tran_coef=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '7'
                        FRAGMENTATION_LIST(item_ID).cmloss=str2doubleNaN(dumpCell{1, 2}{i, 1});                        
                    case '8'
                        FRAGMENTATION_LIST(item_ID).celoss=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '9'
                        FRAGMENTATION_LIST(item_ID).C_expl=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '10'
                        FRAGMENTATION_LIST(item_ID).seeds_dist_id=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '11'
                        FRAGMENTATION_LIST(item_ID).seeds_dist_param1=str2doubleNaN(dumpCell{1, 2}{i, 1});
                    case '12'
                        FRAGMENTATION_LIST(item_ID).seeds_dist_param2=str2doubleNaN(dumpCell{1, 2}{i, 1});                         
                        
                end % end of switch case 'attributeTag' 
            else
                disp('Error during importing Fragmentation List from DumpFile');
                stop                
            end           
            i=i+1;
        end % end of fragment List loop           
        
    
end % end switch case 'categoryString'
 