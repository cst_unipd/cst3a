function P5_plot_Fragmentation_Statistics(Impact_Data,FragMatrix)

%%
global nishida_flag
global local_fitvalue
global projectile_diameter
global impact_velocity
global nishida_test_case

V = 4100; % m/s

nishida_flag = 1;
local_plot_flag = 1;

mass_vect = cell(1,Impact_Data.N_frag_dom);
CN_m = cell(1,Impact_Data.N_frag_dom);

for k=1:Impact_Data.N_frag_dom
    
    if nishida_flag==1
        mass_vect{k} = FragMatrix{k}(1:end,8);
    else
        mass_vect{k} = FragMatrix{k}(1:end,3);
    end
    
    mass_vect{k} = sort(mass_vect{k});
    
    ll = length(mass_vect{k});
    CN_m{k} = ll:-1:1; % it corresponds to the cumulative number
    
    if nishida_flag==1
        D = 0.0032;
        a_vect = linspace(0.5,5,100);
        
        switch nishida_test_case
            case 1
                CN_vect = 38.74*exp(-0.282*a_vect) + 408.3*exp(-1.667*a_vect);
            case 2
                CN_vect = 233.0*exp(-1.881*a_vect);
            case 3
                CN_vect = 148.8*exp(-3.499*a_vect);
            case 4
                CN_vect = 55.06*exp(-0.309*a_vect) + 781.6*exp(-1.338*a_vect);
            case 5
                CN_vect = 500.2*exp(-2.155*a_vect);
            case 6
                CN_vect = 148.8*exp(-3.499*a_vect);
            case 7
                CN_vect = 938.7*exp(-1.995*a_vect);
            case 8
                CN_vect = 148.8*exp(-3.499*a_vect);
            case 9
                CN_vect = 1729*exp(-2.267*a_vect);
            case 10
                CN_vect = 400.8*exp(-2.296*a_vect);
        end
        
        CN_vect_ = V^1.5*(151.6*exp(-10.6*a_vect/D) + 18.0*exp(-3.24*a_vect/D));
        
        if local_plot_flag == 1
            figure();
            loglog(mass_vect{k}*1e3,CN_m{k},'pr');hold on;
            if (nishida_test_case>=1) && (nishida_test_case<=10)
                loglog(a_vect,CN_vect,'*b');
            end
            loglog(a_vect,CN_vect_,'ok');
            %             loglog(a_vect_cst,CN_vect_cst,'pm');
            xlabel('Fragment size a [mm]');
            grid on;
            ylabel('Cumulative Number');
            title(['CN vs. m_f - D' num2str(k)]);
        end
    else
        figure();
        loglog(mass_vect{k},CN_m{k},'pr');
        xlabel('Fragment Mass [kg]');
        grid on;
        ylabel('Cumulative Number');
        title(['CN vs. m_f - D' num2str(k)]);
    end
    
    
%{      
  if nishida_flag==1
        %%
        a_vect_cst = mass_vect{k}*1e3;
        %CN_vect_cst = 500.2*exp(-2.155*a_vect_cst);
        CN_vect_cst = V^1.5*(151.6*exp(-10.6*a_vect_cst/D) + 18.0*exp(-3.24*a_vect_cst/D));
        CN_error = abs(CN_m{k}'-CN_vect_cst)./CN_vect_cst;
        idx_vect = find(CN_error<1e4);
        local_fitvalue = sum(CN_error(idx_vect));
%         local_fitvalue = sum(abs(CN_m{k}'-CN_vect_cst)./CN_vect_cst);
        if local_plot_flag ==1
            figure()
            %plot(mass_vect{k}*1e3,abs(CN_m{k}'-CN_vect_cst)./CN_vect_cst,'pr');
            plot(mass_vect{k}(idx_vect)*1e3,CN_error(idx_vect),'pr');
            grid on;
            ylabel('Error on Cumulative Number');
            xlabel('Fragment size a [mm]');
        end
    end
%}    
    
end

end
