% Standalone for fragments size distribution
% FRAG_DIST=[size cumulative_number]

uiopen('C:\Users\saregiu17613\Desktop\Nishida Simple Plate\TEST 7\CN_curve_Nishida_test_case_7.fig',1)

hold on

if exist('FRAGMENTS_FR','var')
    if length(FRAGMENTS_FR)>length(FRAGMENTS)
        FRAGMENTS=FRAGMENTS_FR;
        ME=ME_FR;
    end
end

%% DELETE FRAGMENTs & MEs WITH MASS == 0 AND DEFINE DIMENSIONS
n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS1=FRAGMENTS;

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS1(n_size+1:n_size+n_size1)=ME;
end

n_size=length(FRAGMENTS1);

%% CREATE Lc, AM RATIO AND VELOCITY DISTRIBUTIONS FROM CST RESULTS
s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST(i-1)=mean(FRAGMENTS1(i).GEOMETRY_DATA.dimensions);
    Am_CST(i-1)=((FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3))/3/FRAGMENTS1(i).GEOMETRY_DATA.mass);
    vel_CST_wp(i-1)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
    mass_CST(i-1)=FRAGMENTS1(i).GEOMETRY_DATA.mass;
    if FRAGMENTS1(i).object_ID~=66
       vel_CST_np(k)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
       k=k+1;
    end
end

%% PLOT FRAGMENT DISTRIBUTIONS CST
s=sort(Lc_CST);

if n_size==1 %patch to avoid only one fragment case (LO)
    n_size=n_size+1;
end

N=n_size-1:-1:1;

FRAG_DIST=[s' N'];

% h = figure('Position', [100, 50, 500, 500],'Visible', 'on');
loglog(s*1000,N,'+g','LineWidth',2)
grid on
hold on
xlim([0.5 3.5])

% xlim([0.5 3.5])
% legend('CST Nishida','Test data interpolation','Nishida generalized','CST random-Gauss') %to plot Nishida results
% legend('CST Nishida','Test data interpolation','Nishida generalized','CST Nishida fixed') %to plot Nishida results

%{
%% LOAD NISHIDA RESULTS (obtained from paper)
load('./Nishida/CFRP_Nishida_0.9.mat')
N_v=Y;
x_nishida=X.*10^-3;

figure
semilogy(x_nishida,N_v/0.9,'*r','LineWidth',2)
grid on
hold on
semilogy(sort(x),N/0.9,'ob','LineWidth',2)
title('Comparison Nishida-CST results CFRP 0.9 km/s')
xlabel('Max dimension [m]')
ylabel('Cumulative number N')
legend('CST simulation','Nishida experimental results')

load('./Nishida/CFRP_Nishida_Lc_0.9.mat')
Lc_nishida=X;
N_nishida=Y;

% [Lc_CSTs index]=sort(Lc_CST);
% ll=1;
% for i=1:1:length(Lc_CSTs)
%     mass_new(ll)=mass_CST(index(i));
%     ll=ll+1;
% end
tot_mass=sum(mass_CST);
N_NASA=0.1*tot_mass^0.75.*s.^-1.71;

figure
loglog(s,N,'ob','LineWidth',2)
grid on
hold on
loglog(Lc_nishida,N_nishida,'*r','LineWidth',2)
loglog(s,N_NASA,'k','LineWidth',2)
title('Comparison Nishida-CST results CFRP 0.9 km/s')
xlabel('L_c [m]')
ylabel('Cumulative number N')
legend('CST simulation','Nishida experimental results','NASA SBM')

%
%% LOAD LAN RESULTS (obtained from paper)
load('./Lan/LAN_A_m_ratio')
Am_LAN=10.^Y;
Lc_LAN=10.^X;

%LAN results
alpha1=0.0060;
sigma1=0.1270;
mu1=-1.180;
alpha2=0.0360;
sigma2=0.1050;
mu2=-0.695;
alpha3=0.0150;
sigma3=0.1450;
mu3=-0.295;
csi=log10(sort(Am_LAN));

P=alpha1*(1/(sigma1*(2*pi)^0.5))*exp(-((csi-mu1).^2)/(2*sigma1^2))+...
    alpha2*(1/(sigma2*(2*pi)^0.5))*exp(-((csi-mu2).^2)/(2*sigma2^2))+...
    alpha3*(1/(sigma3*(2*pi)^0.5))*exp(-((csi-mu3).^2)/(2*sigma3^2));

%CST results
alpha4=0.06;
sigma4=0.2;
mu4=-0.85;
P2=alpha4*(1/(sigma4*(2*pi)^0.5))*exp(-((csi-mu4).^2)/(2*sigma4^2));
% P1=alpha2*(1/(sigma2*(2*pi)^0.5))*exp(-((csi-mu2).^2)/(2*sigma2^2));

Am=logspace(-1,1);
n=length(Am);
csi2=log10(Am);
for i=1:1:n
    lambda_c=-1.5;
    muSOC=-0.3-1.4*(lambda_c+1.75);
    sigmaSOC=0.2;
    DAmSOC(i)=0.06*(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi2(i)-muSOC)^2)/(2*sigmaSOC^2));
end

%PLOT
histogram(log10(Am_CST),'Normalization','probability','BinWidth',0.05)
grid on
hold on
plot(csi,P,'-r','LineWidth',2)
plot(csi,P2,'k','LineWidth',2)
plot(csi2,DAmSOC,'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('A/m [m^2/kg]')
ylabel('Probability Density')
legend('CST Fragments','LAN PDF','CST PDF','NASA SBM***')

%% NASA SBM FOR A/m distribution
%
lambda_c=log10(Lc_CST);
csi=log10(Am_CST);
n=length(lambda_c);

for i=1:1:n
    if Lc_CST(i)<=0.11 %sarebbe minore di 8 cm ma non ci sono dati tra 8 e 11 (MD)
        if lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi(i)-muSOC)^2)/(2*sigmaSOC^2));
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2;
            DAmSOC(i)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi(i)-muSOC)^2)/(2*sigmaSOC^2));
        elseif lambda_c(i)>=-1.25 && lambda_c(i)<=-3.5
            muSOC=-1.0;
            sigmaSOC=0.2;
            DAmSOC(i)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi(i)-muSOC)^2)/(2*sigmaSOC^2));
        elseif lambda_c(i)>-3.5
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi(i)-muSOC)^2)/(2*sigmaSOC^2));
        end
    elseif Lc_CST(i)>0.11
        if lambda_c(i)<=-1.95
            alphaSC=0;
            mu1SC=-0.6;
            sigma1SC=0.1;
            mu2SC=-1.2;
            sigma2SC=0.5;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)<=-1.3 && lambda_c(i)>-1.95
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6;
            sigma1SC=0.1;
            mu2SC=-1.2;
            sigma2SC=0.5;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)<=-1.1 && lambda_c(i)>-1.3
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6;
            sigma1SC=0.1+0.2*(lambda_c(i)+1.3);
            mu2SC=-1.2;
            sigma2SC=0.5;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)<=-0.7 && lambda_c(i)>-1.1
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6-0.318*(lambda_c(i)+1.1);
            sigma1SC=0.1+0.2*(lambda_c(i)+1.3);
            mu2SC=-1.2;
            sigma2SC=0.5;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)<=-0.5 && lambda_c(i)>-0.7
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6-0.318*(lambda_c(i)+1.1);
            sigma1SC=0.1+0.2*(lambda_c(i)+1.3);
            mu2SC=-1.2-1.333*(lambda_c(i)+0.7);
            sigma2SC=0.5;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)<-0.3 && lambda_c(i)>-0.7
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6-0.318*(lambda_c(i)+1.1);
            sigma1SC=0.1+0.2*(lambda_c(i)+1.3);
            mu2SC=-1.2-1.333*(lambda_c(i)+0.7);
            sigma2SC=0.5-(lambda_c(i)+0.5);
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)==-0.3
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6-0.318*(lambda_c(i)+1.1);
            sigma1SC=0.3;
            mu2SC=-1.2-1.333*(lambda_c(i)+0.7);
            sigma2SC=0.3;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)<-0.1 && lambda_c(i)>-0.3
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6-0.318*(lambda_c(i)+1.1);
            sigma1SC=0.3;
            mu2SC=-1.2-1.333*(lambda_c(i)+0.7);
            sigma2SC=0.3;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)==-0.1
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.6-0.318*(lambda_c(i)+1.1);
            sigma1SC=0.3;
            mu2SC=-2;
            sigma2SC=0.3;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)==0
            alphaSC=0.3+0.4*(lambda_c(i)+1.2);
            mu1SC=-0.95;
            sigma1SC=0.3;
            mu2SC=-2;
            sigma2SC=0.3;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        elseif lambda_c(i)>=0.55
            alphaSC=1;
            mu1SC=-0.95;
            sigma1SC=0.3;
            mu2SC=-2;
            sigma2SC=0.3;
            DAmSOC(i)=alphaSC*(1/(sigma1SC*(2*pi)^0.5))*exp(-((csi(i)-mu1SC)^2)/(2*sigma1SC^2))+(1-alphaSC)*(1/(sigma2SC*(2*pi)^0.5))*exp(-((csi(i)-mu2SC)^2)/(2*sigma2SC^2));
        end
    end
end

%% NASA SBM FOR v distribution
csi=log10(Am_CST);
v=log10(sort(vel_CST));
n=length(v);
sigmaCOLL=0.4;

for i=1:1:n
    muCOLL=0.9.*min(csi)+2.9;
    DvCOLL(i)=(1/(sigmaCOLL*(2*pi)^0.5))*exp(-((v(i)-muCOLL)^2)/(2*sigmaCOLL^2));
end

%PLOT
figure
histogram(log10(vel_CST_np),'Normalization','probability','BinWidth',0.1)
grid on
hold on
plot(v,DvCOLL,'b','LineWidth',2)
xlabel('v [km/s]')
ylabel('Probability Density')
legend('CST Fragments velocities (no projectile)','NASA SBM***')

% figure
% histogram(log10(vel_CST_wp),'Normalization','probability','BinWidth',0.05)
%}