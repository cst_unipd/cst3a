% test_cylinder_projection.m

re = Frag_ME.a;
h = 2*Frag_ME.c;
t = Frag_ME.t;
ri = re-t;

Frag_ME.cylinder_e = [re re 0 re re h re]; % [p1 p2 r]
Frag_ME.cylinder_i = [re re t re re h-t ri]; % [p1 p2 r]
center = [re re h/2];

nTheta_cyl = 21; nH_cyl = 2;            % <<<=== TUNING PARAMETERS

% external and internal cylinders (cartesian coordinates)
[Frag_ME.ve_cartc, ~, Frag_ME.fe_cartc] = cylinderMesh_npnts(Frag_ME.cylinder_e,nTheta_cyl,nH_cyl); % 21,2
Frag_ME.ve_cartc_down = Frag_ME.ve_cartc(1:2:end,:);
Frag_ME.ve_cartc_top = Frag_ME.ve_cartc(2:2:end,:);

[Frag_ME.vi_cartc, ~, Frag_ME.fi_cartc] = cylinderMesh_npnts(Frag_ME.cylinder_i,nTheta_cyl,nH_cyl); % 21,2
Frag_ME.vi_cartc_down = Frag_ME.vi_cartc(1:2:end,:);
Frag_ME.vi_cartc_top = Frag_ME.vi_cartc(2:2:end,:);

% external and internal ellipsoids
rxe = re; rye = re; rze = h/2;
rxi = rxe - t;   ryi = rye - t;   rzi = rze - t;

% Frag_ME.ellipsoid_e = [rxe rye rze rxe rye rze];
% Frag_ME.ellipsoid_i = [rxe rye rze rxi ryi rzi];

nTheta_elp = 10; nPhi_elp = 20;         % <<<=== TUNING PARAMETERS

% exdternal and internal ellipsoids (cartesian coordinates)
elli_dome = [Frag_ME.ellipsoid_e, [0 0 0]]; % [xc yc zc a b c phi theta psi]
[Frag_ME.ve_carte, ~, Frag_ME.fe_carte] = ellipsoidMesh(elli_dome,'nTheta',nTheta_elp,'nPhi',nPhi_elp);
Frag_ME.ve_carte = unique(Frag_ME.ve_carte,'rows');
Frag_ME.fe_carte = minConvexHull(Frag_ME.ve_carte,1e-6);

elli_domi = [Frag_ME.ellipsoid_i, [0 0 0]]; % [xc yc zc a b c phi theta psi]
[Frag_ME.vi_carte, ~, Frag_ME.fi_carte] = ellipsoidMesh(elli_domi,'nTheta',nTheta_elp,'nPhi',nPhi_elp);
Frag_ME.vi_carte = unique(Frag_ME.vi_carte,'rows');
Frag_ME.fi_carte = minConvexHull(Frag_ME.vi_carte,1e-6);

%%

points_e = fragments{2}.seeds;
points_c = transfPointsElp2CylHollow(points_e,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);



for k=1:Impact_Data.N_frag_dom % N_frag_dom
%     fragments{k}.edges_cyl = cell(1,fragments{k}.nedge);
%     for i=1:fragments{k}.nedge
%         fragments{k}.edges_cyl{i} = transfPointsElp2CylHollow(fragments{k}.edges{i},Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
%     end
    fragments{k}.vorfc{i} = cell(1,fragments{k}.ncell);
    for i=1:fragments{k}.ncell
        fragments{k}.vorfc{i} = minConvexHull(fragments{k}.vorvx{i},1e-6);
    end
    fragments{k}.vorvx_cyl = cell(1,fragments{k}.ncell);
    fragments{k}.vorfc_cyl = cell(1,fragments{k}.ncell);
    for i=1:fragments{k}.ncell
        fragments{k}.vorvx_cyl{i} = transfPointsElp2CylHollow(fragments{k}.vorvx{i},Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
        fragments{k}.vorvx_cyl{i}(any(isnan(fragments{k}.vorvx_cyl{i}), 2), :) = []; % cancel NaN rows
        fragments{k}.vorfc_cyl{i} = minConvexHull(fragments{k}.vorvx_cyl{i},1e-6);
    end
end


for k=1:Impact_Data.N_frag_vol
    frags_in_volume{k}.CoM_cyl = transfPointsElp2CylHollow(frags_in_volume{k}.CoM,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
end

%%

figure()
set(gcf,'color','white');
hold on; axis equal; axis off;
view(0,0);

% ------------
% plot Frag RF
% pltassi(trasm(eye(3),[0 0 0]'),'b','',1,0.5);

%
% draw internal cylinder
drawMesh(Frag_ME.vi_cartc,Frag_ME.fi_cartc,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);
pid = patch(Frag_ME.vi_cartc_down(:,1),Frag_ME.vi_cartc_down(:,2),Frag_ME.vi_cartc_down(:,3),'red');
set(pid,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);
pit = patch(Frag_ME.vi_cartc_top(:,1),Frag_ME.vi_cartc_top(:,2),Frag_ME.vi_cartc_top(:,3),'red');
set(pit,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);

% draw external cylinder
drawMesh(Frag_ME.ve_cartc,Frag_ME.fe_cartc, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
ped = patch(Frag_ME.ve_cartc_down(:,1),Frag_ME.ve_cartc_down(:,2),Frag_ME.ve_cartc_down(:,3),'red');
set(ped,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
pet = patch(Frag_ME.ve_cartc_top(:,1),Frag_ME.ve_cartc_top(:,2),Frag_ME.ve_cartc_top(:,3),'red');
set(pet,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
%}

%
% draw internal ellipsoid
drawMesh(Frag_ME.vi_carte,Frag_ME.fi_carte,'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.5);
% draw external ellipsoid
drawMesh(Frag_ME.ve_carte,Frag_ME.fe_carte,'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
%}

% points from Ellipsoid to Cylinder
% plot impact point
%
p1e = Frag_Data(1,1:3);
line_vel = createLine3d(Frag_Data(1,1:3),Frag_Data(1,1:3)+Impact_Data.I_VEL_F);
p2e = intersectLineEllipsoid(line_vel,elli_dome); p2e = p2e(end,:);

p1c = transfPointsElp2CylHollow(p1e,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
p2c = transfPointsElp2CylHollow(p2e,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
new_vel = vectorNorm3d(Impact_Data.I_VEL_F)*(p2c-p1c)/vectorNorm3d(p2c-p1c);

drawPoint3d(p1e,'or'); drawPoint3d(p2e,'or');
drawPoint3d(p1c,'ob'); drawPoint3d(p2c,'ob');
drawVector3d(p1e,Impact_Data.I_VEL_F,'r');
drawVector3d(p1c,new_vel,'b');
%}


% points from Cylinder to Ellipsoid
%{
pp0 = [0 0 0];  % 0 0 1
ppv = [5 5 3]; % 5 5 -9
vel_c = ppv-pp0;
line_itsc = createLine3d(pp0,ppv);
p1c = intersectLineCylinder_ext(line_itsc,Frag_ME.cylinder_e);
p2c = p1c(end,:); p1c = p1c(1,:);
p1e = transfPointsCyl2ElpHollow(p1c,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
p2e = transfPointsCyl2ElpHollow(p2c,Frag_ME.cylinder_e,Frag_ME.cylinder_i,elli_dome,elli_domi);
vel_e = p2e-p1e;
drawPoint3d(p1c,'ob'); drawPoint3d(p2c,'ob');
drawVector(p1c,0.4*vel_c,'b');
drawPoint3d(p1e,'*r'); drawPoint3d(p2e,'*r');
drawVector(p1e,1.3*vel_e,'r');
%}

% drawPoint3d(points_e,'.r');
% drawPoint3d(points_c,'.k');

% for k=2:2 % 1:Impact_Data.N_frag_dom
%         % plot fragments edges
%     for i=1:fragments{k}.nedge
%         % this plot the edges
%         plot3(fragments{k}.edges_cyl{i}(:,1),fragments{k}.edges_cyl{i}(:,2),fragments{k}.edges_cyl{i}(:,3),...
%               '-','LineWidth',0.5);
%     end
% end

%{
for k=1:2 % 1:Impact_Data.N_frag_dom
    for i=1:fragments{k}.ncell
%         drawMesh(fragments{k}.vorvx{i},fragments{k}.vorfc{i},'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
        drawMesh(fragments{k}.vorvx_cyl{i},fragments{k}.vorfc_cyl{i},'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.3);
        
%         drawPoint3d(fragments{k}.vorvx_cyl{i},'.k');
    end
end
%}


for k=1:Impact_Data.N_frag_vol % 1:Impact_Data.N_frag_dom
    drawPoint3d(frags_in_volume{k}.CoM_cyl);
    drawVector3d(frags_in_volume{k}.CoM_cyl,frags_in_volume{k}.vel);
end


