flag_restart=1;
for i=1:length(file)
    [~,~,ext] = fileparts(file(i).name);
    if strcmp(ext,'.mat')
        load(fullfile([pwd,filesep,input_data_dir,filesep,file(i).name]))
        flag_restart=1;
    end
    
end

if flag_restart==1
    t_start=tf;
    disp(['Restart old simulation at time ' num2str(tf) ' s'])
end