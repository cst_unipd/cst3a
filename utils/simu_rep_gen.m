% file: simu_rep_gen
rep_fid = fopen(fullfile(sim_title_dir,'simu_report.txt'),'w');
fprintf(rep_fid,'==>> SIMULATION TITLE: %s <<==\n',sim_title);
fprintf(rep_fid,'\n\n');
fprintf(rep_fid,'Results output folder=%s\n',sim_title_dir);
fprintf(rep_fid,'\n');
fprintf(rep_fid,'Initial scenario:\n');
fprintf(rep_fid,'\tNumber of MEs:%d\n',n_ME0);
fprintf(rep_fid,'\tNumber of FRAGMENTS:%d\n',n_FRAGMENTS0);
fprintf(rep_fid,'\tNumber of BUBBLEs:%d\n',n_BUBBLE0);
fprintf(rep_fid,'\tNumber of links:%d\n\n',n_links0);
fprintf(rep_fid,'Settings:\n');
fprintf(rep_fid,'\tSimulation Time (Seconds)=%f\n', t_end);
fprintf(rep_fid,'\tInitial Time Steps (Seconds)=%f\n',  t_step);
fprintf(rep_fid,'\tSimulation Starting Time (Seconds)=%f\n',  t_start);
fprintf(rep_fid,'\tStructural Response Time Steps (Seconds)=%f\n',  dt_SR);
fprintf(rep_fid,'\tMaximum Simulation Step=%d\n',n_max_main_loop);
fprintf(rep_fid,'\tVoronoi Solver=%d\n', voronoi_solver_ID );
if(PROPAGATION_THRESHOLD.bubble_mass_th_flag==1 || PROPAGATION_THRESHOLD.bubble_radius_th_flag==1 || PROPAGATION_THRESHOLD.bubble_energy_th_flag==1)
    fprintf(rep_fid,'\tBUBBLE Generation activated\n');
end
if(PROPAGATION_THRESHOLD.fragment_mass_th_flag==1 || PROPAGATION_THRESHOLD.fragment_radius_th_flag==1 || PROPAGATION_THRESHOLD.fragment_energy_th_flag==1)
    fprintf(rep_fid,'\tAutomatic Fragments Unbreaking activated\n');
end
fprintf(rep_fid,'\nOutputs Summary:\n')

if n_collision_tot>0
    n_ME_final=length(ME)-length(KILL_LIST.ME);
    n_FRAGMENTS_final=length(FRAGMENTS)-length(KILL_LIST.FRAGMENTS);
    n_BUBBLE_final=length(BUBBLE);
    fprintf(rep_fid,'Final scenario:\n');
    fprintf(rep_fid,'\tNumber of MEs:%d\n',n_ME_final);
    fprintf(rep_fid,'\tNumber of FRAGMENTS:%d\n',n_FRAGMENTS_final);
    fprintf(rep_fid,'\tNumber of estimated BUBBLEs:%d\n',n_BUBBLE_final);
    if(length(link_data)>0)
        fprintf(rep_fid,'\tNumber of unbroken links:%d\n\n',sum([link_data.state]));
    end
    fprintf(rep_fid,'\n\n');
    fprintf(rep_fid,'Final simulation time (Seconds)=%f\n',tf);
    fprintf(rep_fid,'Final number of steps=%d\n', main_loop_count);
    fprintf(rep_fid,'Number of processed collisions=%d\n',n_collision_tot);
    fprintf(rep_fid,'Total elapsed time (Seconds)=%f\n',RunTime);
    
    fprintf(rep_fid,'\n');
else
    fprintf('\nNo impact Detected!\n')
end

if cst_log.getErrorCount>0
    fprintf(rep_fid,'Some errors occurred: summary saved in error.log file\n');
end
if cst_log.getWarningCount>0
    fprintf(rep_fid,'Some warnings occurred: summary saved in warning.log file\n');
end
fclose(rep_fid);
