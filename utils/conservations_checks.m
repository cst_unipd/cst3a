%> @file    conservation checks.m
%> @brief   monitor mass, energy and momentum variation for CST Matlab
%> @authors  Giulia Sarego and Francesco Feltrin (giulia.sarego@unipd.it)
%> @date    03 August 2018

%> Copyright 2018 CISAS - UNIVERSITY OF PADOVA

%Post Processing script 
RIS0=load(fullfile(sim_title_dir,'data','step_0.mat'));

%mass conservation
m_ME0=0;
m_MEfinal=0;
for i=1:length(ME)
    m_MEfinal=m_MEfinal+ME(i).GEOMETRY_DATA.mass;
end

for i=1:length(RIS0.ME)
    m_ME0=m_ME0+RIS0.ME(i).GEOMETRY_DATA.mass0;
end

m_FRAGMENTS0=0;
m_FRAGMENTSfinal=0;
for i=1:length(FRAGMENTS)
    m_FRAGMENTSfinal=m_FRAGMENTSfinal+FRAGMENTS(i).GEOMETRY_DATA.mass;
end

for i=1:length(RIS0.FRAGMENTS)
    m_FRAGMENTS0=m_FRAGMENTS0+RIS0.FRAGMENTS(i).GEOMETRY_DATA.mass0;
end

m_BUBBLE0=0;
m_BUBBLEfinal=0;
for i=1:length(BUBBLE)
    m_BUBBLEfinal=m_BUBBLEfinal+BUBBLE(i).GEOMETRY_DATA.mass;
end

for i=1:length(RIS0.BUBBLE)
    m_BUBBLE0=m_BUBBLE0+RIS0.BUBBLE(i).GEOMETRY_DATA.mass0;
end

Initial_mass=m_ME0+m_FRAGMENTS0+m_BUBBLE0;
final_mass=m_MEfinal+m_FRAGMENTSfinal+m_BUBBLEfinal;
disp(['Mass variation :',num2str(Initial_mass-final_mass),'kg, equivalent to ',num2str((Initial_mass-final_mass)/Initial_mass*100),'%.']);

%conservation of Energy
E0=0;
for i=1:length(RIS0.ME)
    E0=E0+norm(RIS0.ME(i).DYNAMICS_INITIAL_DATA.vel0)^2*0.5*ME(i).GEOMETRY_DATA.mass0;
end
for i=1:length(RIS0.FRAGMENTS)
    E0=E0+norm(RIS0.FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel)^2*0.5*RIS0.FRAGMENTS(i).GEOMETRY_DATA.mass0;
end
for i=1:length(RIS0.BUBBLE)
    E0=E0+norm(RIS0.BUBBLE(i).DYNAMICS_INITIAL_DATA.vel)^2*0.5*RIS0.BUBBLE(i).GEOMETRY_DATA.mass0;
end

EF=0;
for i=1:length(ME)
    EF=EF+norm(ME(i).DYNAMICS_DATA.vel)^2*0.5*ME(i).GEOMETRY_DATA.mass;
end

for i=1:length(FRAGMENTS)
    EF=EF+norm(FRAGMENTS(i).DYNAMICS_DATA.vel)^2*0.5*FRAGMENTS(i).GEOMETRY_DATA.mass;
end
for i=1:length(BUBBLE)
    EF=EF+norm(BUBBLE(i).DYNAMICS_DATA.vel)^2*0.5*BUBBLE(i).GEOMETRY_DATA.mass;
end


if E0>1e-15
    disp(['Energy variation :',num2str((E0-EF)/E0*100),'%.']);
else
    disp(['The initial energy is ',num2str(E0),' J, while the final one is ',num2str(EF),' J.']);
end

%conservation of Momentum 
Q0=0;
for i=1:length(RIS0.ME)
    Q0=Q0+RIS0.ME(i).DYNAMICS_INITIAL_DATA.vel0*RIS0.ME(i).GEOMETRY_DATA.mass0;
end
for i=1:length(RIS0.FRAGMENTS)
    Q0=Q0+RIS0.FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel*RIS0.FRAGMENTS(i).GEOMETRY_DATA.mass0;
end
for i=1:length(RIS0.BUBBLE)
    Q0=Q0+RIS0.BUBBLE(i).DYNAMICS_INITIAL_DATA.vel*RIS0.BUBBLE(i).GEOMETRY_DATA.mass0;
end


QF=0;
for i=1:length(ME)
    QF=QF+ME(i).DYNAMICS_DATA.vel*ME(i).GEOMETRY_DATA.mass;
end
for i=1:length(FRAGMENTS)
    QF=QF+FRAGMENTS(i).DYNAMICS_DATA.vel*FRAGMENTS(i).GEOMETRY_DATA.mass;
end
for i=1:length(BUBBLE)
    QF=QF+BUBBLE(i).DYNAMICS_DATA.vel*BUBBLE(i).GEOMETRY_DATA.mass;
end
% disp(['Momentum variation :',num2str(norm(Q0'-QF')/norm(Q0)*100),'%']);

if norm(Q0)>1e-15
    disp(['Momentum variation :',num2str(norm(Q0'-QF')/norm(Q0)*100),'%.']);
else
    disp(['The initial momentum is ',num2str(norm(Q0)),' kg*m/s, while the final one is ',num2str(norm(QF)),' kg*m/s.']);
end

% norm_Q0=norm(Q0)
% norm_QF=norm(QF)
% lQ0=Q0
% lQF=QF