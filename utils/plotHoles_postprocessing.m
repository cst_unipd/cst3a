function plotHoles_postprocessing(HOLES)

%%

nHoles = length(HOLES);

% plot Holes
for k=1:nHoles
    if(HOLES(k).GEOMETRY_DATA.mass>0)
        % plot parent ME   
        drawPoint3d(HOLES(k).DYNAMICS_DATA.cm_coord','*r');
        HOLES(k).f_hull = minConvexHull(HOLES(k).GEOMETRY_DATA.c_hull);
        drawMesh(HOLES(k).GEOMETRY_DATA.c_hull+HOLES(k).DYNAMICS_DATA.cm_coord',...
                 HOLES(k).f_hull,...
                 'FaceColor',[0 0 0],'FaceAlpha',1);
    end
end
%}


end