function P3_plot_SeedsDistrib_FragDomains(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain)

    for i=1:Impact_Data.N_frag_dom % 1:N_frag_dom
    
        figure()
        set(gcf,'color','white');
        hold on; axis equal;
        view(30,40);
        title(['Frag Domain #' num2str(i)]);

        % ------------
        % plot Frag RF
        pltassi(trasm(eye(3),[0 0 0]'),'k','',1,2);

        % --------------------
        % plot the Impacted ME
        drawMesh(Frag_ME.v_sph, Frag_ME.f_sph, 'FaceColor',[0 0 0],'FaceAlpha',0.002);

        % -----------------------------------------
        % plot the Fragmentation Volumes with seeds
        if (length(Impact_Data.intersect_groups{i}) > 1) && (length(Frag_Domain{i}.idx_complete_frag) <=1)
            for j=1:length(Impact_Data.intersect_groups{i})
                drawMesh(Frag_Domain{i}.Volume{j}.v_sph, Frag_Domain{i}.Volume{j}.f_sph, ...
                         'FaceColor', [0 1 0],'FaceAlpha',0.1,'EdgeAlpha',0.5);
                %drawPoint3d(Frag_Domain{i}.Volume{j}.seeds_sph,'o');
            end
        end

        % -----------------------------------------
        % plot the Fragmentation Domains with seeds
        drawMesh(Frag_Domain{i}.v_sph, Frag_Domain{i}.f_sph,...
                 'FaceColor', [1 0 0],'FaceAlpha',0.01,'EdgeAlpha',0.5);
        % for i=1:size(v_i,1)
        %     text(v_i(i,1)+0.1,v_i(i,2),v_i(i,3),num2str(i));
        % end
        drawPoint3d(Frag_Domain{i}.seeds_sph,'.r');

    end
    

end

