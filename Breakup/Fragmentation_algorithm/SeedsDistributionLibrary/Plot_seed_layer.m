function Plot_seed_layer(seeds_layer)
figure()
hold on;

% inserire gli indici dei layer da plottare
k_plot = [1:length(seeds_layer)];
% k_plot = 1:length(seeds_layer);

for j=1:length(k_plot)
    if ~isempty(seeds_layer{j})
        i=k_plot(j);
        plot3(seeds_layer{i}(:,1),seeds_layer{i}(:,2),seeds_layer{i}(:,3),'o');
    end
end

% axis equal; axis off;
%view(0,0);