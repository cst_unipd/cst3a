function Frag_Volume = SeedsDistrib_LogSpiral_Solid_Nishida(COLLISION_DATA,Frag_ME,Frag_Volume,Frag_Data,Impact_Data)
%SeedsDistrib_LogSpiral_Solid Compute the seeds distribution for
%plates for a Logaritmic Spiral Distribution
% 
% Syntax:  Frag_Volume = SeedsDistrib_LogSpiral_Solid(Frag_ME,Frag_Volume,Frag_Data)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Frag_Volume - Frag_Volume structure
%    Frag_Data - Impact point and fragmentation volume radius
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2018/04/11
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/04/11 : first version by AV
% 2018/06/14 : update with Nishida's test cases by AV

%#codegen
%%
global ME_FR
global FRAGMENTS_FR

for i=1:length(Frag_Volume) % N_impact
   % compute the Voronoi (v) Frame of Reference
    Frag_Volume_i_centros = faceCentroids(Frag_Volume{i}.v, Frag_Volume{i}.f);
    Delta_v = Frag_Volume_i_centros - Impact_Data.I_POS_F(i,:);
    [~,imin] = min(vectorNorm3d(Delta_v));
    normal = faceNormal(Frag_Volume{i}.v, Frag_Volume{i}.f);
    normal = normal(imin,:);
    alpha_angle = vectorAngle3d(normal,Impact_Data.I_VEL_F(i,:));
    if alpha_angle>pi/2
        normal = -normal;
        alpha_angle = pi-alpha_angle;
    end

    zv = normal;
    if ~isParallel3d(zv,[1 0 0])
        yv = vectorCross3d(zv,[1 0 0]);
    else
        yv = vectorCross3d(zv,[0 1 0]);
    end
    yv = yv/vectorNorm3d(yv);
    xv = vectorCross3d(yv,zv);
    RVF = [xv',yv',zv'];
    pVF = Impact_Data.I_POS_F(i,:)';
    TVF = trasm(RVF,pVF);
    TFV = inv(TVF);
    
    % Frag_Volume chull in the V RF
    Frag_Volumei_V = trasfg_vectors(Frag_Volume{i}.v',TFV)';
    
    vel_vers_V = trasfg_vectors(Impact_Data.I_VEL_F(i,:)',TFV)';
    vel_vers_V = vel_vers_V/vectorNorm3d(vel_vers_V);
    
    % impact_velocity in km/s projected along the impacting surface normal
    impact_velocity = vectorNorm3d(Impact_Data.I_VEL_F(i,:));
    impact_velocity_n_Nishida = 1e-3*impact_velocity *cos(alpha_angle);
    
    projectile_diameter = getProjectileEquivalentDiameter(COLLISION_DATA.IMPACTOR(i)); % AV 25-07-18
    projectile_diameter_Nishida = 1e3*projectile_diameter;
    
%     if(projectile_diameter_Nishida>)
%             seeds=distribution_LO_3D(Frag_Data(i,1:3),Frag_Data(i,4),n_seeds);
    
    
    
    % compute param_vector from impact conditions using Nishida lookup table
    % input => projectile_diameter, impact velocity
    % output => param_vector = [s1 s2 s3 s4]
    param_vector = getNishidaSpiralParameters2(projectile_diameter_Nishida,impact_velocity_n_Nishida);
    
    % df_min % Nishida's experiment resolution (minimum fragments
    % dimension)
    if projectile_diameter >= 1.6e-3
        df_min = 0.0005;
    else
        df_min = 0.0001;
    end
    
    k_a = param_vector(1);
    
    % Compute h_ext and k_tot
    % depth of the crater in the velocity direction =>
    % Frag_Data(i,4) for spherical crater
    % Frag_Data(i,6) for ellipsoidal crater
    if Frag_Data(i,6)==0
        % spherical crater
        r_ext = Frag_Data(i,4);
        h_ext = Frag_Data(i,4);
        h_ext_eff=min(max(abs(Frag_Volumei_V(:,3))),h_ext); %CG 30-08-18
        %param_vector(4)=param_vector(4)*h_ext_eff/h_ext;
        k_tot = ceil(h_ext_eff/df_min) + 1;
        %k_tot = ceil(k_tot / param_vector(4));
    else
        % ellipsoidal crater
        r_ext = max(Frag_Data(i,4:5))/cos(alpha_angle);
        h_ext = Frag_Data(i,6)*cos(alpha_angle);
        h_ext_eff=min(max(abs(Frag_Volumei_V(:,3))),h_ext); %CG 30-08-18
%         param_vector(4)=param_vector(4)*h_ext_eff/h_ext;
        k_tot = ceil(h_ext_eff/df_min) + 1; 
    end

%     r_ext
    % init Frag_Volume{i}.seeds
    Frag_Volume{i}.seeds = [];
    
    % compute seeds layers in the V RF
    seeds_layer = cell(1,k_tot);
%%
    for k=1:k_tot

        seeds_layer{k} = GenerateSeedsLayer('irregular',k,k_a,df_min,0,0,...
                                            r_ext,h_ext_eff,param_vector); % AV => h_est => h_ext_eff
        if(isempty(seeds_layer{k})==0)                                                          
            [~,imin] = min(seeds_layer{k}(:,3));
            z_min_vect = real(seeds_layer{k}(imin,:)); % patch GS
            cos_alpha = cos(vectorAngle3d(z_min_vect,vel_vers_V)); 
            l_vect = vectorNorm3d(z_min_vect)/cos_alpha*vel_vers_V;
            xc = l_vect(1); yc = l_vect(2);
            seeds_layer{k}(:,1) = seeds_layer{k}(:,1) + xc;
            seeds_layer{k}(:,2) = seeds_layer{k}(:,2) + yc;
            
            %seeds_layer_origin{k} = seeds_layer{k};
            
            seeds_layer{k} = getPointsWithinBoundary(seeds_layer{k},Frag_Volumei_V);

        end
    end
    
    %%
    %{
    figure()
    set(gcf,'color','white');
    hold on; axis equal;
    view(30,40);
    
    %drawMesh(Frag_ME.v, Frag_ME.f, 'FaceColor',[0 0 0],'FaceAlpha',0.02);
    for jj=1:2%length(seeds_layer_origin)
        drawPoint3d(seeds_layer_origin{jj},'*');
    end
    %}
    
    %%
    % compute seeds matrix in V RF
    seeds = [];
    for k=1:k_tot
%         if(isfinite(seeds_layer{k})) % CG 19-07-18
            seeds = [seeds; seeds_layer{k}];
%         end
    end

    if isempty(seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v);
    else
        % seeds in the F RF
        seeds_F = trasfg_vectors(seeds',TVF)';
        seeds_F = getPointsWithinBoundary(seeds_F,Frag_Volume{i}.v);
        Frag_Volume{i}.seeds = unique(seeds_F,'rows');
        if(isempty(seeds_F)) % CG 24-07-18
            [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v);
        end
        Frag_Volume{i}.seeds_layer = seeds_layer;
%          Plot_seed_layer(seeds_layer);
    end


end



end
