%> @file  distribution_LO_3D.m
%> @brief calculation of seeds 3D spherical standard distributions
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri

function[seeds]=distribution_LO_3D(POINT,R,s_number)
%% Input data
% POINT: impact point, 3x1 or 1x3 vector
% R: fragmentation radius 
% s_number: number of seeds

%% Output data
% seeds: [s_number x s] coordinates of seeds

debug_mode=0;


% input check
s_number=ceil(s_number/0.1);

%% DISTRIBUTION CALCULATION
% standard distribution, radius
r=randn(s_number,1);

% rectangular distribution, angles
az=2*pi*rand(s_number,1);
el=pi*(1-2*rand(s_number,1));

seeds=zeros(s_number,3);
i=1;
for count=1:s_number
    if abs(r(count))<=0.25
        seeds(i,1)=POINT(1)+4*R*r(count)*cos(az(count))*cos(el(count));
        seeds(i,2)=POINT(2)+4*R*r(count)*sin(az(count))*cos(el(count));
        seeds(i,3)=POINT(3)+4*R*r(count)*sin(el(count));
        i=i+1;
    end
end

if debug_mode==1
    plot3(seeds(:,1),seeds(:,2),seeds(:,3),'*')
end

end