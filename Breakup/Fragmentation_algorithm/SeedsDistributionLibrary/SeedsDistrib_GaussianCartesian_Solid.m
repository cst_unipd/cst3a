function Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data)
%SeedsDistrib_GaussianCartesian_Solid Compute the seeds distribution for
%solid shapes for a gaussian distribution in cartesian coordinates
% 
% Syntax:  Frag_Volume = SeedsDistrib_GaussianCartesian_Solid(Frag_Volume,n_seeds,n_max_loop,Frag_Data)
%
% Inputs:
%    Frag_Volume - Frag_Volume structure
%    n_seeds - maximum number of seeds within the Frag Volume
%    n_max_loop - maximum number of while loops to obtain the required
%                 number of seeds
%    Frag_Data - Impact point and fragmentation volume radius (GS+FF)
%
% Outputs:
%    Frag_Volume - Frag_Volume structure with updated field seeds
%
% Other m-files required: getPointsWithinBoundary
% Subfunctions: none
% MAT-files required: unique, rand
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV, FF, GS

%#codegen

% generate a random distribution of n_seeds seeds
for i=1:length(Frag_Volume) % N_impact
    n_seeds
    [~,volume] = polyhedronCentroidVolume(Frag_Volume{i}.v);
    if(volume<pi*2/3*(Frag_Data(i,4))^3)
        n_seeds=ceil(n_seeds*(pi*2/3*(Frag_Data(i,4))^3)/volume)
    end
    Frag_Volume{i}.seeds = [];
    check_loops = 0;

    while ( check_loops<n_max_loop && size(Frag_Volume{i}.seeds,1)< n_seeds )

        % generate n_seeds random
%         ax = min(Frag_Volume{i}.v(:,1)); bx = max(Frag_Volume{i}.v(:,1));
%         ay = min(Frag_Volume{i}.v(:,2)); by = max(Frag_Volume{i}.v(:,2));
%         az = min(Frag_Volume{i}.v(:,3)); bz = max(Frag_Volume{i}.v(:,3));
%         seeds = [ax+(bx-ax)*rand(n_seeds,1), ...
%                  ay+(by-ay)*rand(n_seeds,1), ...
%                  az+(bz-az)*rand(n_seeds,1)];
        seeds=distribution_LO_3D(Frag_Data(i,1:3),Frag_Data(i,4),n_seeds);
        % get the seeds that are within the Frag_Volume
        seeds = getPointsWithinBoundary(seeds,Frag_Volume{i}.v);
        seeds = unique(seeds,'rows');

        % add the new seeds to Frag_Volume{i}.seeds
        Frag_Volume{i}.seeds = [Frag_Volume{i}.seeds;seeds];

        check_loops = check_loops + 1;
    end

    if isempty(Frag_Volume{i}.seeds)
        [Frag_Volume{i}.seeds, ~] = polyhedronCentroidVolume(Frag_Volume{i}.v); % GS + FF
    else
        Frag_Volume{i}.seeds = unique(Frag_Volume{i}.seeds,'rows');
    end
    % take n_seeds seeds
    if (size(Frag_Volume{i}.seeds,1)>n_seeds)
        Frag_Volume{i}.seeds = Frag_Volume{i}.seeds(1:n_seeds,:);
    end

end

% disp('ok');

end
