function P4_plot_Fragmentation_Solid(Frag_ME,Impact_Data,Frag_Volume,Frag_Domain,domain,frags_in_volume)

%%
    for k=1:Impact_Data.N_frag_dom
%%
        figure()
        set(gcf,'Color','White');
        hold on; axis equal; axis off;
        view(30,40);
        
        title(['Frag Domain ' num2str(k)],'FontSize',15);

        if ~isempty(domain{k})
            for i=1:domain{k}.nedge
                % this plot the edges
                plot3(domain{k}.edges{i}(:,1),domain{k}.edges{i}(:,2),domain{k}.edges{i}(:,3),'k');
            end

            for i=1:domain{k}.nface
                % this plot the faces
                fill3(domain{k}.faces{i}(:,1),domain{k}.faces{i}(:,2),domain{k}.faces{i}(:,3),'k','FaceAlpha',0);
            end
        else
            drawMesh(Frag_Domain{k}.v,Frag_Domain{k}.f, 'FaceColor', 'r','FaceAlpha',0.1);
        end

%         for i=1:domain{k}.nvertex
%              % this plot the vertices
%             plot3(domain{k}.vertex(:,1),domain{k}.vertex(:,2),domain{k}.vertex(:,3),'*k');
%         end

        % plot fragments
        if length(Impact_Data.intersect_groups{k})==1
            i = Impact_Data.intersect_groups{k};
            for ii=1:frags_in_volume{i}.ncells   % 1:frags_in_volume{i}.ncells
%                 drawMesh(frags_in_volume{i}.vorvx{ii}, frags_in_volume{i}.vorfc{ii}, ...
%                          'FaceColor',[1 0 0],'FaceAlpha',0.02);
                plot3(frags_in_volume{i}.seeds(ii,1),frags_in_volume{i}.seeds(ii,2),frags_in_volume{i}.seeds(ii,3),'*k');
                plot3(frags_in_volume{i}.CoM(ii,1),frags_in_volume{i}.CoM(ii,2),frags_in_volume{i}.CoM(ii,3),'*r');
            end
        else
            for j = 1:length(Impact_Data.intersect_groups{k})
                i = Impact_Data.intersect_groups{k}(j);
                col = rand(1,3); col = col/norm(col);
                
                % plot fragments
                for ii=1:frags_in_volume{i}.ncells   % ii=1:frags_in_volume{i}.ncells
                    if frags_in_volume{i}.kill_flag(ii,1)==1
%                     drawMesh(frags_in_volume{i}.vorvx{ii}, frags_in_volume{i}.vorfc{ii}, ...
%                              'FaceColor',[0 0 0],'FaceAlpha',0.002);
%                    plot3(frags_in_volume{i}.seeds(ii,1),frags_in_volume{i}.seeds(ii,2),frags_in_volume{i}.seeds(ii,3),'*','Color',col);
                    plot3(frags_in_volume{i}.CoM(ii,1),frags_in_volume{i}.CoM(ii,2),frags_in_volume{i}.CoM(ii,3),'om');
                    end
                end
                
                % plot Frag Volume
                drawMesh(Frag_Volume{i}.v, Frag_Volume{i}.f, 'FaceColor', 'g','FaceAlpha',0.1);
                
            end
        end
        
    end


end
