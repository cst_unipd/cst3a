function points_elp = transfPointsCyl2ElpHollow(points_cyl,cylinder_e,cylinder_i,ellipsoid_e,ellipsoid_i)

center = ellipsoid_e(1,1:3);

points_cyl = points_cyl - center;
cylinder_e(1,1:3) = cylinder_e(1,1:3) - center;
cylinder_i(1,1:3) = cylinder_i(1,1:3) - center;
cylinder_e(1,4:6) = cylinder_e(1,4:6) - center;
cylinder_i(1,4:6) = cylinder_i(1,4:6) - center;
ellipsoid_e(1,1:3) = ellipsoid_e(1,1:3) - center;
ellipsoid_i(1,1:3) = ellipsoid_i(1,1:3) - center;

line = createLine3d(zeros(size(points_cyl,1),3),points_cyl);
rP_C = points_cyl;
rP_Ce = intersectLinesCylinder_ext(line,cylinder_e); rP_Ce = rP_Ce((end/2)+1:end,:);
rP_Ci = intersectLinesCylinder_ext(line,cylinder_i); rP_Ci = rP_Ci((end/2)+1:end,:);
rP_Ee = intersectLineEllipsoid(line,ellipsoid_e); rP_Ee = rP_Ee((end/2)+1:end,:);
rP_Ei = intersectLineEllipsoid(line,ellipsoid_i); rP_Ei = rP_Ei((end/2)+1:end,:);

rP_C_mod = vectorNorm3d(rP_C);
rP_Ce_mod = vectorNorm3d(rP_Ce);
rP_Ci_mod = vectorNorm3d(rP_Ci);
rP_Ee_mod = vectorNorm3d(rP_Ee);
rP_Ei_mod = vectorNorm3d(rP_Ei);

rP_E_mod = rP_Ee_mod - (rP_Ee_mod-rP_Ei_mod).*(rP_Ce_mod-rP_C_mod)./(rP_Ce_mod-rP_Ci_mod);
rP_E = rP_E_mod .* rP_C./rP_C_mod;

points_elp = rP_E + center;

% disp('ok');

end
