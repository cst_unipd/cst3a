/*!  @file  cst_tracking_mex_interface.cpp
* @brief Collection of Matlab/MEX functions as interface between cst_tracking_cpp.h C++ code for tracking/collision detection using Bullet and CST main Matlab code 
* @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
* Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA
*======================================================================
*/
// #include "mex.h"
#include "string.h"

#include "cst_voropp_cpp.h"


//------------------------------------------------------------------------

//======================================================================
/*! @brief Main function to manage inputs from matlab and outputs from voro++.
* Call is formatted as MEX standards
*   
* @param nlhs Number of output variables
* @param plhs Pointer to mxArray of output variables. Outputs are:
 *
* @param prhs Pointer to mxArray of input variables. Inputs are:
 *

*
*======================================================================
*/
void mexFunction(int nlhs, mxArray *plhs[], int nrhs, const mxArray *prhs[])
{

    int voropp_main_return;
    mwSize np, ns, nv; // number of domains and nuber of seeds   
    int i,j,k; // indexes
    
    #ifdef DBG
    printf("-> mex: Starting voro++ analysis ...\n");
    #endif
    
    /*-----------------------------------------------------------------------------------------*/
    /*Getting inputs from CST Matlab code*/
    /* INPUTS from matlab:
     *  const double con_bounding_box[3][2];
        const int con_num_blocks[3];
        const double domain[np][4]
        const double seeds[ns][3]
     */
    //Retrieving ME structure
    np=mxGetNumberOfElements(prhs[2])/4;
    ns=mxGetNumberOfElements(prhs[3])/3;
    
    #ifdef DBG
    printf("number of seeds: %d\n",ns);
    printf("number of planes: %d\n",np);
    #endif
    
    //Initialization
    //Inputs
    double con_bounding_box[3][2];
    int con_num_blocks[3];
    double domain[np][4];
    double seeds[ns][3];
    //Outputs
    int frag_id[ns] = {-10};
    double frag_volume[ns] = {-10.0};
    double frag_centroid[ns][3];
    int frag_nvert[ns] = {0};
    int nvert_tot;
    vector< vector<double> > frag_c_hull;
    //double frag_c_hull[][3];
//     vector <double> frag_c_hull[ns][3];

//     vector< vector<double> > vec;
//     for(i=0;i<10;i++){
//         vector<double> row;
//         for(j=0;j<3;j++){
//             row.push_back(i*j);
//         }
//         vec.push_back(row);
//     }
       
//     mxArray *out_array;
    /*-----------------------------------------------------------------------------------------*/
    /*Converting inputs from CST Matlab code to C++ data*/

    if( nrhs ) {
        for (i=0;i<3;i++){
            for (j=0;j<2;j++){
                con_bounding_box[i][j]=*(mxGetPr(prhs[0])+i+j*3); //
//                 printf("con_bounding_box[%d][%d]=%f\n",i,j,con_bounding_box[i][j]);
            }
        }
        for (i=0;i<3;i++){
            con_num_blocks[i]=*(mxGetPr(prhs[1])+i); 
//             printf("con_num_blocks[%d]=%d\n",i,con_num_blocks[i]);
        }
        for (i=0;i<np;i++){
            for (j=0;j<4;j++){
                domain[i][j]=*(mxGetPr(prhs[2])+i+j*np); 
//                 printf("domain[%d][%d]=%f\n",i,j,domain[i][j]);
            }
        }
        for (i=0;i<ns;i++){
            for (j=0;j<3;j++){
                seeds[i][j]=*(mxGetPr(prhs[3])+i+j*ns);
//                 printf("seeds[%d][%d]=%f\n",i,j,seeds[i][j]);
            }
        }
    #ifdef DBG
    printf("\n");
    #endif
    
    /*-----------------------------------------------------------------------------------------*/
    /*Calling voropp_main function */
            
    voropp_main_return = voropp_main(con_bounding_box, con_num_blocks, domain, seeds, np, ns,
                                     frag_id, frag_volume, frag_centroid, frag_nvert,
                                     &nvert_tot,
                                     &frag_c_hull);//, frag_c_hull
    
    #ifdef DBG
    printf("\n\n\n");
    printf("==>>>> output at mex\n\n");
    printf("-->> frag_id:\n");
    for(i=0;i<ns;i++)
        printf("%d\n",frag_id[i]);
    
    printf("\n");
    printf("-->> frag_volume:\n");
    for(i=0;i<ns;i++)
        printf("%g\n",frag_volume[i]);
    
    printf("\n");
    printf("-->> frag_centroid:\n");
    for(i=0;i<ns;i++)
        printf("%g , %g , %g\n",frag_centroid[i][0],frag_centroid[i][1],frag_centroid[i][2]);
    
    printf("\n");
    printf("-->> frag_nvert:\n");
    for(i=0;i<ns;i++)
        printf("%d\n",frag_nvert[i]);
    
    printf("\n");
    printf("-->> nvert_tot:\n%d\n",nvert_tot);
    #endif
        
     /*-----------------------------------------------------------------------------------------*/
     /*Converting outputs from C++ CST tracking/collision detection code to Matlab data*/
//        nv=frag_c_hull.size();
//        frag_c_hull_dims[3] = {ns,nv,3};
//        plhs[0] = mxCreateNumericArray(3, frag_c_hull_dims, mxDOUBLE_CLASS, mxREAL);
//        for( i=0; i<ns; i++ ) {
//            for(j=0; j<nv; j++)
//            {
//                for(k=0;k<3;k++)
//                     *(mxGetPr(plhs[0])+i*nv+j*3+k) = frag_c_hull[i][j][k];
// //                     *(mxGetPr(plhs[0])+i+j*ns+k*nv) = frag_c_hull[i][j][k];
//                }
//            }
//        }
//      


    // frag_id
    plhs[0] = mxCreateDoubleMatrix(ns,1,mxREAL);
    for( i=0; i<ns; i++ ) {
       for(j=0; j<1; j++)
       {
            *(mxGetPr(plhs[0])+i+j) = frag_id[i];
       }
    }
    
    // frag_volume
    plhs[1] = mxCreateDoubleMatrix(ns,1,mxREAL);
    for( i=0; i<ns; i++ ) {
       for(j=0; j<1; j++)
       {
            *(mxGetPr(plhs[1])+i+j) = frag_volume[i];
       }
    }
    
    // frag_centroid
    plhs[2] = mxCreateDoubleMatrix(ns,3,mxREAL);
    for( i=0; i<ns; i++ ) {
       for(j=0; j<3; j++)
       {
            *(mxGetPr(plhs[2])+i+j*ns) = frag_centroid[i][j];
       }
    } 

    // frag_nvert
    plhs[3] = mxCreateDoubleMatrix(ns,1,mxREAL);
    for( i=0; i<ns; i++ ) {
       for(j=0; j<1; j++)
       {
            *(mxGetPr(plhs[3])+i+j) = frag_nvert[i];
       }
    }
    
    // nvert_tot
    plhs[4] = mxCreateDoubleMatrix(1,1,mxREAL);
    *(mxGetPr(plhs[4])) = nvert_tot;

    }
    
    // frag_c_hull
    plhs[5] = mxCreateDoubleMatrix(nvert_tot,3,mxREAL);
    //printf("frag_c_hull.size() = %d\n",frag_c_hull.size());
    for( i=0; i<nvert_tot; i++ ) {
       vector<double> row = frag_c_hull.at(i);
        for(j=0; j<3; j++)
       {
            *(mxGetPr(plhs[5])+i+j*nvert_tot) = row.at(j);
       }
    } 
    
    //mxFree(timeStep);
    #ifdef DBG
    printf("... voro++ analysis completed\n");
    #endif
    
    return;
}//ending mexFunction








//============================================================================================
//END OF FILE


