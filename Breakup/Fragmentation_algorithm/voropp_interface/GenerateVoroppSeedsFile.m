%> @file   GenerateVoroppSeedsFile.m
%> @brief  Generate Voro++ seeds file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Generate Voro++ seeds file
%> 
%> Thsi function creates in the neper_folder a seeds .msh file with name
%> %<test_name>_seeds.msh
%>
%> @param neper_folder Path to the neper folder.
%> @param test_name name of the test.
%> @param seeds matrix with the seeds positions (Nx3 matrix)
%>
%==========================================================================
function [ ] = GenerateVoroppSeedsFile(neper_folder,test_name,seeds)


% neper file name
if isunix
    neper_file_name = [neper_folder '/' test_name '_seeds' '.msh'];
else
    neper_file_name = [neper_folder '\' test_name '_seeds' '.msh'];
end

% open file
fileID = fopen(neper_file_name,'w+');

% write data
for i=1:size(seeds,1)
    fprintf(fileID,'%d\t',i);
    for j=1:size(seeds,2)
        fprintf(fileID,'%.16f\t',seeds(i,j));
    end
    fprintf(fileID,'\n');
end

% close file
fclose(fileID);


end

