function P1_plot_FragVolumes(Frag_ME,Impact_Data,Frag_Volume)

    for k=1:Impact_Data.N_frag_vol
        
        figure()
        set(gcf,'color','white');
        hold on; axis equal; axis off;
        view(30,40);

        title(['Frag Volume #' num2str(k)]);
        
        % plot RFs
        pltassi(trasm(eye(3),[0 0 0]'),'k','',1,3);
        pltassi(trasm(eye(3),[Frag_ME.l1x/2 Frag_ME.l1y/2 0]'),'r','',1,2);

        % plot domain
        drawMesh(Frag_ME.v_sph, Frag_ME.f_sph, 'FaceColor',[0 0 0],'FaceAlpha',0.002);
        
        % plot Frag Volume
%         drawPoint3d(Frag_Volume{k}.vi_sph,'.b');
%         for kk=1:size(Frag_Volume{k}.vi_sph,1)
%             text(Frag_Volume{k}.vi_sph(kk,1)+0.05,Frag_Volume{k}.vi_sph(kk,2),Frag_Volume{k}.vi_sph(kk,3), ...
%                  num2str(kk),'color','k','FontSize',10);
%         end
%         drawPoint3d(Frag_Volume{k}.ve_sph,'.r');
        
        drawMesh(Frag_Volume{k}.v_sph, Frag_Volume{k}.f_sph, 'FaceColor', 'g','FaceAlpha',0.5);

    end

end
