function baricenter = baricenterPoints3d(points)

baricenter = zeros(1,3);

for j=1:3
    baricenter(1,j) = mean(points(:,j));
end

end