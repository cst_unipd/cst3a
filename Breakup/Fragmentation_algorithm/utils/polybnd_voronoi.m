function [vornb,vorvx,Aaug,baug] = polybnd_voronoi(pos,bnd_pnts)
% -------------------------------------------------------------------------
% -------------------------------------------------------------------------
% [Voronoi neighbor,Voronoi vertices] = voronoi_3d(points, boundary)
% Given n points and a bounded space in R^2/R^3, this function calculates
% Voronoi neighbor/polygons associated with each point (as a generator).
% =========================================================================
% INPUTS:
% pos       points that are in the boundary      n x d matrix (n: number of points d: dimension) 
% bnd_pnts  points that defines the boundary     m x d matrix (m: number of vertices for the convex polytope
% boundary d: dimension)
% -------------------------------------------------------------------------
% OUTPUTS:
% vornb     Voronoi neighbors for each generator point:     n x 1 cells
% vorvx     Voronoi vertices for each generator point:      n x 1 cells
% =========================================================================
% This functions works for d = 2, 3
% -------------------------------------------------------------------------
% This function requires:
%       vert2lcon.m (Matt Jacobson / Michael Keder)
%       pbisec.m (by me)
%       con2vert.m (Michael Keder)
% -------------------------------------------------------------------------
% Written by Hyongju Park, hyongju@gmail.com / park334@illinois.edu

% pos = pos_seeds;

% In 3-D the boundary of the convex hull, K, is represented by a triangulation.
% This is a set of triangular facets in matrix format that is indexed with respect to
% the point array. Each row of the matrix K represents a triangle.

% K = convhulln(bnd_pnts);
% K = convhull(bnd_pnts,'simplify', true);
% bnd_pnts_ = bnd_pnts(K,:);
bnd_pnts = unique(bnd_pnts,'rows'); % delete reduntant vertices

[Abnd,bbnd] = vert2lcon(bnd_pnts); 
% obtain inequality constraints for convex polytope boundary
% vert2lcon.m by Matt Jacobson that is an extension of the 'vert2con' by
% Michael Keder

% check av
% Sbnd = [Abnd,bbnd];
% bnd_pnts2= MY_con2vert(Abnd,bbnd);
% plot3(bnd_pnts(:,1),bnd_pnts(:,2),bnd_pnts(:,3),'+'); hold on;
% plot3(bnd_pnts2(:,1),bnd_pnts2(:,2),bnd_pnts2(:,3),'o');

% % TRI_bnd = delaunayTriangulation(bnd_pnts(:,1),bnd_pnts(:,2),bnd_pnts(:,3));
% TRI_bnd = delaunay(bnd_pnts(:,1),bnd_pnts(:,2),bnd_pnts(:,3));
% TRI_bnd = unique(TRI_bnd,'rows');
% tetramesh(TRI_bnd,bnd_pnts);

% find Voronoi neighbors using Delaunay triangulation
switch size(pos,2)
    case 2
         TRI = delaunay(pos(:,1),pos(:,2));
%        TRI = delaunayTriangulation(pos(:,1),pos(:,2));
    case 3
         TRI = delaunay(pos(:,1),pos(:,2),pos(:,3));
%        TRI = delaunayTriangulation(pos(:,1),pos(:,2),pos(:,3));
end

% tetramesh(TRI(1:500,:),pos);
% TRI_ = unique(TRI,'rows');

n_cell = size(pos,1);
neib2 = cell(1,n_cell);  % memory allocation
neib3 = cell(1,n_cell);
vornb = cell(1,n_cell);

for i = 1:n_cell
    k = 0;
    for j = 1:size(TRI,1)
        if ~isempty(MY_intersect(i,TRI(j,:)))
            k = k + 1;
            neib2{i}(k,:) = MY_setdiff(TRI(j,:),i);
        end
    end
    neib3{i} = unique(neib2{i});
    if size(neib3{i},1) == 1
        vornb{i} = neib3{i};
    else
        vornb{i} = neib3{i}';
    end
end

% obtain perpendicular bisectors
A = cell(1,n_cell);
b = cell(1,n_cell);
for i = 1:n_cell
    k = 0;
    for j = 1:size(vornb{i},2)
        k = k + 1;
        [A{i}(k,:),b{i}(k,:)] = pbisec(pos(i,:), pos(vornb{i}(j),:));
    end
end

% obtain MY_intersection between bisectors + boundary
Aaug = cell(1,n_cell);
baug = cell(1,n_cell);
for i = 1:n_cell
    Aaug{i} = [A{i};Abnd];
    baug{i} = [b{i};bbnd];
end

% convert set of inequality constraints to the set of vertices at the
% intersection of those inequalities used 'con2vert.m' by Michael Kleder
V = cell(1,n_cell);
ID = cell(1,n_cell);
vorvx_ = cell(1,n_cell);
vorvx = cell(1,n_cell);

% for i =1:n_cell
%     Saug = [Aaug{i},baug{i}];
%     Saug = unique(Saug,'rows');
%     Aaug{i} = Saug(:,1:end-1);
%     baug{i} = Saug(:,end);
% end

for i =1:n_cell
    
   V{i}= MY_con2vert(Aaug{i},baug{i});
   
%    ID{i} = convhull(V{i}); % => original

   ID{i} = convhull(V{i});
   vorvx_{i} = V{i}(ID{i},:);
    
%   ID{i} = convhulln(V{i});
%   vorvx_{i} = V{i}(ID{i});
   
   % ADDED BY AV TO DELETE DOUBLE VERTICES
   if size(pos,2) == 3
       vorvx{i} = unique(vorvx_{i},'rows');
       %vorvx{i} = vorvx_{i};
       %vorvx{i} = V{i};
   else
       vorvx{i} = vorvx_{i};
   end
   
end

end
