function voronoi_solver_list()

global VORONOI_SOLVER_LIST;

% voronoi_solver_ID:

% 1 => Neper with bash
VORONOI_SOLVER_LIST.NEPER_BASH = 1;

% 2 => Voro++ with bash
VORONOI_SOLVER_LIST.VOROPP_BASH = 2;

% 3 => Voro++ from system
VORONOI_SOLVER_LIST.VOROPP_SYS = 3;

% 4 => Voro++ with MEX interface
VORONOI_SOLVER_LIST.VOROPP_MEX = 4;

end
