function P4_plot_Fragmentation_Cartesian_Hollow(Frag_ME,Impact_Data,domain_cart,fragments_cart,frags_in_volume)
%%
global VORONOI_SOLVER_LIST;
global voronoi_solver_ID;
global SHAPE_ID_LIST;

    figure()
    set(gcf,'color','white');
    hold on; axis equal; axis off;
    view(30,40);

    % ------------
    % plot Frag RF
    pltassi(trasm(eye(3),[0 0 0]'),'b','',1,0.5);

    % ------------------
    % plot impact points
    drawPoint3d(Impact_Data.I_POS_F,'pm');
%     plot3(I_PNT_F(:,1),I_PNT_F(:,2),I_PNT_F(:,3),'pb');
%     for i=1:N_impact
%         text(I_POS_F(i,1)+0.1,I_POS_F(i,2)-0.1,I_POS_F(i,3),num2str(i),'color','k','FontSize',20);
%     end
    
    % ----------------------
    % plot impact velocities
    for i=1:Impact_Data.N_impact
        drawVector3d(Impact_Data.I_POS_F(i,:), 1e-4*Impact_Data.I_VEL_F(i,:),'Color','m','LineWidth',2);
    end

    % --------------------
    % plot the Impacted ME
    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        drawMesh(Frag_ME.vi_cart_cc,Frag_ME.fi_cart_cc, 'FaceColor',[1 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        drawMesh(Frag_ME.ve_cart_cc,Frag_ME.fe_cart_cc, 'FaceColor',[1 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
    else
        drawMesh(Frag_ME.vi_cart,Frag_ME.fi_cart, 'FaceColor',[0 0 0],'FaceAlpha',0.2,'EdgeAlpha',0.1);
        drawMesh(Frag_ME.ve_cart,Frag_ME.fe_cart, 'FaceColor',[0 0 1],'FaceAlpha',0.05,'EdgeAlpha',0.1);
    end
    
    % --------------------------
    % plot fragmentation domains
    %{
    for k=1:N_frag_dom % N_frag_dom
        drawPoint3d(domain_cart{k}.vertex,'*');
        
        for i=1:domain_cart{k}.nedge
            % this plot the edges
            plot3(domain_cart{k}.edges{i}(:,1),domain_cart{k}.edges{i}(:,2),domain_cart{k}.edges{i}(:,3),...
                  'k','LineWidth',1);
        end        
    end
    %}
    
    %
    if Frag_ME.shape_ID == SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        % plot seeds
        for i=1:Impact_Data.N_frag_vol
            drawPoint3d(frags_in_volume{i}.seeds_cart,'.b');
            drawPoint3d(frags_in_volume{i}.seeds_cart_cyl,'.r');
        end
        % plot edges
        if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH
            for k=1:Impact_Data.N_frag_vol
                if isfield(frags_in_volume{k},'nedge')
                    for i=1:frags_in_volume{k}.nedge
                        plot3(frags_in_volume{k}.edges_cart_cyl{i}(:,1),frags_in_volume{k}.edges_cart_cyl{i}(:,2),frags_in_volume{k}.edges_cart_cyl{i}(:,3),...
                          '-','LineWidth',0.5);
                    end
                end
            end
        end
    else
        if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH
            for k=1:Impact_Data.N_frag_dom % N_frag_dom
                    % plot fragments edges
                if isfield(fragments_cart{k},'nedge')
                    for i=1:fragments_cart{k}.nedge
                        % this plot the edges
                        plot3(fragments_cart{k}.edges{i}(:,1),fragments_cart{k}.edges{i}(:,2),fragments_cart{k}.edges{i}(:,3),...
                              '-','LineWidth',0.5);
                    end
                end
            end
        else
            for i=1:Impact_Data.N_frag_vol
                drawPoint3d(frags_in_volume{i}.CoM,'*');
            end
        end
    end
    %}

end
