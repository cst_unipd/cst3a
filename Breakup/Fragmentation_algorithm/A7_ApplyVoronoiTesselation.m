function [fragments,domain,voronoi_check_flag,Frag_Domain] = A7_ApplyVoronoiTesselation(Frag_ME,Impact_Data,Frag_Domain)
%A7_ApplyVoronoiTesselation Apply the Voronoi tessellation algorithm
% 
% Syntax:  [fragments,domain] = A7_ApplyVoronoiTesselation(Frag_ME,Impact_Data,Frag_Domain)
%
% Inputs:
%    Frag_ME - Frag_ME structure
%    Impact_Data - Impact_Data structure
%    Frag_Domain - Frag_Domain structure
%
% Outputs:
%    fragments - fragments structure output of Neper
%    domain - domain structure output of Neper
%
% Other m-files required: GenerateNeperBashFile, GenerateNeperSeedsFile,
%                         GenerateNeperDomainFile 
% Subfunctions: none
% MAT-files required: size, system, num2str
%
% See also:
%
% Author:   Andrea Valmorbida, Ph.D.
%           Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%           University of Padova
% Email address: andrea.valmorbida@unipd.it
% Date: 2017/11/24
% Revision: 1.0
% Copyright: 2017 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2017/11/24 : first version by AV

%#codegen

global neper_folder;
global VORONOI_SOLVER_LIST;
global voronoi_solver_ID;
global cygwin64_folder;
global voropp_folder;
global hostname;

% voronoi_solver_ID:
% 1 => Neper with bash
% 2 => Voro++ with bash
% 3 => Voro++ from system
% 4 => Voro++ with MEX interface

% -----------------------------
% Create files for Neper/Voro++
% This is done for voronoi_solver_ID < 4
if voronoi_solver_ID < VORONOI_SOLVER_LIST.VOROPP_MEX
    voropp_command={};
    for i=1:Impact_Data.N_frag_dom %N_impact

        test_name = ['Frag_ME_shape',num2str(Frag_ME.shape_ID),'_ID', num2str(Frag_ME.object_ID_index) '_D' num2str(i)];
        domain_str = ['planes(' test_name '_domain.msh)'];
        if isSolidShape(Frag_ME.shape_ID)
            if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH % Neper
                nseeds = size(Frag_Domain{i}.seeds,1);
                dim = size(Frag_Domain{i}.seeds,2);            
                GenerateNeperBashFile(neper_folder,test_name,nseeds,dim,domain_str);
                GenerateNeperSeedsFile(neper_folder,test_name,Frag_Domain{i}.seeds);
                GenerateNeperDomainFile(neper_folder,test_name,Frag_Domain{i}.normals,Frag_Domain{i}.centros);
            else % Voro++ bash or system
                voropp_command{i}=GenerateVoroppBashFile(neper_folder,test_name,Frag_Domain{i}.normals,Frag_Domain{i}.centros,Frag_Domain{i}.v);
                GenerateVoroppSeedsFile(neper_folder,test_name,Frag_Domain{i}.seeds);
            end
        else
            if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH % Neper
                nseeds = size(Frag_Domain{i}.seeds_sph,1);
                dim = size(Frag_Domain{i}.seeds_sph,2);
                GenerateNeperBashFile(neper_folder,test_name,nseeds,dim,domain_str);
                GenerateNeperSeedsFile(neper_folder,test_name,Frag_Domain{i}.seeds_sph);
                GenerateNeperDomainFile(neper_folder,test_name,Frag_Domain{i}.normals_sph,Frag_Domain{i}.centros_sph);
            else % Voro++ bash or system
                voropp_command{i}=GenerateVoroppBashFile(neper_folder,test_name,Frag_Domain{i}.normals_sph,Frag_Domain{i}.centros_sph,Frag_Domain{i}.v_sph);
                GenerateVoroppSeedsFile(neper_folder,test_name,Frag_Domain{i}.seeds_sph);
            end
        end

    end
end


% ----------
% call NEPER

% cd to Neper folder
cd(neper_folder);

% call Neper
if voronoi_solver_ID <= VORONOI_SOLVER_LIST.VOROPP_BASH % either Neper or voro++ bash
    for i=1:Impact_Data.N_frag_dom 
        test_name = ['Frag_ME_shape',num2str(Frag_ME.shape_ID),'_ID', num2str(Frag_ME.object_ID_index) '_D' num2str(i)];
        if(isunix)
            system(['bash ' test_name '.sh > ' test_name '_LOG.txt']);
        else
             neper_folder_win=strrep(neper_folder,'\','/');
             if(strncmp(hostname,'fwfranale-01',12)==1)
                  system(['C:\cygwin64\bin\bash.exe --login -c "' [neper_folder_win,test_name] '.sh > ' [neper_folder_win,test_name] '.log "']);
             else
%            system([cygwin64_folder 'bash.exe --login -c "' [neper_folder_win,test_name] '.sh > ' [neper_folder_win,test_name] '.log "']);
                system([cygwin64_folder 'bash.exe --login -c "' [neper_folder_win,test_name] '.sh > ' [neper_folder_win,test_name] '.log "']);
             end
        %FUNZIONA!
         %system('C:\cygwin64\bin\bash --login -c "E:/AO_8507Catastrophic/CST_integration/CST2/Breakup/Fragmentation_algorithm/neper_files/Frag_ME_shape1_ID1_D1.sh"')
        end
    end
elseif voronoi_solver_ID == VORONOI_SOLVER_LIST.VOROPP_SYS
    for i=1:Impact_Data.N_frag_dom 
        if(isunix)
            system([voropp_command{i}, ' > ' test_name '_LOG.txt']);           
        else
             system([voropp_command{i},' > ' test_name '.log ']);
        end
    end
elseif voronoi_solver_ID == VORONOI_SOLVER_LIST.VOROPP_MEX
    
    % OUTPUT
    frag_id = cell(Impact_Data.N_frag_dom,1);
    frag_volume = cell(Impact_Data.N_frag_dom,1);
    frag_centroid = cell(Impact_Data.N_frag_dom,1);
    frag_nvert = cell(Impact_Data.N_frag_dom,1);
    nvert_tot = cell(Impact_Data.N_frag_dom,1);
    frag_c_hull = cell(Impact_Data.N_frag_dom,1);
    
    for i=1:Impact_Data.N_frag_dom
        
        [bnd_box,planes,seeds] = getVoroppInput(Frag_Domain{i},Frag_ME.shape_ID);
          
        [frag_id{i},frag_volume{i},frag_centroid{i},frag_nvert{i},nvert_tot{i},frag_c_hull{i}] = ...
            cst_voropp_mex_interface(bnd_box,[7 7 7],planes,seeds);
        [frag_id{i},frag_volume{i},frag_centroid{i},frag_nvert{i},nvert_tot{i},frag_c_hull{i}] = ...
            check_voropp_fragments(frag_id{i},frag_volume{i},frag_centroid{i},frag_nvert{i},nvert_tot{i},frag_c_hull{i});
        
    end
end
% cd to the main directory
cd('..');
cd('..');
cd('..');


% ----------------------------------
% import data from Neper .tess files

fragments = cell(1,Impact_Data.N_frag_dom);
domain = cell(1,Impact_Data.N_frag_dom);
exist_results_files = zeros(1,Impact_Data.N_frag_dom);

for i=1:Impact_Data.N_frag_dom 
    if voronoi_solver_ID == VORONOI_SOLVER_LIST.NEPER_BASH
        neper_tess_file = ['Frag_ME_shape',num2str(Frag_ME.shape_ID),'_ID', num2str(Frag_ME.object_ID_index) '_D' num2str(i),'.tess'];
        if exist(neper_tess_file,'file')==2
            [fragments{i},domain{i}] = loadNeperResultsFile(neper_folder,neper_tess_file);
            exist_results_files(i) = 1;
        else
            disp(['warning: ' neper_tess_file ' does not exist']);
        end
    elseif   voronoi_solver_ID == VORONOI_SOLVER_LIST.VOROPP_MEX
        
        % ncells
        ncells = length(frag_id{i});
        fragments{i}.ncells = ncells;
        
        % seeds
        if isSolidShape(Frag_ME.shape_ID)
            seeds = Frag_Domain{i}.seeds;
        else
            seeds = Frag_Domain{i}.seeds_sph;
        end
        nseeds = size(seeds,1);
        fragments{i}.nseeds = nseeds;
        
        % check corrupted cells
        kill_idx = [];
        if nseeds ~= ncells
            
            for k=1:nseeds
                if ~any(frag_id{i} == k)
                    kill_idx = [kill_idx k];
                end
            end
%             if ~isempty(kill_idx)
%                 for k=1:length(kill_idx)
%                     seeds(kill_idx(k),:) = [NaN NaN NaN]; %% <<< necessario NaN?
%                 end
%             end
        end
        % currupted cells idx
        fragments{i}.corrupted_idx_ = kill_idx;
        
%         if isSolidShape(Frag_ME.shape_ID)
%             Frag_Domain{i}.seeds = seeds;
%         else
%             Frag_Domain{i}.seeds_sph = seeds;
%         end
        fragments{i}.seeds = seeds;
        
        % vorvx
        fragments{i}.vorvx = cell(1,nseeds);
        fragments{i}.vorfc = cell(1,nseeds); %ncells
        fragments{i}.volume = -10+zeros(nseeds,1);
        fragments{i}.CoM = zeros(nseeds,3);
        for j=1:ncells
            if j>=2
                sum_nv_prev = sum(frag_nvert{i}(1:j-1));
            else
                sum_nv_prev = 0;
            end
            fragments{i}.vorvx{frag_id{i}(j)} = unique(frag_c_hull{i}(sum_nv_prev+(1:frag_nvert{i}(j)),:),'rows'); % <<<< unique solo qui e non di seguito!
            fragments{i}.volume(frag_id{i}(j),1) = frag_volume{i}(j);
            fragments{i}.CoM(frag_id{i}(j),:) = frag_centroid{i}(j,:);
        end
        
        % vorfc
        for j=1:nseeds % ncells
            fragments{i}.vorfc{j} = minConvexHull(fragments{i}.vorvx{j});
            if isempty(fragments{i}.vorfc{j})
                warning(['fragments{' num2str(i) '}.vorfc{' num2str(j) '} is empty']);
            end
        end
        
        domain{i} = [];
        exist_results_files(i) = 1;
        
    else % voro++ bash or sys
        voropp_results_file = ['Frag_ME_shape',num2str(Frag_ME.shape_ID),'_ID', num2str(Frag_ME.object_ID_index) '_D' num2str(i),'_seeds.msh.vol'];
        if exist(voropp_results_file,'file')==2
            [fragments{i},domain{i}] = loadVoroppResultsFile(neper_folder,voropp_results_file);
            exist_results_files(i) = 1;
        else
            disp(['warning: ' voropp_results_file ' does not exist']);
        end
        
    end
end

% compute voronoi_check_flag
if sum(exist_results_files)==Impact_Data.N_frag_dom
    voronoi_check_flag = 1;
else
    voronoi_check_flag = 0;
end

end
