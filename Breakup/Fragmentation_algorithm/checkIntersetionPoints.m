function [pppoints,fix_flag] = checkIntersetionPoints(pppoints,ppline,hollow_center,shape_ID,shape)

global SHAPE_ID_LIST;

fix_flag = 0;

if sum(sum(isnan(pppoints))) > 0
    % a ppline does not intersect the internal sphere
    for kk = 1:size(pppoints,1)
        if sum(isnan(pppoints(kk,:)))>0
            %disp(kk);
            proj_pnt = projPointOnLine3d(hollow_center,ppline(kk,:));
            proj_line = createLine3d(hollow_center,proj_pnt);
            switch shape_ID
                case SHAPE_ID_LIST.HOLLOW_SPHERE
                    intersect_points = intersectLineSphere(proj_line,shape);
                case SHAPE_ID_LIST.HOLLOW_CYLINDER
                    intersect_points = intersectLinesCylinder(proj_line,shape);
                case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
                    intersect_points = intersectLineEllipsoid(proj_line,shape);
            end
            if ( vectorNorm3d(intersect_points(1,:)-proj_pnt) < vectorNorm3d(intersect_points(2,:)-proj_pnt) )
                intersect_point = intersect_points(1,:);
            else
                intersect_point = intersect_points(2,:);
            end
            pppoints(kk,:) = intersect_point;
        end
    end
    %disp('fixed internal intersection points');
    fix_flag = 1;
end

end