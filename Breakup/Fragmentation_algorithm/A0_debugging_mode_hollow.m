% A0_debugging_mode_hollow.m

debug_verbose = 1;                      % <<=== SET THIS FOR DEBUGGING
frag_plot_flag = 1;                     % <<=== SET THIS FOR DEBUGGING

% count = shape ID
% 3 - HOLLOW SPHERE
% 5 - HOLLOW CYLINDER
% 7 - HOLLOW ELLIPSOID
count = 5;                              % <<=== SET THIS FOR DEBUGGING

% test case: 1-10
test_case = 4;                          % <<=== SET THIS FOR DEBUGGING

fprintf('==>\tDEBUGGING MODE:\n');
switch count
    case SHAPE_ID_LIST.HOLLOW_SPHERE
        fprintf('\tHOLLOW SPHERE WITH ');
%     case SHAPE_ID_LIST.HOLLOW_CYLINDER
%         fprintf('\tHOLLOW CYLINDER WITH ');
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID
        fprintf('\tHOLLOW CYLINDER (ELLIPSOID) WITH ');
end

switch test_case
    case 1  % 1 impact => ok for sph, cyl, elp
            % the north pole is involved
        nimpact = 1;
    case 2  % 2 impacts, => ok for sph, cyl, elp
            % intersection of 2 frag volumes (2 and 4)
        nimpact = 2;
    case 3  % 1 impact, => ok for sph, cyl, elp => problem with Neper
            % not complete intersection of the cone with the internal shape
        nimpact = 1;
    case 4  % 1 impact,  => ok for sph, cyl, elp => problem with Neper
            % not complete intersection of the cone with both internal and external shapes
        nimpact = 1;
    case 5  % 1 impact,  => ok for sph, TBV cyl, ok elp
            % complete fragmentation of the hollow shape
        nimpact = 1;
    case 6  % 2 impacts, => ok for sph, TBV cyl, ok elp
            % 1st impact => complete fragmentation of the hollow shape
            % 2nd impact => local fragmentation
        nimpact = 2;
    case 7  % 1 impact,  => ok cyl (not envisaged for sph and elp)
            % part of the intersection cone is outside the upper or lower
            % bound of the cylinder
        nimpact = 1;
    case 8  % 1 impact,  => TBV cyl (not envisaged for sph and elp)
            % part of the intersection cone is outside the upper or lower
            % bound of the cylinder, and not complete intersection
            % of the cone with both internal and external shapes
        nimpact = 1;
    case 9  % 2 impacts, => ok for sph, TBV cyl, ok elp
            % 1st impact => complete fragmentation of the hollow shape
            % 2nd impact => local fragmentation
            % THE FRAG DOMAIN IS THE WHOLE HOLLOW SHAPE
        nimpact = 2;
    case 10  % 2 impacts, => ok for sph, TBV cyl, ok elp
            % 1st impact => complete fragmentation of the hollow shape
            % 2nd impact => complete fragmentation of the hollow shape
            % THE FRAG DOMAIN IS THE WHOLE HOLLOW SHAPE
        nimpact = 2;
    case 11 % 1 impact => impact on the base
        nimpact = 1;
end
fprintf('%d IMPACT(S) - TEST CASE %d\n',nimpact,test_case);

% displacement vector from SR
SR_delta = [0 0 0]';

switch count

    % HOLLOW SPHERE
    case SHAPE_ID_LIST.HOLLOW_SPHERE 
        COLLISION_DATA(count).TARGET.ID = count;    % 3rd ME
        COLLISION_DATA(count).TARGET.type = 1;      % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = 1; % externalradius
        ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = 0;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = 0;
        ME(object_ID_index).GEOMETRY_DATA.thick = 0.2;
        ME(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [1 0 0 0]';

    % HOLLOW CYLINDER
    %
    case SHAPE_ID_LIST.HOLLOW_CYLINDER 
        COLLISION_DATA(count).TARGET.ID = count;    % 5th ME
        COLLISION_DATA(count).TARGET.type = 1;      % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = 0.5;    % radius
        ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = 0;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = 2.5;      % height
        ME(object_ID_index).GEOMETRY_DATA.thick = 0.2;
        ME(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [1 0 0 0]';
    %}
    % HOLLOW ELLIPSOID
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID 
        COLLISION_DATA(count).TARGET.ID = count;    % 7th ME
        COLLISION_DATA(count).TARGET.type = 1;      % ME
        object_ID_index = COLLISION_DATA(count).TARGET.ID;
        ME(object_ID_index).GEOMETRY_DATA.shape_ID = count;
%         ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = 1.0;    % x semi-axis
%         ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = 1.2;    % y semi-axis
%         ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = 1.7;    % z semi-axis
        ME(object_ID_index).GEOMETRY_DATA.dimensions(1) = 1.0;    % radius
        ME(object_ID_index).GEOMETRY_DATA.dimensions(2) = 0;
        ME(object_ID_index).GEOMETRY_DATA.dimensions(3) = 2.5;      % height
        ME(object_ID_index).GEOMETRY_DATA.thick = 0.2;            % thickness
        ME(object_ID_index).DYNAMICS_DATA.cm_coord = [3.5 0 5.8]';
        ME(object_ID_index).DYNAMICS_DATA.quaternions = [1 0 0 0]';
end

ME(object_ID_index).FRAGMENTATION_DATA.breakup_flag = 1;
ME(object_ID_index).FRAGMENTATION_DATA.cMLOSS = 0.15;
ME(object_ID_index).FRAGMENTATION_DATA.cELOSS = 0.55;
ME(object_ID_index).FRAGMENTATION_DATA.c_EXPL = 0.77;
ME(object_ID_index).GEOMETRY_DATA.mass = 50;
ME(object_ID_index).GEOMETRY_DATA.mass0 = 50;
ME(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_ID = 4; %<<<<=== 0 random cartesian, 4 random spherical
ME(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param1 = 100;
ME(object_ID_index).FRAGMENTATION_DATA.seeds_distribution_param2 = 10;   

COLLISION_DATA(count).TARGET.mass = ME(object_ID_index).GEOMETRY_DATA.mass;
COLLISION_DATA(count).TARGET.c_EXPL = ME(object_ID_index).FRAGMENTATION_DATA.c_EXPL;
COLLISION_DATA(count).TARGET.Ek = 1;
COLLISION_DATA(count).F_L = 0.5;
COLLISION_DATA(count).Ek_TOT = 1;


% -------------
% IMPACT POINTS
switch count

    % HOLLOW SPHERE
    case SHAPE_ID_LIST.HOLLOW_SPHERE 
        a = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % external diameter
        b = a;
        c = a;
        switch test_case
            case 1
                impact_points_F = [1.1*a b/2 c/2];
            case 2
                impact_points_F = [1.1*a b/2 c/2; ...
                                   a/2+0.1 1.1*b/2 1.1*c/2];
            case 3
                impact_points_F = [1.0*a b/2 c/2];
            case 4
                impact_points_F = [1.0*a b/2 c/2];
            case 5
                impact_points_F = [1.1*a b/2 c/2];
            case 6
                impact_points_F = [1.1*a b/2 c/2; ...
                                   a b c/2];
            case 7
                error('test case 7 not envisaged for hollow spheres');
            case 8
                error('test case 8 not envisaged for hollow spheres');
            case 9
                impact_points_F = [1.1*a b/2 c/2; ...
                                   0.75*a b c/2];
            case 10
                impact_points_F = [1.1*a b/2 c/2; ...
                                   0.75*a b c/2];
            case 11
                error('test case 11 not envisaged for hollow spheres');
        end

    % HOLLOW CYLINDER
    %
    case SHAPE_ID_LIST.HOLLOW_CYLINDER 
        a = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % external diameter
        b = a;
        c = ME(object_ID_index).GEOMETRY_DATA.dimensions(3);   % height
        switch test_case
            case 1
                impact_points_F = [a b/2 c/2];
            case 2
                impact_points_F = [a b/2 c*0.4; ...
                                   a/2+0.1 1.1*b 1.1*c/2];
            case 3
                impact_points_F = [1.01*a b/2 c/2];
            case 4
                impact_points_F = [1.01*a b/2 c/3];
            case 5
                impact_points_F = [1.1*a 1*1*b/2 1.2*c/3];
            case 6
                error('todo');
            case 7
                impact_points_F = [a b/2 c/2];
            case 8
                impact_points_F = [a b/2 1.2*c/2];
            case 9
                error('todo');
            case 10
                error('todo');
            case 11
                impact_points_F = [a/2 b/2 0];
        end
    %}
        
    % HOLLOW ELLIPSOID    
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID 
        a = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(1); % x diameter
        b = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(2); % y diameter;
        c = 2*ME(object_ID_index).GEOMETRY_DATA.dimensions(3); % z diameter
        switch test_case
            case 1
                impact_points_F = [a b/2 3*c/5];
            case 2
                impact_points_F = [a b/2 c/2; ...
                                   a/2+0.1 1.1*b/2 1.1*c/2];
            case 3
                impact_points_F = [a b/2 c/2];
            case 4
                impact_points_F = [a b/2 c/2];
            case 5
                impact_points_F = [a b/2 c/2];
            case 6
                impact_points_F = [a b/2 c/2; ...
                                   a b   1.2*c/3];
            case 7
                error('test case 7 not envisaged for hollow ellipsoids');
            case 8
                error('test case 8 not envisaged for hollow ellipsoids');
            case 9
                impact_points_F = [a b/2 c/2; ...
                                   0.75*a b   1.2*c/3];
            case 10
                impact_points_F = [a b/2 c/2; ...
                                   0.75*a b   1.2*c/3];
            case 11
                error('test case 11 not envisaged for hollow ellipsoids');
        end
end

for i=1:size(impact_points_F,1)
    COLLISION_DATA(count).IMPACTOR(i).POINT = [ impact_points_F(i,:) + ...
                            - [a/2, b/2, c/2] + ...
                            + [ME(object_ID_index).DYNAMICS_DATA.cm_coord(1), ...
                               ME(object_ID_index).DYNAMICS_DATA.cm_coord(2), ...
                               ME(object_ID_index).DYNAMICS_DATA.cm_coord(3)] ]; % ### point is a row vector
    COLLISION_DATA(count).IMPACTOR(i).Ek = 1;
    COLLISION_DATA(count).IMPACTOR(i).mass = 1;
    COLLISION_DATA(count).IMPACTOR(i).type = 0;
end

% ---------------
% IMPACT VELOCITY
switch count

    % HOLLOW SPHERE
    case SHAPE_ID_LIST.HOLLOW_SPHERE 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.05 +0.35]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.18 +0.2]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.1 -1 -0.1]';
            case 3
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.21 +0.25]';
            case 4
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.84 +0.25]';
            case 5 
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.4*1 0.3]';
            case 6 
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.4*1 0.2]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 -1 -0.05]';
            case 9
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.4*1 0.2]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.3 -1 -0.05]';
            case 10
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.4*1 0.2]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.3 -1 -0.05]';
        end

    %
    % HOLLOW CYLINDER
    case SHAPE_ID_LIST.HOLLOW_CYLINDER 
        switch test_case
            case 1
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.2 +0.1]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.2 +0.1]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.1 -1 -0.1]';
            case 3
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.48 +0.15]';
            case 4
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.71 +0.15]';
            case 5
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.2 +0.3]';
            case 6
                error('todo');
            case 7
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.2 +0.5]';
            case 8
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.8 -0.8]';
            case 9
                error('todo');
            case 10
                error('todo');
            case 11
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [0 0 1]';
        end
    %}
        
    % HOLLOW ELLIPSOID    
    case SHAPE_ID_LIST.HOLLOW_ELLIPSOID 
        switch test_case
            case 1 % 1 impact
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.05 +0.35]';
            case 2
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.05 +0.35]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.2 -1 -0.1]';
            case 3
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.5 +0.3]';
            case 4
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.6 +0.3]';
            case 5
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.6 +0.3]';
            case 6
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.6 +0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-1 -0.8 +0.4]';
            case 9
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.6 +0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.2 -0.8 +0.4]';
            case 10
                COLLISION_DATA(count).IMPACTOR(1).v_rel = [-1 -0.6 +0.3]';
                COLLISION_DATA(count).IMPACTOR(2).v_rel = [-0.2 -0.8 +0.4]';
        end
end

clear a b c;





