%> @file   loadNeperResultsFile.m
%> @brief  Import data from a Neper .tess results file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Function used to import data from a Neper results file (.tess)
%>
%> @param neper_folder Path to the neper folder with the .tess file
%> @param neper_tess_file Name of the Neper .tess file to be loaded
%>
%> @retval fragments Structure with fragments
%> @retval domain Structure with the fragmentation domain
%>
%==========================================================================
function [fragments,domain] = loadNeperResultsFile(neper_folder,neper_tess_file)
%loadNeperResultsFile Import data from a Neper .tess results file


%% open .tess file
% fid = fopen([neper_folder '/' neper_tess_file]);
fid = fopen(fullfile(neper_folder, neper_tess_file));

%%
fgetl(fid);  % ***tess  
fgetl(fid);  % **format
line = fgetl(fid);  % format
format = str2double(line);
fgetl(fid);  % **general
line = fgetl(fid);  % general
c = textscan(line,'%s');
dim = str2double(c{1}{1});
fgetl(fid);  % **cell
line = fgetl(fid);  % cell
ncell = str2double(line);
id_vect = 1:ncell;

while ~strcmp('  *crysym',line)
    line = fgetl(fid);  % *id
end
line = fgetl(fid);  % crysym
crysym = line;


%% SEED

fgetl(fid);  % *seed
seeds = zeros(ncell,dim);
for i=1:ncell
line = fgetl(fid);  % seed
c = textscan(line,'%f');
seeds(i,:) = [c{1}(2:4)]';
end

%% ORI
fgetl(fid);  % *ori
fgetl(fid);  % ori
ori = zeros(ncell,3);

for i=1:ncell
    line = fgetl(fid);  % seed
    c = textscan(line,'%f');
    ori(i,:) = [c{1}]';
end

%% VERTEX
fgetl(fid);  % *vertex
line = fgetl(fid);  % vertex
nvertex = str2double(line);
vertex = zeros(nvertex,3);

for i=1:nvertex
    line = fgetl(fid);  % seed
    c = textscan(line,'%f');
    vertex(i,:) = [c{1}(2:4)]';
end

%%
%{
figure()
hold on;

plot3(vertex(:,1),vertex(:,2),vertex(:,3),'*r');

for i=1:nvertex
txt = num2str(i);
text(vertex(i,1)+0.05,vertex(i,2),vertex(i,3),txt);
end

plot3(seeds(:,1),seeds(:,2),seeds(:,3),'*b');
for i=1:ncell
txt = num2str(i);
text(seeds(i,1)+0.05,seeds(i,2),seeds(i,3),txt);
end

axis equal;
%}

%% EDGE
fgetl(fid);  % **edge
line = fgetl(fid);  % edge
nedge = str2double(line);
edge_vert_list = zeros(nedge,2);
edges = cell(1,nedge);

for i=1:nedge
    line = fgetl(fid);  % seed
    c = textscan(line,'%f');
    edge_vert_list(i,:) = [c{1}(2:3)]';
    edges{i} = [vertex(edge_vert_list(i,1),1),vertex(edge_vert_list(i,1),2),vertex(edge_vert_list(i,1),3);...
                vertex(edge_vert_list(i,2),1),vertex(edge_vert_list(i,2),2),vertex(edge_vert_list(i,2),3)];
end


%% FACE
fgetl(fid);  % **face
line = fgetl(fid);  % face
nface = str2double(line);
face_vert_list = cell(1,nface);
faces = cell(1,nface);

for i=1:nface
    line = fgetl(fid);  % face_id number_of_vertices ver_1 ver_2 ... 
    c = textscan(line,'%f');
    face_vert_list{i} = [c{1}(3:3+c{1}(2)-1)]';
    for j=1:length(face_vert_list{i})
        faces{i}(j,:) = [vertex(face_vert_list{i}(j),1), ...
                         vertex(face_vert_list{i}(j),2), ...
                         vertex(face_vert_list{i}(j),3)];
    end
    fgetl(fid);
    fgetl(fid);
    fgetl(fid);
end

%% POLYHEDRON
fgetl(fid);  %  **polyhedron
line = fgetl(fid);  % total_number_of_polyhedra 
npoly = str2double(line);
poly_face_list = cell(1,npoly);
poly_vert_list = cell(1,npoly);
vorvx = cell(1,npoly);

for i=1:npoly
    line = fgetl(fid);  % face_id number_of_vertices ver_1 ver_2 ... 
    c = textscan(line,'%f');
    poly_face_list{i} = [c{1}(3:3+c{1}(2)-1)]';
    for j=1:length(poly_face_list{i})
        poly_vert_list{i} = [poly_vert_list{i}, face_vert_list{abs(poly_face_list{i}(j))}];
        poly_vert_list{i} = unique(poly_vert_list{i});
    end
    for j=1:length(poly_vert_list{i})
        vorvx{i}(j,:) = [vertex(poly_vert_list{i}(j),1), ...
                         vertex(poly_vert_list{i}(j),2), ...
                         vertex(poly_vert_list{i}(j),3)];
    end
    
    
end


%% DOMAIN VERTEX
line = fgetl(fid);  %  **domain
fgetl(fid); fgetl(fid); fgetl(fid);
line = fgetl(fid);  % total_number_of_dom_vertices
dom_nvertex = str2double(line);
dom_vertex = zeros(dom_nvertex,3);

for i=1:dom_nvertex
    line = fgetl(fid);  % dom_ver_id dom_ver_x dom_ver_y dom_ver_z dom_ver_label 
    c = textscan(line,'%f');
    dom_vertex(i,:) = [c{1}(2:4)]';
    fgetl(fid);
end


%% DOMAIN EDGE
fgetl(fid);  %  *edge
line = fgetl(fid);  % edge
dom_nedge = str2double(line);
dom_edge_vert_list = zeros(dom_nedge,2);
dom_edges = cell(1,dom_nedge);

for i=1:dom_nedge
    fgetl(fid); fgetl(fid); line = fgetl(fid);
    c = textscan(line,'%f');
    dom_edge_vert_list(i,:) = [c{1}(1:2)]';
    dom_edges{i} = [dom_vertex(dom_edge_vert_list(i,1),1),dom_vertex(dom_edge_vert_list(i,1),2),dom_vertex(dom_edge_vert_list(i,1),3);...
                    dom_vertex(dom_edge_vert_list(i,2),1),dom_vertex(dom_edge_vert_list(i,2),2),dom_vertex(dom_edge_vert_list(i,2),3)];
end


%% DOMAIN FACE
fgetl(fid);  % **face
line = fgetl(fid);  % face
dom_nface = str2double(line);
dom_face_vert_list = cell(1,dom_nface);
dom_faces = cell(1,dom_nface);

for i=1:dom_nface
    line = fgetl(fid);  % face_id number_of_vertices ver_1 ver_2 ... 
    c = textscan(line,'%f');
    dom_face_vert_list{i} = [c{1}(3:3+c{1}(2)-1)]';
    for j=1:length(dom_face_vert_list{i})
        dom_faces{i}(j,:) = [dom_vertex(dom_face_vert_list{i}(j),1), ...
                             dom_vertex(dom_face_vert_list{i}(j),2), ...
                             dom_vertex(dom_face_vert_list{i}(j),3)];
    end
    fgetl(fid); fgetl(fid); fgetl(fid); fgetl(fid);
end

%% Close .tess file
fclose(fid);


%% Output

fragments.ncells = ncell;
fragments.seeds = seeds;
fragments.ori   = ori;
fragments.nvertex = nvertex;
fragments.vertex = vertex;
fragments.nedge  = nedge;
fragments.edge_vert_list = edge_vert_list;
fragments.edges = edges;
fragments.nface = nface;
fragments.face_vert_list = face_vert_list;
fragments.faces = faces;
fragments.npoly = npoly;
fragments.poly_face_list = poly_face_list;
fragments.poly_vert_list = poly_vert_list;
fragments.vorvx = vorvx;
 
domain.nvertex = dom_nvertex;
domain.vertex = dom_vertex;
domain.nedge = dom_nedge;
domain.edge_vert_list = dom_edge_vert_list;
domain.edges = dom_edges;
domain.nface = dom_nface;
domain.face_vert_list = dom_face_vert_list;
domain.faces = dom_faces;


end

