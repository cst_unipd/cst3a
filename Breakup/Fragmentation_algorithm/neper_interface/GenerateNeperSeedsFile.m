%> @file   GenerateNeperSeedsFile.m
%> @brief  Generate Neper seeds file
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%==========================================================================
%> @brief Generate Neper seeds file
%> 
%> Thsi function creates in the neper_folder a seeds .msh file with name
%> %<test_name>_seeds.msh
%>
%> @param neper_folder Path to the neper folder with the .tess file
%> @param test_name name of the test.
%> @param seeds matrix with the seeds positions (Nx3 matrix)
%>
%==========================================================================
function [ ] = GenerateNeperSeedsFile(neper_folder,test_name,seeds)
%GenerateNeperSeedsFile 

% neper file name
if isunix
    neper_file_name = [neper_folder '/' test_name '_seeds' '.msh'];
else
    neper_file_name = [neper_folder '\' test_name '_seeds' '.msh'];
end
    
fileID = fopen(neper_file_name,'w+');

for i=1:size(seeds,1)
    for j=1:size(seeds,2)
        fprintf(fileID,'%.12f\t',seeds(i,j));
    end
    fprintf(fileID,'\n');
end


%fprintf(fileID,'\n');

fclose(fileID);

end

