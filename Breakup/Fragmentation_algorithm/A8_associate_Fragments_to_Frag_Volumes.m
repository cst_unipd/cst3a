function frags_in_volume = A8_associate_Fragments_to_Frag_Volumes(Frag_ME,Impact_Data,Frag_Volume)

% ---------------------------------------
% Associate fragments to each Frag Volume
frags_in_volume = cell(1,Impact_Data.N_impact);
% k => index referred to the Frag Domain
% i => index referred to the Frag Volume
% j => index referred to the intersection group
% ii => index referred to each fragment

for k = 1:N_frag_dom
    if length(intersect_groups{k})>1
        idx_start = 1;
        idx_end = 0;
        for j = 1:length(intersect_groups{k})
            i = intersect_groups{k}(j);
            if j>1
                idx_start = idx_end + 1;
            end
            idx_end = idx_start - 1 + Frag_Volume{i}.nseeds;
            frags_in_volume{i}.ncell = Frag_Volume{i}.nseeds;
            frags_in_volume{i}.seeds = fragments{k}.seeds(idx_start:idx_end,:);
            frags_in_volume{i}.vorvx = cell(1,Frag_Volume{i}.nseeds);
            for ii=1:Frag_Volume{i}.nseeds
                frags_in_volume{i}.vorvx{ii} = unique(fragments{k}.vorvx{idx_start-1+ii},'rows'); % ##
            end
        end
    else
        i = intersect_groups{k};
        frags_in_volume{i}.ncell = fragments{k}.ncell;
        frags_in_volume{i}.seeds = fragments{k}.seeds;
        for ii=1:length(fragments{k}.vorvx)
            frags_in_volume{i}.vorvx{ii} = unique(fragments{k}.vorvx{ii},'rows');
        end
    end
end

end
