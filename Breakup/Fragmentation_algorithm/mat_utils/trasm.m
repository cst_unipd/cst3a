%> @file  trasm.m
%> @brief Compute the roto-translation operator (matrix)
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)

%> Copyright 2017 CISAS - UNIVERSITY OF PADOVA

%==========================================================================
%> @brief Compute the roto-translation operator (matrix)
%>
%> @param Rij rotation matrix from i to j
%> @param Pij position of the origin of reference frame (RF) i wrt the j RF
%>
%> @retval Tij the roto-translation operator that transforms i RF to j RF
%>
%==========================================================================
function Tij = trasm(Rij,Pij) %#codegen

Tij=eye(4,4);
Tij(1:3,1:3)=Rij;
Tij(1:3,4)=Pij;

end