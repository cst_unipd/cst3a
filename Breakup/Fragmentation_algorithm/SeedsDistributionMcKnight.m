%> @file   SeedsDistributionMcKnight.m
%> @brief  McKnight's Seeds Distribution 
%> @author Andrea Valmorbida (andrea.valmorbida@unipd.it)
%==========================================================================
%> @brief McKnight's Seeds Distribution 
%>
%> McKnight's Seeds Distribution 
%>
%> @param shape_ID Shape ID
%> @param rho Density of the impacted object
%> @param Frag_ME Frag_ME structure (see Fragmerntation Algorithm variable list)
%> @param Frag_Data Frag_Data structure (see Fragmerntation Algorithm variable list)
%> @param i i-th Frag. Volume
%> @param Frag_Volume_v Vertices that define the i-th Frag. Volume
%>
%> @retval seeds Ns x 3 matrix with seeds coordinates
%>
%==========================================================================
function seeds = SeedsDistributionMcKnight(shape_ID,rho,...
                                        Frag_ME, Frag_Data,i,Frag_Volume_v)

global SHAPE_ID_LIST;

switch shape_ID
    
    case SHAPE_ID_LIST.SOLID_BOX
        
        % compute parameters used for the seeds distribution 
        mf_min = 1e-3; % kg - minimum mass of the fragments <<<=== TUNING PARAMETER
        df_min = (6*mf_min/rho/pi)^(1/3); % df_min = 0.005;
        k_a = 3;
        h_ext = Frag_ME.c;
        k_tot = ceil(h_ext/df_min) + 1;

        % compute seeds layers
        seeds_layer = cell(1,k_tot);
        for k=1:k_tot
            seeds_layer{k} = GenerateSeedsLayer('irregular',k,k_a,df_min, ...
                                            Frag_Data(i,1),Frag_Data(i,2), ...
                                            Frag_Data(i,4),h_ext);
            seeds_layer{k} = getPointsWithinBoundary(seeds_layer{k},Frag_Volume_v);
        end

        % compute seeds matrix
        seeds = [];
        for k=1:k_tot
            seeds = [seeds; seeds_layer{k}];
        end
        
        % get the seeds that are within the Frag_Volume
        seeds = getPointsWithinBoundary(seeds,Frag_Volume_v);
        seeds = unique(seeds,'rows');
        
        if isempty(seeds)
            [seeds, ~] = polyhedronCentroidVolume(Frag_Volume_v); % GS + FF
        end
        
    otherwise
        % <<<=== TUNING
        error('McKnight seeds distribution available only for SOLID BOX/PLATE (for now)');
end

end

