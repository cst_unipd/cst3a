%> @file  unbroken_targets.m
%> @brief ME FRAGMENTATION ALGORITHM v04 - main
%> @author dr. L. Olivieri (lorenzo.olivieri@unipd.it)
%======================================================================
%> @brief Function used to manage non fragmenting impacts
%>
%> This algorithm recieves the COLLISION_DATA from the CST breakup main 
%> and investigate the non fragmenting collisions, managing bubbles and 
%> bouncing objects.
%>
%> @param C_DATA: info on collisions detected from tracking
%> @param C_DATA1: info on non fragmenting collisions
%> @param KILL_LIST: all killed elements
%>
%> @retval KILL_LIST: all killed elements
%> @retval ME, FRAGMENTS, BUBBLE, HOLES update
%>
%======================================================================
function [C_DATA]=unbroken_targets(C_DATA,C_DATA1,KILL_LIST)

global BUBBLE
global ME_FR FRAGMENTS_FR
global HOLES

%% Studying the bubbles
n_C=length(C_DATA1);
for i=n_C:-1:1
    n_C1=length(C_DATA1(i).IMPACTOR);
    for j=n_C1:-1:1
        if C_DATA1(i).IMPACTOR(j).type==2 
            HOLES_creation_flag=bubble_damage(C_DATA1(i),j,BUBBLE(C_DATA1(i).IMPACTOR(j).ID),ME_FR(C_DATA1(i).TARGET.ID));
            if(HOLES_creation_flag)
                disp(['POTENTIAL FAILURE!: Hole (',num2str(length(HOLES)),') created on ME(',num2str(C_DATA1(i).TARGET.ID),') due to BUBBLE impact'])
            end
            [v_NEW_B,~]=velocity_model_breakup(C_DATA1,i);
            BUBBLE_v_LOSS=C_DATA1(i).IMPACTOR(j).v_loss;
            BUBBLE_v_NEW=C_DATA1(i).IMPACTOR(j).vel-BUBBLE_v_LOSS;
            v_NEW_B2 = (ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass*v_NEW_B+...
                BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass*BUBBLE_v_NEW)/...
                (ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass+BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass);
            C_DATA1(i).TARGET.v_loss = C_DATA1(i).TARGET.v_loss + (v_NEW_B - v_NEW_B2);
            ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass=...
                ME_FR(C_DATA1(i).TARGET.ID).GEOMETRY_DATA.mass+BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass;
            BUBBLE(C_DATA1(i).IMPACTOR(j).ID).GEOMETRY_DATA.mass=0;
            
            C_DATA1(i).IMPACTOR(j)=[];
            if isempty(C_DATA1(i).IMPACTOR)
                [v_NEW,~]=velocity_model_breakup(C_DATA1,i);
                ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
                 C_DATA1(i).TARGET=[];
             end
        end
    end
end

%% IMPACTORS have olready fragmented
for i=n_C:-1:1
    if(~isempty(C_DATA1(i).TARGET))
        n_C0=length(C_DATA1(i).IMPACTOR);
        flag_del=0;
        for i1=1:n_C0
            n_C1=length(C_DATA);
            for j=1:n_C1
                if ( ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(i).IMPACTOR)  && C_DATA1(i).IMPACTOR(i1).type==C_DATA(j).TARGET.type &&  C_DATA1(i).IMPACTOR(i1).ID==C_DATA(j).TARGET.ID )
                    if C_DATA(j).F_L ~= 0
                        flag_del=1;
                    end
                end
            end
        end
        if flag_del==1
            [v_NEW,~]=velocity_model_breakup(C_DATA1,i);
            if C_DATA1(i).TARGET.type==1
                ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
            elseif C_DATA1(i).TARGET.type==0
                FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v_NEW;
            end
            C_DATA1(i).TARGET=[];
        end
    end
end

%% BOUNCING of single impacts
for i=n_C:-1:2                   
    if ~isempty(C_DATA1(i).TARGET) 
        if length(C_DATA1(i).IMPACTOR)==1
            for j=1:i-1
                if ( ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(j).TARGET) && ~isempty(C_DATA1(i).IMPACTOR) && C_DATA1(i).IMPACTOR.type==C_DATA1(j).TARGET.type &&  C_DATA1(i).IMPACTOR.ID==C_DATA1(j).TARGET.ID && length(C_DATA1(j).IMPACTOR)==1)
                    m1= C_DATA1(i).TARGET.mass;
                    m2= C_DATA1(j).TARGET.mass;
                    [v1,~]=velocity_model_breakup(C_DATA1,i);
                    [v2,~]=velocity_model_breakup(C_DATA1,j);
                    E1=m1*(norm(C_DATA1(i).TARGET.vel-C_DATA1(i).V_CM))^2;
                    E2=m2*(norm(C_DATA1(j).TARGET.vel-C_DATA1(j).V_CM))^2;
                    [E,i_E]=max([C_DATA1(i).TARGET.type*E2/E1;C_DATA1(j).TARGET.type*E1/E2]);
                    
                    cond1= C_DATA1(i).TARGET.type==1 && m2/m1<0.001 && C_DATA1(i).IMPACTOR.V_imp>0.9;
                    cond2= C_DATA1(j).TARGET.type==1 && m1/m2<0.001 && C_DATA1(j).IMPACTOR.V_imp>0.9;
                    if ( (C_DATA1(i).TARGET.type+C_DATA1(j).TARGET.type)==1 && (E<0.1 || cond1 || cond2 ) ) 
                        ii=i;
                        jj=j;
                        if i_E==2 || cond2
                            ii=j;
                            jj=i;
                        end
                        
                        [v_NEWii,~]=velocity_model_breakup(C_DATA1,ii);
                        [v_NEWjj,~]=velocity_model_breakup(C_DATA1,jj);
                        m_TOT=C_DATA1(ii).TARGET.mass+C_DATA1(jj).TARGET.mass;
                        Q1=C_DATA1(ii).TARGET.mass*v_NEWii;
                        Q2=C_DATA1(jj).TARGET.mass*v_NEWjj;
                        vel=(Q1+Q2)/m_TOT;
                        ME_FR(C_DATA1(ii).TARGET.ID).DYNAMICS_DATA.vel=vel;
                        ME_FR(C_DATA1(ii).TARGET.ID).GEOMETRY_DATA.mass=m_TOT;
                        
                        KILL_LIST.FRAGMENTS=[KILL_LIST.FRAGMENTS; C_DATA1(jj).TARGET.ID];
                        FRAGMENTS_FR(C_DATA1(jj).TARGET.ID).GEOMETRY_DATA.mass=0;
                    else
                        v1N = ( (m1-m2)*v1 + 2*m2*v2 )/(m1+m2);
                        v2N = ( 2*m1*v1 + (m2-m1)*v2 )/(m1+m2);
                        
                        if C_DATA1(i).TARGET.type==1
                            ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v1N;
                        elseif C_DATA1(i).TARGET.type==0
                            FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=v1N;
                        end
                        
                        if C_DATA1(j).TARGET.type==1
                            ME_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=v2N;
                        elseif C_DATA1(j).TARGET.type==0
                            FRAGMENTS_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=v2N;
                        end
                    end
                    
                    C_DATA1(i).TARGET=[];
                    C_DATA1(j).TARGET=[];
                    
                end
            end
        end
    end
end


%% BOUNCING of multiple impacts
for i=n_C:-1:1
    if (~isempty(C_DATA1(i).TARGET))
        for i1=1:length(C_DATA1(i).IMPACTOR)
            for j=1:i-1
                if (~isempty(C_DATA1(j).TARGET) && ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(i).IMPACTOR(i1))&& C_DATA1(i).IMPACTOR(i1).type==C_DATA1(j).TARGET.type &&  C_DATA1(i).IMPACTOR(i1).ID==C_DATA1(j).TARGET.ID)
                    for j1=length(C_DATA1(j).IMPACTOR):-1:1
                        if ( ~isempty(C_DATA1(i).TARGET) && ~isempty(C_DATA1(j).IMPACTOR(j1)) && C_DATA1(j).IMPACTOR(j1).type==C_DATA1(i).TARGET.type &&  C_DATA1(j).IMPACTOR(j1).ID==C_DATA1(i).TARGET.ID)
                            m1= C_DATA1(i).TARGET.mass;
                            m2= C_DATA1(j).TARGET.mass;
                            [v1,~]=velocity_model_breakup(C_DATA1,i);
                            [v2,~]=velocity_model_breakup(C_DATA1,j);
                            v1N = ( (m1-m2)*v1 + 2*m2*v2 )/(m1+m2);
                            v2N = ( 2*m1*v1 + (m2-m1)*v2 )/(m1+m2);
                            C_DATA1(i).TARGET.v_loss= C_DATA1(i).TARGET.v_loss-(v1N-C_DATA1(i).TARGET.vel); 
                            C_DATA1(j).TARGET.v_loss= C_DATA1(j).TARGET.v_loss-(v2N-C_DATA1(j).TARGET.vel);
                            C_DATA1(i).IMPACTOR(i1)=[];
                            C_DATA1(j).IMPACTOR(j1)=[];
                            if isempty(C_DATA1(i).IMPACTOR)
                                [v_NEW_i,~]=velocity_model_breakup(C_DATA1,i); % CG, GS 05-07-18
                                if(C_DATA1(i).TARGET.type==1)
                                    ME_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_i;
                                else
                                    FRAGMENTS_FR(C_DATA1(i).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_i;
                                end
                                C_DATA1(i).TARGET=[];
                            end
                            if isempty(C_DATA1(j).IMPACTOR)
                                [v_NEW_j,~]=velocity_model_breakup(C_DATA1,j);  % CG, GS 05-07-18
                                if(C_DATA1(j).TARGET.type==1)
                                    ME_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_j;
                                else
                                    FRAGMENTS_FR(C_DATA1(j).TARGET.ID).DYNAMICS_DATA.vel=  v_NEW_j;
                                end
                                C_DATA1(j).TARGET=[];
                            end
                        end
                    end
                end
            end
        end
    end
end
