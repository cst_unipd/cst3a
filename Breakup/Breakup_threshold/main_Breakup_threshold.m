%> @file  main_Breakup_threshold.m
%> @brief Full ME FRAGMENTSation Algorithm\BREAKUP THRESHOLD
%>
%======================================================================
%> @brief Calculation of the breakup threshold
%>
%> This algorithm uses a FRAGMENTSation threshold to determine the
%> FRAGMENTSation level of the target
%>
%> @param C_D = C_DATA(i), i-th investigated collision
%> 
%> @retval C_D = C_DATA(i), i-th investigated collision, updated
%> @retval ME, FRAGMENTS updated
%>
%======================================================================

function [C_D]= main_Breakup_threshold(C_D)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% ME FRAGMENTATION ALGORITHM v04 - dr. L. Olivieri
% Full ME FRAGMENTation Algorithm\BREAKUP THRESHOLD
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% This algorithm uses a fragmentation threshold to determine the
% fragmentation level of the target

%% Input data:
% C_D = C_DATA(i), i-th investigated collision

%% Output_data
% C_D = C_DATA(i), i-th investigated collision, updated

%% Updated data
% ME_FR
% FRAGMENTS_FR

global ME_FR FRAGMENTS_FR BUBBLE

%% 0 - Definition of breakup_flag
switch C_D.TARGET.type
    case 0 
        flag=FRAGMENTS_FR(C_D.TARGET.ID).FRAGMENTATION_DATA.breakup_flag;
    case 1
        flag=ME_FR(C_D.TARGET.ID).FRAGMENTATION_DATA.breakup_flag;
    case 2
        flag=BUBBLE(C_D.TARGET.ID).FRAGMENTATION_DATA.breakup_flag; 
    otherwise   
end

%% 1 - Breakup Threshold definition
switch flag
    case 0 % NO FRAGMENTATION (unbreakable target)
        C_D.F_L=0;
        
    case 1 % EMR and area FRAGMENTATION
        
        %% 1) DEPENDINNG ON GEOMETRY, PROJECTION OF AREA(S) OR VOLUMES ON TARGET
        %   E is the Energy related to the impact areas
        %   E = [E_TOT E_PROJ] where the size is DIM rows ,2 columns
        [E,A_ratio]=VOLUME_projection(C_D);        
        
        %% 2) Fragmentation Threshold determination & 3) F_L update
        switch C_D.TARGET.type
            case 0
                T_Threshold_OLD=FRAGMENTS_FR(C_D.TARGET.ID).FRAGMENTATION_DATA.threshold;
                [ C_D.F_L,TARGET_threshold_NEW,C_D.TARGET.Threshold_update ]=frag_threshold_main(A_ratio,E,T_Threshold_OLD,C_D);
                FRAGMENTS_FR(C_D.TARGET.ID).FRAGMENTATION_DATA.threshold=TARGET_threshold_NEW;
            case 1
                T_Threshold_OLD=ME_FR(C_D.TARGET.ID).FRAGMENTATION_DATA.threshold;
                [ C_D.F_L,TARGET_threshold_NEW,C_D.TARGET.Threshold_update]=frag_threshold_main(A_ratio,E,T_Threshold_OLD,C_D);
                ME_FR(C_D.TARGET.ID).FRAGMENTATION_DATA.threshold=TARGET_threshold_NEW;
            case 2
                T_Threshold_OLD=BUBBLE(C_D.TARGET.ID).FRAGMENTATION_DATA.threshold;
                [ C_D.F_L,TARGET_threshold_NEW,C_D.TARGET.Threshold_update ]=frag_threshold_main(A_ratio,E,T_Threshold_OLD,C_D);
                BUBBLE(C_D.TARGET.ID).FRAGMENTATION_DATA.threshold=TARGET_threshold_NEW;                
        end

    otherwise % error in breakup_flag
        disp('Error in the breakup_flag definition, it can be only =0 (no FRAGMENTation) or = 1 (FRAGMENTATION)')
        pause(0.5);
end

%% 2 Definition of impacts by only bubbles: damages but no fragmentation
DIM2=length([C_D.IMPACTOR]); % number of impactors on target
count=0;
for i=1:DIM2
    if C_D.IMPACTOR(i).type==2 % BUBBLE
        count=count+1;
    end
end
if count/DIM2==1
    C_D.F_L=0;
end
