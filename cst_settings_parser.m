 function [t_end, t_step, t_start, dt_SR, n_max_main_loop, voronoi_solver_ID, PROPAGATION_THRESHOLD]=cst_settings_parser(user_config_file)

global sim_title

if(nargin==1)
    if(strcmp(user_config_file,fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt'))==0)
        copyfile(user_config_file, fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt'));
    end
else
    run_config_infile=fullfile('Simulation_cases',sim_title,'run.config');
    if(exist(run_config_infile))
        default_config_infile=fullfile('Input_check','default_configuration','cst_simulation_default_part_settings.txt');
%         system(['copy ' run_config_infile '+' default_config_infile ' cst_tot_sim_settings.txt']);
        if(ispc)
            system(['type ' run_config_infile ' ' default_config_infile ' > ' fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt')]);
        else
            system(['cat ' run_config_infile ' ' default_config_infile ' > ' fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt')]);
        end

    else
        default_config_infile=fullfile('Input_check','default_configuration','cst_simulation_default_settings.txt');
%         system(['copy ' default_config_infile ' cst_tot_sim_settings.txt']); 
        copyfile(default_config_infile, fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt')); 
        disp([run_config_infile, ' not found. cst_simulation_default_settings.txt loaded']);
    end
end
[PROPAGATION_THRESHOLD] = propagation_thr_struct();

fid = fopen(fullfile('Simulation_cases',sim_title,'cst_tot_sim_settings.txt'));
a = textscan(fid, '%s %f','Delimiter','=','CommentStyle','%');
fclose(fid);

var_names=a{1};
var_values=a{2};

for i=1:length(var_values)
    if(isempty(strfind(var_names{i},'Simulation Time (Seconds)'))==0)
        t_end=var_values(i);
    elseif(isempty(strfind(var_names{i},'Initial Time Steps (Seconds)'))==0)
        t_step=var_values(i);
    elseif(isempty(strfind(var_names{i},'Simulation Starting Time (Seconds)'))==0)
        t_start=var_values(i);
    elseif(isempty(strfind(var_names{i},'Structural Response Time Steps (Seconds)'))==0)
        dt_SR=var_values(i);
    elseif(isempty(strfind(var_names{i},'Maximum Simulation Step'))==0)
        n_max_main_loop=var_values(i);
    elseif(isempty(strfind(var_names{i},'Voronoi Solver'))==0)
        voronoi_solver_ID =var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Mass Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.bubble_mass_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Mass Threshold (kg)'))==0)
        PROPAGATION_THRESHOLD.bubble_mass_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Radius Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.bubble_radius_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Radius Threshold (m)'))==0)
        PROPAGATION_THRESHOLD.bubble_radius_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Energy Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.bubble_energy_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Bubble Energy Threshold (N m)'))==0)
        PROPAGATION_THRESHOLD.bubble_energy_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Mass Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.fragment_mass_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Mass Threshold (kg)'))==0)
        PROPAGATION_THRESHOLD.fragment_mass_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Radius Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.fragment_radius_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Radius Threshold (m)'))==0)
        PROPAGATION_THRESHOLD.fragment_radius_th=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Energy Threshold Flag'))==0)
        PROPAGATION_THRESHOLD.fragment_energy_th_flag=var_values(i);
    elseif(isempty(strfind(var_names{i},'Fragment Energy Threshold (N m)'))==0)
        PROPAGATION_THRESHOLD.fragment_energy_th=var_values(i);
    end
end
disp('')
if(voronoi_solver_ID==1)
    disp('Voronoi Solver: NEPER');
elseif(voronoi_solver_ID>1 && voronoi_solver_ID<5)
    disp('Voronoi Solver: VORO++');
end

if(PROPAGATION_THRESHOLD.bubble_mass_th_flag==1 || PROPAGATION_THRESHOLD.bubble_radius_th_flag==1 || PROPAGATION_THRESHOLD.bubble_energy_th_flag==1)
    disp('BUBBLE Generation activated');
end
if(PROPAGATION_THRESHOLD.fragment_mass_th_flag==1 || PROPAGATION_THRESHOLD.fragment_radius_th_flag==1 || PROPAGATION_THRESHOLD.fragment_energy_th_flag==1)
    disp('Automatic Fragments Unbreaking activated');
end
end