%> @file  PLOT2_basic_plot.m
%> @brief basic plot of CST data with no bubble
%> Copyright (c) 2017 CISAS - UNIVERSITY OF PADOVA - Dr L. Olivieri

function [h]=PLOT2_basic_plot(link_data,ME,FRAGMENTS,t,C,dim,az,el,V_scale,plot_flag,flag_velocities,flag_NO_c_hull)


if plot_flag == 1
    h = figure('Position', [100, 50, 700, 700],'Visible', 'off');
else
    h = figure('Position', [100, 50, 700, 700],'Visible', 'off');
end
draw3Dv0(link_data,ME,t,V_scale); % draw all MEs
hold on
for j=1:length(FRAGMENTS)
    hold on
    if FRAGMENTS(j).GEOMETRY_DATA.mass>0 %% && FRAGMENTS(j).object_ID==53
        plot3(FRAGMENTS(j).DYNAMICS_DATA.cm_coord(1),FRAGMENTS(j).DYNAMICS_DATA.cm_coord(2),FRAGMENTS(j).DYNAMICS_DATA.cm_coord(3),'k')
        m_color=FRAGMENTS(j).FRAGMENTATION_DATA.threshold/FRAGMENTS(j).FRAGMENTATION_DATA.threshold0;
        hold on
        % Flag to plot the C_HULLS
        if flag_NO_c_hull==0 && (~isempty(find(isfinite(FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,:)))) && ~isempty(find(isfinite(FRAGMENTS(j).DYNAMICS_DATA.cm_coord))))
            % drawing the c-hulls
            TRI = delaunay(FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,1)+FRAGMENTS(j).DYNAMICS_DATA.cm_coord(1),...
                FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,2)+FRAGMENTS(j).DYNAMICS_DATA.cm_coord(2),...
                FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,3)+FRAGMENTS(j).DYNAMICS_DATA.cm_coord(3));
            hSurface=trisurf(TRI,FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,1)+FRAGMENTS(j).DYNAMICS_DATA.cm_coord(1),...
                FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,2)+FRAGMENTS(j).DYNAMICS_DATA.cm_coord(2),...
                FRAGMENTS(j).GEOMETRY_DATA.c_hull(:,3)+FRAGMENTS(j).DYNAMICS_DATA.cm_coord(3));
            set(hSurface,'FaceColor',[.6 .8*(m_color) .9*(m_color)],'FaceAlpha',0.3,'EdgeColor',[.4 .4 .4]);
            hold on
        end
        if flag_velocities==1
            %Draws FRAGMENTS velocity vector (scaled)
            quiver3(FRAGMENTS(j).DYNAMICS_DATA.cm_coord(1),FRAGMENTS(j).DYNAMICS_DATA.cm_coord(2),FRAGMENTS(j).DYNAMICS_DATA.cm_coord(3),...
                FRAGMENTS(j).DYNAMICS_DATA.vel(1)/V_scale,FRAGMENTS(j).DYNAMICS_DATA.vel(2)/V_scale,FRAGMENTS(j).DYNAMICS_DATA.vel(3)/V_scale,'r'); % V_FRAG_j
        end
    end
end
% 
xlim([C(1)-dim/3 C(1)+dim/3])
ylim([C(2)-dim/3 C(2)+dim/3])
zlim([C(3)-dim/3 C(3)+dim/3]) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% xlim([-2 2])
% ylim([-2 2])
% zlim([-2 2]) %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% xticks([-5 0 5 ])
% yticks([-5 0 5 ])
% zticks([-5 0 5 10])
hold on
set(gca,'fontsize',14)
xlabel('x [m]');
ylabel('y [m]');
zlabel('z [m]');
% axis equal
view([az el])
title(sprintf('time = %d',t));