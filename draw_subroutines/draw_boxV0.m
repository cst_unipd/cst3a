function draw_box( i ,m_color,ME)
%DRAW_PLATE Draws the box (with orientation). 

col_box = [.6 .8*(m_color) .9*(m_color)];
q0=ME(i).DYNAMICS_DATA.quaternions(1);
q1=ME(i).DYNAMICS_DATA.quaternions(2);
q2=ME(i).DYNAMICS_DATA.quaternions(3);
q3=ME(i).DYNAMICS_DATA.quaternions(4);
%The orientation of the plate is given by the quaternions
Rot_l2g=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
    2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
    2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];

Rot_l2g=Rot_l2g'; %transpose the matrix

lx=ME(i).GEOMETRY_DATA.dimensions(1); %x
ly=ME(i).GEOMETRY_DATA.dimensions(2); %y
lz=ME(i).GEOMETRY_DATA.dimensions(3); %z

corner(:,1)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[lx/2;ly/2;-lz/2];
corner(:,2)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[lx/2;-ly/2;-lz/2];
corner(:,3)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[-lx/2;-ly/2;-lz/2];
corner(:,4)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[-lx/2;ly/2;-lz/2];
X = corner(1,:);
Y = corner(2,:);
Z = corner(3,:);
patch(X,Y,Z,col_box,'FaceAlpha',0.3); %bottom surface

corner(:,1)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[lx/2;ly/2;+lz/2];
corner(:,2)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[lx/2;-ly/2;+lz/2];
corner(:,3)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[-lx/2;-ly/2;+lz/2];
corner(:,4)=ME(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[-lx/2;ly/2;+lz/2];
X2 = corner(1,:);
Y2 = corner(2,:);
Z2 = corner(3,:);
patch(X2,Y2,Z2,col_box,'FaceAlpha',0.3); %top surface
patch([X(1:2),X2(2),X2(1)],[Y(1:2),Y2(2),Y2(1)],[Z(1:2),Z2(2),Z2(1)],col_box,'FaceAlpha',0.3); %sides
patch([X(3:4),X2(4),X2(3)],[Y(3:4),Y2(4),Y2(3)],[Z(3:4),Z2(4),Z2(3)],col_box,'FaceAlpha',0.3);
patch([X(2:3),X2(3),X2(2)],[Y(2:3),Y2(3),Y2(2)],[Z(2:3),Z2(3),Z2(2)],col_box,'FaceAlpha',0.3);
patch([X(1),X(4),X2(4),X2(1)],[Y(1),Y(4),Y2(4),Y2(1)],[Z(1),Z(4),Z2(4),Z2(1)],col_box,'FaceAlpha',0.3);
end

