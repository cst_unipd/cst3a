function draw_plate( i,col )
%DRAW_PLATE Draws the plate (with orientation). To draw the plate we need the coordinates
%of the 4 corners.
global ME_SR

col_plate = col;
q0=ME_SR(i).DYNAMICS_DATA.quaternions(1);
q1=ME_SR(i).DYNAMICS_DATA.quaternions(2);
q2=ME_SR(i).DYNAMICS_DATA.quaternions(3);
q3=ME_SR(i).DYNAMICS_DATA.quaternions(4);
%The orientation of the plate is given by the quaternions
Rot_l2g=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
    2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
    2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];

Rot_l2g=Rot_l2g'; %transpose the matrix
lx=ME_SR(i).GEOMETRY_DATA.dimensions(1); %x
ly=ME_SR(i).GEOMETRY_DATA.dimensions(2); %y
corner(:,1)=ME_SR(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[lx/2;ly/2;0];
corner(:,2)=ME_SR(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[lx/2;-ly/2;0];
corner(:,3)=ME_SR(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[-lx/2;-ly/2;0];
corner(:,4)=ME_SR(i).DYNAMICS_DATA.cm_coord+Rot_l2g*[-lx/2;ly/2;0];

%Position of vertices
x = corner(1,:);
y = corner(2,:);
z = corner(3,:);
patch(x,y,z,col_plate)

end

