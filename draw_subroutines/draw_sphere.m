function draw_sphere( i )
%DRAW_PLATE Draws the box (with orientation).

global ME_SR
radius=ME_SR(i).GEOMETRY_DATA.dimensions(1);
X_cg=ME_SR(i).DYNAMICS_DATA.cm_coord(1);
Y_cg=ME_SR(i).DYNAMICS_DATA.cm_coord(2);
Z_cg=ME_SR(i).DYNAMICS_DATA.cm_coord(3);
[X,Y,Z] = sphere;
if ME_SR(i).GEOMETRY_DATA.shape_ID==2 % sphere
    Color = 'b';
elseif ME_SR(i).GEOMETRY_DATA.shape_ID==3 % hollow sphere
    Color = [127/255 1 212/255];
end
surf(X*radius+X_cg,Y*radius+Y_cg,Z*radius+Z_cg,'FaceColor',Color,'EdgeColor','k');
end