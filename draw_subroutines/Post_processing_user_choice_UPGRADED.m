
%> @file  Post_processing.m
%> @brief post processing of CST data, plots and figures

%======================================================================
%% Paths and initial data
%clc
%clear all
%close all
%addpath draw_subroutines

global output_folder
global n
%% STANDALONE
flag_alone=0; % standalone: compile the next section with right names

if flag_alone==1
    clc
    clear all
    close all
    sim_title_dir=fullfile('Results','prova_fin_2');
    plot_flag = 1;
end
%%

% FLAGS for PLOTS
flag_velocities=0; % PLOTTING VELOCITY VECTORS
flag_bubbles=0;    % PLOTTING BUBBLES
plot_flag = 0;  %%%% non fa le .fig
%%
% addpath ffmpeg\bin

input_folder = fullfile(sim_title_dir,'data');
output_folder = sim_title_dir;
output_folder_fig=fullfile(output_folder,'figures');
% name=sprintf('%s/step_',input_folder);
name=fullfile(input_folder,'step_');

% d=sprintf('%s/*.mat',input_folder);
d=fullfile(input_folder,'step_*.mat');
n = length(dir(d))-1; % number of CST_DATA files

tmpl = fullfile(output_folder_fig, 'user_fig');
tmp2 = fullfile(output_folder_fig,'user_fig_BUBBLE');
tmp3 = fullfile(output_folder_fig,'user_fig_vel');
tmpVID=fullfile(output_folder, 'user_vid');
tmpVID_bubble=fullfile(output_folder, 'user_vid_bubble');
tmpVID_vel=fullfile(output_folder, 'user_vid_vel');

%% DEFINITION OF GEOMETRIC PARAMETERS FOR PLOTS

in_file=[name num2str(0)];
RIS=load(in_file);
ME=RIS.ME;
FRAGMENTS=RIS.FRAGMENTS;
[C,dim,az,el,V_scale]=PLOT1_geometry_parameters(ME,FRAGMENTS);

%% initial conditions plot, to give user idea of orientation
clear RIS
in_file=[name num2str(0)];
RIS=load(in_file);
ME=RIS.ME;
FRAGMENTS=RIS.FRAGMENTS;
link_data=RIS.link_data;
t=0;
% FIRST PLOT
h=PLOT2_basic_plot(link_data,ME,FRAGMENTS,t,C,dim,az,el,V_scale,1,0,0);

%% DEFINITION OF USER INPUTS
videos_flag=1;
plot_fragmentation_flag=1;
pdf_flag=1;
step_size=1;
general_plot_flag=1;
plot_crater=0;
[C,dim,az,el,V_scale,flag_velocities,flag_bubbles,plot_flag,general_plot_flag,videos_flag,plot_fragmentation_flag,pdf_flag,step_size,plot_crater]=...
    user_input_post_pocessing_UPGRADED(C,dim,az,el,V_scale,flag_velocities,flag_bubbles,plot_flag,general_plot_flag,videos_flag,plot_fragmentation_flag,pdf_flag,step_size,plot_crater);

if general_plot_flag ==0 && plot_fragmentation_flag ==0 && plot_crater==0 && pdf_flag==0
    disp('No output has been chosen. Exit from post processing...')
end
%% initial conditions plot
clear RIS
in_file=[name num2str(0)];
RIS=load(in_file);
ME=RIS.ME;
FRAGMENTS=RIS.FRAGMENTS;
link_data=RIS.link_data;
t=0;
fnam=sprintf('%s_%d',tmpl,0);
fnam2=sprintf('%s_%d',tmp2,0);
fnam3=sprintf('%s_%d',tmp3,0);

if general_plot_flag==1 % plot of MEs figures and videos
    % FIRST PLOT
    h=PLOT2_basic_plot(link_data,ME,FRAGMENTS,t,C,dim,az,el,V_scale,plot_flag,flag_velocities,0);
    
    if flag_bubbles==1
        hold on
        if isempty(RIS.BUBBLE)==0 % there are bubbles
            for j=1:length(RIS.BUBBLE)
                if RIS.BUBBLE(j).GEOMETRY_DATA.mass>0
                    radius=RIS.BUBBLE(j).GEOMETRY_DATA.dimensions(1);
                    X_cg=RIS.BUBBLE(j).DYNAMICS_DATA.cm_coord(1);
                    Y_cg=RIS.BUBBLE(j).DYNAMICS_DATA.cm_coord(2);
                    Z_cg=RIS.BUBBLE(j).DYNAMICS_DATA.cm_coord(3);
                    [X,Y,Z] = sphere;
                    hSurface=surf(X*radius+X_cg,Y*radius+Y_cg,Z*radius+Z_cg);
                    set(hSurface,'FaceColor',[1 0 0],'FaceAlpha',0.3,'EdgeColor',[0.5 0 0]);
                    hold on
                end
            end
        end
    end
    
    if plot_flag == 1
        pause(0.001)
        saveas(h, fnam,'png'); set(h, 'Visible', 'off');
        saveas(h, fnam,'fig'); set(h, 'Visible', 'off');
        if flag_bubbles==1
            saveas(h, fnam2,'png'); set(h, 'Visible', 'off');
            saveas(h, fnam2,'fig'); set(h, 'Visible', 'off');
        end
        if flag_velocities==1
            saveas(h, fnam3,'png'); set(h, 'Visible', 'off');
            saveas(h, fnam3,'fig'); set(h, 'Visible', 'off');
        end
    else
        saveas(h, fnam,'png'); set(h, 'Visible', 'off');
        if flag_bubbles==1
            saveas(h, fnam2,'png'); set(h, 'Visible', 'off');
        end
        if flag_velocities==1
            saveas(h, fnam3,'png'); set(h, 'Visible', 'off');
        end
    end
    
    clear RIS
    
    %%  PLOT of each step figure, without and with bubble
    
    j_step=1;
    for i=1:step_size:n
        hold off
        %     name=sprintf('%s/step_',input_folder);
        in_file=[name num2str(i)];
        RIS=load(in_file); %
        ME=RIS.ME;
        FRAGMENTS=RIS.FRAGMENTS;
        link_data=RIS.link_data;
        t=RIS.tf;
        disp(['time ',num2str(t,'%10.5e'),' s, step ',num2str(i),' of ',num2str(n)])
        fnam=sprintf('%s_%d',tmpl,j_step);
        fnam2=sprintf('%s_%d',tmp2,j_step);
        fnam3=sprintf('%s_%d',tmp3,j_step);
        
        %     % NO VELOCITIES PLOT
        h=PLOT2_basic_plot(link_data,ME,FRAGMENTS,t,C,dim,az,el,V_scale,plot_flag,0,0);
        if plot_flag == 1
            pause(0.001)
            saveas(h, fnam,'png'); set(h, 'Visible', 'off');
            saveas(h, fnam,'fig'); set(h, 'Visible', 'off');
        else
            saveas(h, fnam,'png'); set(h, 'Visible', 'off');
        end
        
        % BUBBLE PLOT
        if flag_bubbles==1
            hold on
            if isempty(RIS.BUBBLE)==0 % there are bubbles
                for j=1:length(RIS.BUBBLE)
                    if RIS.BUBBLE(j).GEOMETRY_DATA.mass>0
                        radius=RIS.BUBBLE(j).GEOMETRY_DATA.dimensions(1);
                        X_cg=RIS.BUBBLE(j).DYNAMICS_DATA.cm_coord(1);
                        Y_cg=RIS.BUBBLE(j).DYNAMICS_DATA.cm_coord(2);
                        Z_cg=RIS.BUBBLE(j).DYNAMICS_DATA.cm_coord(3);
                        [X,Y,Z] = sphere;
                        hSurface=surf(X*radius+X_cg,Y*radius+Y_cg,Z*radius+Z_cg);
                        set(hSurface,'FaceColor',[1 0 0],'FaceAlpha',0.3,'EdgeColor',[0.5 0 0]);
                        hold on
                    end
                end
            end
            
            if plot_flag == 1
                pause(0.001)
                saveas(h, fnam2,'png'); set(h, 'Visible', 'off');
                saveas(h, fnam2,'fig'); set(h, 'Visible', 'off');
            else
                saveas(h, fnam2,'png'); set(h, 'Visible', 'off');
            end
        end
        
        % VELOCITIES PLOT
        if flag_velocities==1
            h=PLOT2_basic_plot(link_data,ME,FRAGMENTS,t,C,dim,az,el,V_scale,plot_flag,flag_velocities,0);
            
            if plot_flag == 1
                pause(0.001)
                saveas(h, fnam3,'png'); set(h, 'Visible', 'off');
                saveas(h, fnam3,'fig'); set(h, 'Visible', 'off');
            else
                saveas(h, fnam3,'png'); set(h, 'Visible', 'off');
            end
        end
        
        j_step=j_step+1;
        
        if i~=n
            clear RIS
        end
        
        clear h
        clear hSurface
        clear ME FRAGMENTS BUBBLE fnam fnam2 fnam3 t radius X_cg Y_cg Z_cg X Y Z link_data_t
        clear VORONOI_SOLVER_LIST voronoi_solver_ID voropp_folder voropp_interface_folder warning_file_name
        clear toi_time ti tf t_step t_start t_end sum_ME_mass SHAPE_ID_LIST SEEDS_DISTRIBUTION PROPAGATION_THRESHOLD
        clear neper_folder neper_files neper_figures n_max_main_loop n_collision_tot n_collision ME_SR_tm1
        clear MATERIAL_LIST KILL_LIST main_loop_count HOLES
        close all
    end
    if isempty(i)==false
        n=i;
    else
        n=0;
    end
    
    %% Create video from png
    if videos_flag==1
        pause(0.1)
        string1=[pwd fullfile(filesep,'draw_subroutines','ffmpeg','bin','ffmpeg')];
        if(isunix)
            string1=['wine ' pwd fullfile(filesep,'draw_subroutines','ffmpeg','bin','ffmpeg.exe')];
        end
        ris_video=system([ string1 ' -loglevel quiet -y -r 10 -f image2 -s 700x1200 -i ' pwd filesep tmpl '_%d.png -vcodec libx264 -vf "scale=2160:trunc(ow/a/2)*2" -crf 25  -pix_fmt yuv420p ' pwd filesep tmpVID '.mp4' ]);
        pause(0.1)
        if ris_video==0
            disp('--> Standard video creation complete')
        else
            disp('--> Warning: standard video creation failed')
        end
        if flag_bubbles==1
            ris_video=system([ string1 ' -loglevel quiet  -y -r 10 -f image2 -s 700x1200 -i ' pwd filesep tmp2 '_%d.png -vcodec libx264 -vf "scale=2160:trunc(ow/a/2)*2" -crf 25  -pix_fmt yuv420p ' pwd filesep tmpVID_bubble '.mp4' ]);
            pause(0.1)
            if ris_video==0
                disp('--> Bubbles video creation complete')
            else
                disp('--> Warning: bubbles video creation failed')
            end
        end
        if flag_velocities==1
            ris_video=system([ string1 ' -loglevel quiet  -y -r 10 -f image2 -s 700x1200 -i ' pwd filesep tmp3 '_%d.png -vcodec libx264 -vf "scale=2160:trunc(ow/a/2)*2" -crf 25  -pix_fmt yuv420p ' pwd filesep tmpVID_vel '.mp4' ]);
            pause(0.1)
            if ris_video==0
                disp('--> Velocities video creation complete')
            else
                disp('--> Warning: velocities video creation failed')
            end
        end
    end
end

%% Plot Fragments statistics: mass and size distribution, A/m and velocities
if plot_fragmentation_flag==1
    disp('--> Plotting statistics on debris distribution')
    %disp('NOTE: PLOTS CONSIDER FRAGMENT(S) AS FIRST IMPACTOR(S)!!!')
    
    clear RIS
    in_file=[name num2str(n)];
    if exist([in_file '.mat'],'file')==0
        in_file=[name num2str(n+1)];
    end
    
    if exist([in_file '.mat'],'file')~=0
        RIS=load(in_file);
        if length(RIS.FRAGMENTS)>1
            n_size=length(RIS.FRAGMENTS);

            %% DELETE FRAGMENTS WITH MASS == 0
            for i_frag=n_size:-1:1
                if RIS.FRAGMENTS(i_frag).GEOMETRY_DATA.mass==0
                    RIS.FRAGMENTS(i_frag)=[];
                end
            end
            n_size=length(RIS.FRAGMENTS);
            if isempty(RIS.FRAGMENTS)==false
                PLOT3_statistics_on_IMPACT_UPGRADED(RIS,output_folder_fig,plot_flag)
                FRAGMENTS=RIS.FRAGMENTS;
                PLOT4_statistics_on_FRAGMENTS(FRAGMENTS,output_folder_fig,plot_flag)
            else
                disp('No surviving fragments in the simulation')
            end
        else
            disp('No fragments in the simulation')
        end
    else
        disp('No fragments in the simulation')
    end
    
end

if plot_crater==1
    PLOT5_first_crater_plot_UPGRADED
end


% data on the impact (only fragments)
if pdf_flag==1
    n = j_step;
    options_doc_nocode.format = 'pdf';
    options_doc_nocode.showCode = false;
    options_doc_nocode.outputDir = sim_title_dir;
    pause(0.01)
    publish('user_defined_output.m',options_doc_nocode);
    close all
end


