function BLE_tests()
tests_tot=51:97;
penetr_vec=2*ones(size(tests_tot));

i_tests_tot=47:47;%16:length(tests_tot);
tests=tests_tot(i_tests_tot);%51:51;
n_steps=20*ones(size(tests));

BLE_res_mat='BLE_results.mat';

if(exist(BLE_res_mat))
    delete(BLE_res_mat);
end

holes_tot=[];
for i=1:length(tests)
    if(tests(i)~=64 && tests(i)~=65)
        caso=tests(i);
        steps=n_steps(i);
        save('caso.mat','caso','steps');
        
        if(exist('holes_temp.mat'))
            delete('holes_temp.mat')
        end
        BLE_res=['BLE_results',num2str(caso),'.mat'];
        if(exist(BLE_res))
            delete(BLE_res);
        end
        
        cst_main_BLE();
        delete('caso.mat');
        h=load('holes_temp.mat');
        holes_tot=h.HOLES;
        plate2_holes=find([holes_tot.object_ID]==2);
        j=i_tests_tot(i);
        if(isempty(plate2_holes))
            penetr_vec(j)=0;
        else
            penetr_vec(j)=1;
        end
        penetr_vec_j=penetr_vec(j);
        save(BLE_res,'caso','penetr_vec_j');
    end
end
save(BLE_res_mat,'tests_tot','penetr_vec');

for j=1:length(tests_tot)
    BLE_res=['BLE_results',num2str(tests_tot(j)),'.mat'];
    if(exist(BLE_res))
        aa=load(BLE_res);
        penetr_vec(j)=aa.penetr_vec_j;
    else
        penetr_vec(j)=2;
    end
end
BLE_verification(penetr_vec);
