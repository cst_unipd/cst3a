%> @file  link_local_description.m
%> @brief Provides the local description of the link which connects the i and j ME
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
%> @brief Computes the link in global coordinates as the difference between the positions
%> of the two ME which connects. Then it rotates it to local coordinates.
%> @param i The index of the ME attached to one end of the link  
%> @param j The index of the ME attached to the other end of the link 
%>
%> @retval X [6x1] vector that describes the link in local frame of reference, both length and relative angles 
%> @retval d_X [6x1] Describes the speed at which the links changes length/shape
%> @retval Rot_matrix_l2g [3x3] matrix of the rotation from local to global coordinates 
%>======================================================================
function [X,d_X,Rot_matrix_l2g] = link_local_description(idx_ME1,idx_ME2)
%LINK_LOCAL_DESCRIPTION; from the global information on the position of
%each ME, it provides the shape of the link in local coordinates. 
global ME_SR;

%Link shape in GLOBAL coordinates; renaming for a more "intuitive"
Pos_i=ME_SR(idx_ME1).DYNAMICS_DATA.cm_coord;
Vel_i=ME_SR(idx_ME1).DYNAMICS_DATA.vel;
Pos_j=ME_SR(idx_ME2).DYNAMICS_DATA.cm_coord;
Vel_j=ME_SR(idx_ME2).DYNAMICS_DATA.vel;

link_geom=Pos_j-Pos_i; %Vector of differences in position, global coordinates
link_vel=Vel_j-Vel_i;  

q0=ME_SR(idx_ME1).DYNAMICS_DATA.quaternions(1);
q1=ME_SR(idx_ME1).DYNAMICS_DATA.quaternions(2);
q2=ME_SR(idx_ME1).DYNAMICS_DATA.quaternions(3);
q3=ME_SR(idx_ME1).DYNAMICS_DATA.quaternions(4);

%From the global to the body(local coordinates)
Rot_matrix_g2l=[q0^2+q1^2-q2^2-q3^2, 2*(q1*q2-q0*q3), 2*(q0*q2+q1*q3);
                2*(q1*q2+q0*q3)    , q0^2-q1^2+q2^2-q3^2, 2*(q2*q3-q0*q1);
                2*(q1*q3-q0*q2)    , 2*(q0*q1+q2*q3),     q0^2-q1^2-q2^2+q3^2];
            
Rot_matrix_l2g=Rot_matrix_g2l'; %transpose the matrix is the inverse

%Link shape in LOCAL coordinates; has it compressed or stretched?
Local_link_geom=Rot_matrix_g2l*link_geom;
Local_link_vel=Rot_matrix_g2l*link_vel;

%Link shape in LOCAL coordinates; did it twist? Direction cosines of the
%link
rel_angle=Local_link_geom/norm(Local_link_geom);

%For now, this is identiacally zero. It should be the velocity at which the
%direction cosine change.
rel_d_angle=[0;0;0];%Local_link_vel/norm(Local_link_vel);

X=[Local_link_geom;rel_angle];
d_X=[Local_link_vel;rel_d_angle];
end

