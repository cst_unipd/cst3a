%> @file  check_and_update.m
%> @brief Check if current configuration is compatible with the link, otherwise kills it
%> @author Francesco Feltrin (francesco.feltrin@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
%> @brief  First check for failure of the link and eventually kills it.
%> Then, updates the link proprieties according to deformed geometry and material
%> proprieties
%> @param link_idx the id of the link that needs to be updated
%> @param X  The current configuration of the link
%> @param d_X The current velocity at which the link is deforming
%> @param Rot_l2g The rotation matrix from the local to global frame of reference
%>
%> @retval link_data Updates the link information
%>======================================================================
function check_and_update(link_idx,X,d_X)

global link_data;
global link_data0;
global MATERIAL_LIST;
global ME_SR;

%CHECK for failure
%Failure by distruction of ME: if the mass of any of the linked ME is zero, kill the link
for j=1:1:2
    ME_one_end=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(j));
    if ME_SR(ME_one_end).GEOMETRY_DATA.mass==0
        link_data(link_idx).state=0;
    end
end

%Failure of the link
if link_data(link_idx).failure_ID==1 %Fail by maximum elongation (strain)

    if link_data(link_idx).type_ID == 1
        mat_idx_joint = find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST
        max_strain_adm = MATERIAL_LIST(mat_idx_joint).max_strain;
        strain = (norm(X(1:3,1))- norm(link_data(link_idx).rest))/norm(link_data(link_idx).rest);
        %Check for failure
        if strain>max_strain_adm 
            link_data(link_idx).state=0; % the link breaks
        end
    elseif link_data(link_idx).type_ID == 2
        error('Wrong failure criterion for welding!')
    elseif link_data(link_idx).type_ID == 3
        error('Wrong failure criterion for welding!')
    elseif link_data(link_ID).type_ID == 4
        error('Failure criterion not implemented for continuum link!')
    end
    
elseif link_data(link_idx).failure_ID==2 %Fail by maximum equivalent stress
    
    idx_ME(1)=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(1));
    idx_ME(2)=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(2));

    if link_data(link_idx).type_ID == 1
        mat_idx_joint =  find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST
        tau_bmax = MATERIAL_LIST(mat_idx_joint).max_shear*0.5; % 0.5 safety factor for bolted joint
        sigma_bmax = MATERIAL_LIST(mat_idx_joint).max_stress*0.25; % 0.25 safety factor for bolted joint
        l_screw = link_data(link_idx).geometry{1}(4);
        sigma_current = MATERIAL_LIST(mat_idx_joint).young...
            *norm(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))/...
            l_screw;
        tau_current = MATERIAL_LIST(mat_idx_joint).shear_modulus...
            *norm((link_data(link_idx).geometry{4}(1:3,2:3)'*(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))))/...
            l_screw;
        %Check for failure
        if ((tau_current/tau_bmax)^2+(sigma_current/sigma_bmax)^2)>=1 || isnan(((tau_current/tau_bmax)^2+(sigma_current/sigma_bmax)^2))
            link_data(link_idx).state=0; % the link breaks
        end
        
    elseif link_data(link_idx).type_ID == 2
        
        mat_idx_joint = find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST
        filler_sigma_adm = 0.7*MATERIAL_LIST(mat_idx_joint).max_stress;
        
        sigma_current = norm(link_data(link_idx).geometry{4}*(link_data(link_idx).geometry{3}(1))'...
            *(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))/...
            (link_data(link_idx).geometry{1}(1)*link_data(link_idx).geometry{1}(2)*link_data(link_idx).geometry{1}(3)));
        
        if (sigma_current>=filler_sigma_adm)
            link_data(link_idx).state=0; % the link breaks
            disp(['link(',num2str(link_idx),') failed by maximum equivalent stress'])
        end
        
    elseif link_data(link_idx).type_ID == 3
        
        mat_idx_joint = find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST
        tau_adm = 1.25*MATERIAL_LIST(mat_idx_joint).max_shear;
        
        tau_current = norm(link_data(link_idx).geometry{4}*(link_data(link_idx).geometry{3}(1))'...
            *(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))/...
            (link_data(link_idx).geometry{1}(1)*link_data(link_idx).geometry{1}(2)*link_data(link_idx).geometry{1}(3)));
        def = ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0);
        if any(abs(tau_current)>tau_adm) || dot(def,link_data(link_idx).geometry{3}(:,3))>1e-3*norm(def)
            link_data(link_idx).state=0; % the link breaks
            disp(['link(',num2str(link_idx),') failed by maximum equivalent stress'])
        end
        
    elseif link_data(link_idx).type_ID == 4
        
        mat_idx_joint = [MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID; % index of link material in MATERIAL_LIST
        sigma_adm = 0.7*MATERIAL_LIST(mat_idx_joint).max_stress;
        
        sigma_current = norm(link_data(link_idx).geometry{4}*(link_data(link_idx).geometry{3}(1))'...
            *(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))/...
            (link_data(link_idx).geometry{1}(1)*link_data(link_idx).geometry{1}(2)*link_data(link_idx).geometry{1}(3)));
        
        if (sigma_current>=sigma_adm)
            link_data(link_idx).state=0; % the link breaks
            disp(['link(',num2str(link_idx),') failed by maximum equivalent stress'])
        end
        
    end
    
elseif link_data(link_idx).failure_ID==3
    
    idx_ME(1)=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(1));
    idx_ME(2)=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(2));
    
    %Fail by maximum equivalent stress
    if link_data(link_idx).type_ID == 1
        
        mat_idx_joint =  find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST
        tau_bmax = MATERIAL_LIST(mat_idx_joint).max_shear*0.5; % 0.5 safety factor for bolted joint
        sigma_bmax = MATERIAL_LIST(mat_idx_joint).max_stress*0.25; % 0.25 safety factor for bolted joint
        l_screw = link_data(link_idx).geometry{1}(4);
        sigma_current = MATERIAL_LIST(mat_idx_joint).young...
            *norm(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))/...
            l_screw;
        tau_current = MATERIAL_LIST(mat_idx_joint).shear_modulus...
            *norm((link_data(link_idx).geometry{4}(1:3,2:3)'*(ME_SR(idx_ME(1)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(1)).DYNAMICS_INITIAL_DATA.cm_coord0...
            -(ME_SR(idx_ME(2)).DYNAMICS_DATA.cm_coord-ME_SR(idx_ME(2)).DYNAMICS_INITIAL_DATA.cm_coord0))))/...
            l_screw;
        %Check for failure
        if ((tau_current/tau_bmax)^2+(sigma_current/sigma_bmax)^2)>=1 || isnan(((tau_current/tau_bmax)^2+(sigma_current/sigma_bmax)^2))
            link_data(link_idx).state=0; % the link breaks
            disp(['link(',num2str(link_idx),') failed by maximum equivalent stress'])
        end
        
        max_strain_adm = MATERIAL_LIST(mat_idx_joint).max_strain;
        strain = (norm(X(1:3,1))- norm(link_data(link_idx).rest))/norm(link_data(link_idx).rest);
        %Check for failure
        if strain>max_strain_adm
            link_data(link_ID).state=0; % the link breaks
            disp(['link(',num2str(link_idx),') failed by maximum strain'])
        end
        
    elseif link_data(link_ID).type_ID == 2
        error('Wrong failure criterion for welding!')
    elseif link_data(link_ID).type_ID == 3
        error('Wrong failure criterion for adhesive bondings!')
    elseif link_data(link_ID).type_ID == 4
        error('Failure criterion not implemented for continuum link!')
    end
    
end
