function [ response ] = link_response(link_ID,connection,X,d_X,rest)
%> @file  link_response.m
%> @brief Applies viscous-elastic response to the link  
%> 
%======================================================================
%> @brief How the link reacts (through forces and Torques) to the 
%> deformation (in local coordinates) imposed by the current configuration
%>
%> @param link_ID Which link is evaluated 
%> @param connection To which ME it is connected to
%> @param X current configuration fo the link (local)
%> @param d_X current deformation speed (local )
%> @param rest equilibrium configuration 
%> @retval response [6x1] vector with forces and torques (which are now zero)
%======================================================================

%LINK_RESPONSE: How the link reacts (through forces and Torques) to the 
% deformation (in local coordinates) imposed by the current configuration
% The expected output format is
%       F=[Fx;Fy;Fz;M_x;M_y;M_z]

global link;

%The links are entities that connect two different ME, each of which has
%its own local coordinates. Therefore We have to write link proprieties as
%in the local coordinates.

%Reminder: link_structure  is as follows 
%Fields : 'id',       eg. {1}
%        'connect'        {[1,2]} 
%        'material'       {1}
%        'k_mat'          {K_al} 4x4 matrix (only one rotation)
%        'rest'           {l0} 4x1 vec  
%        'c_mat'          {C_al} 4x4 mat
%        'failure'        {1} fail mode index
%        'state'          {1} 1 is intact, 0 broken

% Each link is connected to two elements. The "rest" configuration, the "shape"
% the link would have when at rest, is calculated from one(i) to the other(j), 
% so if the "rest" configuration from the point of view of "the other"(j) is needed,
% it has to be flipped (multiply by -1).
if connection==link(link_ID).connect
    %Verse is in agreement 
    index=1;
else
    %Other verse, connection is a permutation of link.connect
    %eg. [i,j] instead of [j,i]
    index=-1;    
end

%For now, a place holder of simply 
Elastic_response=link(link_ID).k_mat*(index*rest-X);
Viscous_response=link(link_ID).c_mat*(d_X);

response= Elastic_response+Viscous_response;
end

