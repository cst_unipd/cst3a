%> @file link_strain.m
%> @brief Computes strain of the deformed link
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function [ strain,shear ] = link_strain( X,link_idx )
%STRAIN Computes strain of the deformed link
%Strain is given a fraction of initial length
% two vectors!!

global link_data;

Glob_link_geom=X(1:3,1); %stretching or compressing
link_dir=X(4:6,1);
%Computes Linear strain
streched_lenght=(Glob_link_geom);
rest=link_data(link_idx).rest;
rest_local=rest(1:3);%transfer to local coordinates

l0=(rest_local(1:3));
strain=(streched_lenght-l0)./l0;
strain(~isfinite(strain))=0;
dir_0=rest(4:6); %original link orientation
shear=dir_0-link_dir;
end

