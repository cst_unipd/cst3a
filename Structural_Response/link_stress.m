%> @file link_stress.m
%> @brief Computes stress of the deformed link
%> @author Francesco Feltrin (francesco.feltrin@unipd.it)and Giulia Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function [ stress, strain ] = link_stress( X,link_ID )

global link_data;
global MATERIAL_LIST;

[strain,shear]=link_strain(X,link_ID);
E = MATERIAL_LIST(link_data(link_ID).material_ID).young;
nu = MATERIAL_LIST(link_data(link_ID).material_ID).poisson;
stress_11=strain*E/((1-2*nu)*(1+nu))*(1-nu);
stress_12=shear*E/(2*(1+nu));
stress = max([stress_11; stress_12]); % maximum stress

end

