%> @file update_link.m
%> @brief According to the new geometry configuration, it updates 
%the physical proprieties of the link. 
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function update_link(link_idx)
%UPDATE_LINK_S According to the new geometry configuration, it updates 
%the physical proprieties of the link. 

global link_data;
global ME_SR;

%Link shape in GLOBAL coordinates; renaming for a more "intuitive"
%representation
idx_ME1=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(1)); % idx_ME1 is the ME1 index
idx_ME2=find([ME_SR.object_ID]==link_data(link_idx).ME_connect(2)); % idx_ME2 is the ME2 index

Pos_i=ME_SR(idx_ME1).DYNAMICS_DATA.cm_coord;
Vel_i=ME_SR(idx_ME1).DYNAMICS_DATA.vel;
Pos_j=ME_SR(idx_ME2).DYNAMICS_DATA.cm_coord;
Vel_j=ME_SR(idx_ME2).DYNAMICS_DATA.vel;

X=[Pos_j-Pos_i; 0; 0; 0]; %Vector of differences in position, global coordinates
d_X=[Vel_j-Vel_i; 0; 0; 0];  

% With the deformation of the link (in local coordinates), link proprieties can be reassigned.
check_and_update(link_idx,X,d_X);
end

