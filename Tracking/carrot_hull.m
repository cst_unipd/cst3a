%> @file  carrot_hull.m
%> @brief Populating COLLISION_DATA structure for Breakup algorithm
%> @author Lorenzo Olivieri (lorenzo.olivieri@unipd.it)
%======================================================================
%> @brief calculations of the intersection of a body with a prisma
%> with known base pn XY plane
%>
%> @param C2D Base area verteces
%> @param C3D Body verteces
%> @param a size of the base area parent figure
%> @param v direction of the z-axis
%>
%> @retval int_v verteces of the intersection volume
%======================================================================
function int_v = carrot_hull(C2D,C3D,a,v)

d=sqrt(2*(a(1)^2+a(2)^2+a(3)^2));
d=max(d,1e-3);

x=C2D(:,1); 
y=C2D(:,2);

carrot_v=zeros(2*length(x),3);

for j=1:length(x)
    v1=[x(j) y(j) 0]+d*[0 0 1];
    v2=[x(j) y(j) 0]-d*[0 0 1];
    carrot_v(j,1) = v1(1);
    carrot_v(j,2) = v1(2);
    carrot_v(j,3) = v1(3);
    carrot_v(j+length(x),1) = v2(1);
    carrot_v(j+length(x),2) = v2(2);
    carrot_v(j+length(x),3) = v2(3);
end

% direction of the velocity
c=v(3);
if abs(c)==1
    R=eye(3); % no rotation
else
    w1=(-v(2)*[1 0 0]+v(1)*[0 1 0]);
    c1=sqrt((1+c)/2);
    k=sqrt(0.5/(1+c)); %sinP/2)/sin/P)
    q=[c1 k.*w1];
    q=q/norm(q);
    R = rquat(q);
end
%%
% Verteces of the carrot and of the original volume
car_v = unique(carrot_v,'rows');

for i=1:length(C3D(:,1))
C3D(i,:)=(R*C3D(i,:)')';
end

object_v = unique(C3D,'rows');


% compute the intersection 
% [int_v, ~] = getBoundariesIntersection(car_v,object_v);
[int_v, ~] = getBoundariesIntersection(object_v,car_v);
if isempty(int_v) % the two objects are identical
    int_v=object_v;
end
int_v = unique(int_v,'rows');

%% PLOTS

% car_f = minConvexHull(car_v);
% object_f = minConvexHull(object_v);
% int_f = minConvexHull(int_v);
% 
% figure()
% drawMesh(car_v,car_f, 'FaceColor',[0 1 0],'FaceAlpha',0.1); hold on;
% drawMesh(object_v,object_f, 'FaceColor',[1 0 0],'FaceAlpha',0.08);
% drawMesh(int_v,int_f, 'FaceColor',[0 0 1],'FaceAlpha',0.2);
% axis equal;
% xlim([-3E-3 3E-3])
% ylim([-3E-3 3E-3])
% zlim([-3E-3 3E-3])

