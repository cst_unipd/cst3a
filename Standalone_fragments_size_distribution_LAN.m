% Standalone for fragments size distribution
% FRAG_DIST=[size cumulative_number]

if exist('FRAGMENTS_FR','var')
    if length(FRAGMENTS_FR)>length(FRAGMENTS)
        FRAGMENTS=FRAGMENTS_FR;
        ME=ME_FR;
    end
end

%% DELETE FRAGMENTs & MEs WITH MASS == 0 AND DEFINE DIMENSIONS
n_size=length(FRAGMENTS);
for i=n_size:-1:1
    if FRAGMENTS(i).GEOMETRY_DATA.mass==0
        FRAGMENTS(i)=[];
    else
        box = boundingBox3d(FRAGMENTS(i).GEOMETRY_DATA.c_hull);
        lx = abs(box(2)-box(1));
        ly = abs(box(4)-box(3));
        lz = abs(box(6)-box(5));
        FRAGMENTS(i).GEOMETRY_DATA.dimensions=[lx ly lz];
        x(i)=max(FRAGMENTS(i).GEOMETRY_DATA.dimensions);
    end
end
n_size=length(FRAGMENTS);
FRAGMENTS1=FRAGMENTS;

n_size1=length(ME);
for i=n_size1:-1:1
    if ME(i).GEOMETRY_DATA.mass==0
        ME(i)=[];
    end
end
n_size1=length(ME);

if ~isempty(ME)
    FRAGMENTS1(n_size+1:n_size+n_size1)=ME;
end

n_size=length(FRAGMENTS1);

%% CREATE Lc, AM RATIO AND VELOCITY DISTRIBUTIONS FROM CST RESULTS
s1 =zeros(1,n_size-1);
k=1;
for i = 2:n_size
    Lc_CST(i-1)=mean(FRAGMENTS1(i).GEOMETRY_DATA.dimensions);
    Am_CST(i-1)=((FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(1)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3)+...
        FRAGMENTS1(i).GEOMETRY_DATA.dimensions(2)*FRAGMENTS1(i).GEOMETRY_DATA.dimensions(3))/3/FRAGMENTS1(i).GEOMETRY_DATA.mass);
    vel_CST_wp(i-1)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
    mass_CST(i-1)=FRAGMENTS1(i).GEOMETRY_DATA.mass;
    if FRAGMENTS1(i).object_ID~=66
       vel_CST_np(k)=norm(FRAGMENTS1(i).DYNAMICS_DATA.vel);
       k=k+1;
    end
end

%% PLOT FRAGMENT DISTRIBUTIONS CST
s=sort(Lc_CST);

if n_size==1 %patch to avoid only one fragment case (LO)
    n_size=n_size+1;
end

N=n_size-1:-1:1;

FRAG_DIST=[s' N'];

% h = figure('Position', [100, 50, 500, 500],'Visible', 'on');
% loglog(s*1000,N,'+g','LineWidth',2)
% grid on
% hold on
% xlim([0.5 3.5])
% legend('CST Nishida','Test data interpolation','Nishida generalized','CST Spiral','CST Random-Gauss') %to plot Nishida results

%% LOAD LAN RESULTS (obtained from paper)
% load('./Lan/LAN_A_m_ratio')
Am_LAN=10.^Y;
Lc_LAN=10.^X;

%coefficients
alpha1=0.0060;
sigma1=0.1270;
mu1=-1.180;
alpha2=0.0360;
sigma2=0.1050;
mu2=-0.695;
alpha3=0.0150;
sigma3=0.1450;
mu3=-0.295;
csi_LAN=log10(sort(Am_LAN));

%complete PDF
% P=alpha1*(1/(sigma1*(2*pi)^0.5))*exp(-((csi-mu1).^2)/(2*sigma1^2))+...
%     alpha2*(1/(sigma2*(2*pi)^0.5))*exp(-((csi-mu2).^2)/(2*sigma2^2))+...
%     alpha3*(1/(sigma3*(2*pi)^0.5))*exp(-((csi-mu3).^2)/(2*sigma3^2));

%Al fragments PDF
P=alpha2*(1/(sigma2*(2*pi)^0.5))*exp(-((csi_LAN-mu2).^2)/(2*sigma2^2));

%% CST results
alpha4=1;%0.06;
sigma4=std(Am_CST);%0.2;
mu4=mean(Am_CST);%-0.85;
csi_CST=sort(log10(Am_CST));
P2=alpha4*(1/(sigma4*(2*pi)^0.5))*exp(-((csi_CST-mu4).^2)/(2*sigma4^2));

%% NASA SBM
[csi_NASA index]=sort(log10(Am_CST));
for j=1:1:length(csi_NASA)
    lambda_c(j)=log10(Lc_CST(index(j)));
end

n=length(lambda_c);
% lambda_cc=[0:1/749:1];
% lambda_c=log10(lambda_cc);
for i=1:1:n
        if  lambda_c(i)<=-3.5
            muSOC=-0.3;
            sigmaSOC=0.2;
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
        elseif lambda_c(i)>-3.5 && lambda_c(i)<=-1.75
            muSOC=-0.3;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
        elseif lambda_c(i)>-1.75 && lambda_c(i)<-1.25
            muSOC=-0.3-1.4*(lambda_c(i)+1.75);
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
        elseif lambda_c(i)>=-1.25
            muSOC=-1.0;
            sigmaSOC=0.2+0.1333*(lambda_c(i)+3.5);
            DAmSOC(i,:)=(1/(sigmaSOC*(2*pi)^0.5))*exp(-((csi_NASA-muSOC).^2)/(2*sigmaSOC^2));
        end
end

for k=1:1:max(length(DAmSOC))
    DAmSOC_NASA(k)=sum(DAmSOC(:,k));
end

%% PLOT
figure
histogram(log10(Am_CST),'Normalization','probability','BinWidth',0.05)
grid on
hold on
plot(csi_LAN,P,'-r','LineWidth',2)
% plot(csi_CST,P2,'k','LineWidth',2)
% plot(csi_NASA,DAmSOC(1,:)./max(DAmSOC(1,:)),'b','LineWidth',2)
plot(csi_NASA,DAmSOC_NASA./max(DAmSOC_NASA),'b','LineWidth',2)
title('Comparison among PDFs')
xlabel('A/m [m^2/kg]')
ylabel('Probability Density')
% legend('CST Fragments','LAN PDF','CST PDF','NASA SBM***')