function [ME,FRAGMENTS,BUBBLE]=generate_ME_LOFT22(i1,i2,i3,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

% INITIAL CONDITION AT WILL
vel=[0;0;0]; %velocity of every ME
EMR= 0; % Energy to mass ratio

DEFAULT_seeds_distribution_ID = 0;%<<<<=== 0 random cartesian, 4 random hollow
DEFAULT_seeds_distribution_ID_hollow = 4;
DEFAULT_seeds_distribution_param1 = 100;
DEFAULT_seeds_distribution_param2 = 5;
z_plane=2.685; %m
z_lower=-z_plane;
long_side=3.47;
short_side=1;
thickness=0.05;
body_diameter=1.925; %[m]
mass_panel=50; %[kg]

for i=i1:i1+5
    %they are 6 panel rotated of 60 degrees each
    j=i-1; %start from zero, just because
    r=(long_side/2+body_diameter/2)*1.05;
    pos=[r*cos(j*pi/3);r*sin(j*pi/3);z_plane];
    quaternions=[cos(j*pi/6),0,0,-sin(j*pi/6)];
    w=[0,0,0]';
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,thickness];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0=mass_panel; %kg
    ME(i).GEOMETRY_DATA.mass=mass_panel; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    ME(i).FRAGMENTATION_DATA.failure_ID=1;
    ME(i).FRAGMENTATION_DATA.threshold0=0;
    ME(i).FRAGMENTATION_DATA.threshold=0;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    
end
%center element of the Hat
i=i+1;
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %A box
ME(i).GEOMETRY_DATA.dimensions=[0.65*body_diameter,0.65*body_diameter,0.5]; %#####
ME(i).GEOMETRY_DATA.thick=0;
ME(i).GEOMETRY_DATA.mass0=5; %kg
ME(i).GEOMETRY_DATA.mass=5; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;z_plane];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=1;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;

%body: Main cylinder
i=i+1;
tube_diameter=0.9;
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=5; %cylinder
ME(i).GEOMETRY_DATA.dimensions=[tube_diameter/2,0,0.85*2*z_plane]; % ### tube_diameter,2*z_plane,0
ME(i).GEOMETRY_DATA.thick=0.001; %m
ME(i).GEOMETRY_DATA.mass0=200; %kg
ME(i).GEOMETRY_DATA.mass=200; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;0];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=1;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=1000;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID_hollow;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;


%Tank (sphere)
i=i+1;
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=3; % a hollow sphere
ME(i).GEOMETRY_DATA.dimensions=[0.7*body_diameter/4,0,0]; % ### body_diameter/4
ME(i).GEOMETRY_DATA.thick=0.02; %kg
ME(i).GEOMETRY_DATA.mass0=500; %kg
ME(i).GEOMETRY_DATA.mass=500; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;z_lower];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=1;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=500;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID_hollow;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;


%Bottom hexagonal prism
hex_diameter=1.5;
mass_hex_panel=50;
long_side=0.8*0.992; % ### 0.992
short_side=0.8*0.900; % ### 0.900
thickness=0.01;

for i=i+1:i+6
    %they are 6 panel rotated of 60 degrees each
    j=i-1; %start from zero, just because
    r=1.05*hex_diameter/2;
    pos=[r*cos(j*pi/3);r*sin(j*pi/3);z_lower];
    quaternion_1=[cos(pi/4),0,sin(pi/4),0];
    quaternion_2=[cos(j*pi/6),0,0,-sin(j*pi/6)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quaternion_1,quaternion_2);
    else
        quaternions=quatxquat(quaternion_1,quaternion_2);
    end
    w=[0;0;0];
    ME(i).object_ID=i;
    ME(i).material_ID=1;
    ME(i).GEOMETRY_DATA.shape_ID=1; %plate
    ME(i).GEOMETRY_DATA.dimensions=[long_side,short_side,thickness];
    ME(i).GEOMETRY_DATA.thick=0; %kg
    ME(i).GEOMETRY_DATA.mass0=mass_hex_panel; %kg
    ME(i).GEOMETRY_DATA.mass=mass_hex_panel; %kg
    ME(i).GEOMETRY_DATA.A_M_ratio=0;
    ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
    ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
    ME(i).DYNAMICS_INITIAL_DATA.w0=w;
    ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
    ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
    ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
    ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
    ME(i).FRAGMENTATION_DATA.failure_ID=1;
    ME(i).FRAGMENTATION_DATA.threshold0=EMR;
    ME(i).FRAGMENTATION_DATA.threshold=EMR;
    ME(i).FRAGMENTATION_DATA.breakup_flag=1;
    ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
    ME(i).FRAGMENTATION_DATA.c_EXPL=0;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    ME(i).FRAGMENTATION_DATA.param_add1=0;
    ME(i).FRAGMENTATION_DATA.param_add2=0;
    
end

%Some boxes for electronics
i=i+1;
pos=[r*cos(0*pi/3);r*sin(0*pi/3);z_lower];
quaternion_2=[cos(0*pi/6),0,0,-sin(0*pi/6)];
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %box
ME(i).GEOMETRY_DATA.dimensions=0.8*[0.2,0.3,0.4]; %### 0.2,0.3,0.4
ME(i).GEOMETRY_DATA.thick=0; %kg
ME(i).GEOMETRY_DATA.mass0=20; %kg
ME(i).GEOMETRY_DATA.mass=20; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos-[0.2;0;0];
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternion_2;
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=1;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;


%Another box for electronics
i=i+1;
r2=r+0.05;
pos=[r2*cos(2*pi/3);r2*sin(2*pi/3);z_lower];
quaternion_2=[cos(2*pi/6),0,0,-sin(2*pi/6)];
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %box
ME(i).GEOMETRY_DATA.dimensions=0.8*[0.2,0.3,0.4]; % ### 0.2,0.3,0.4
ME(i).GEOMETRY_DATA.thick=0; %kg
ME(i).GEOMETRY_DATA.mass0=20; %kg
ME(i).GEOMETRY_DATA.mass=20; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternion_2;
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0;0;0];
ME(i).FRAGMENTATION_DATA.failure_ID=1;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;


%Solar panel
i=i+1;
pos=[r*cos(0*pi/3);r*sin(0*pi/3);z_lower];
quaternion_1=[cos(pi/4),0,sin(pi/4),0];
ME(i).object_ID=i;
ME(i).material_ID=1;
ME(i).GEOMETRY_DATA.shape_ID=1; %plate
ME(i).GEOMETRY_DATA.dimensions=[2,5,0.05];
ME(i).GEOMETRY_DATA.thick=0; %kg
ME(i).GEOMETRY_DATA.mass0=50; %kg
ME(i).GEOMETRY_DATA.mass=50; %kg
ME(i).GEOMETRY_DATA.A_M_ratio=0;
ME(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=pos-[0;0;1.7]; % ### 1.5
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternion_1;
ME(i).DYNAMICS_INITIAL_DATA.vel0=vel;
ME(i).DYNAMICS_INITIAL_DATA.w0=w;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).DYNAMICS_DATA.vel=ME(i).DYNAMICS_INITIAL_DATA.vel0;
ME(i).DYNAMICS_DATA.w=ME(i).DYNAMICS_INITIAL_DATA.w0;
ME(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
ME(i).FRAGMENTATION_DATA.failure_ID=1;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=1;
ME(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
ME(i).FRAGMENTATION_DATA.cMLOSS=0.2;
ME(i).FRAGMENTATION_DATA.cELOSS=0.1;
ME(i).FRAGMENTATION_DATA.c_EXPL=0;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
ME(i).FRAGMENTATION_DATA.param_add1=0;
ME(i).FRAGMENTATION_DATA.param_add2=0;



end