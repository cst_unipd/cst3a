Simulation Time (Seconds)= 2.1
Initial Time Steps (Seconds)= 0.1
Simulation Starting Time (Seconds)= 0               % initial time of the entire simulation
Structural Response Time Steps (Seconds)= 1E-6      % Structural response timestep
Maximum Simulation Step= 2                          % maximum number of the main while loop runs
Voronoi Solver= 1                                   % 1 => Neper with bash; 2 => Voro++ with bash; 3 => Voro++ from system; 4 => Voro++ with MEX 
Bubble Mass Threshold Flag= 0                       % Flag to activate bubble generation using mass criterion
Bubble Mass Threshold (kg)=5e-08                    % Bubble generation mass threshold
Bubble Radius Threshold Flag= 1                     % Flag to activate bubble generation using radius criterion
Bubble Radius Threshold (m)=1e-07                   % Bubble generation radius threshold
Bubble Energy Threshold Flag= 0                     % Flag to activate bubble generation using energy criterion
Bubble Energy Threshold (N m)=0                     % Bubble generation energy threshold
Fragment Mass Threshold Flag= 0                     % Flag to activate fragments automatic unbreaking using mass criterion
Fragment Mass Threshold (kg)=0                      % Fragments unbreaking mass threshold
Fragment Radius Threshold Flag= 0                   % Flag to activate fragments automatic unbreaking using radius criterion
Fragment Radius Threshold (m)=1e-07                 % Fragments unbreaking radius threshold
Fragment Energy Threshold Flag= 0                   % Flag to activate fragments automatic unbreaking using energy criterion
Fragment Energy Threshold (N m)=0                   % Fragments unbreaking energy threshold


