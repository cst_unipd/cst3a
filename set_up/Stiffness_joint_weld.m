%> @file Stiffness_joint_weld.m
%> @brief creates the stiffness and viscousness matrices for the WELDED joint 
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function Stiffness_joint_weld(link_idx,X)

% function which creates the stiffness and viscousness matrices in GLOBAL
% COORDINATES for the WELDED joint and update them in the link_data
% structure
%
% Syntax: Stiffness_joint_weld(link_idx,X)
%
% Inputs 
%           link_idx:   index of the considered link
%           X:          relative cuttent position between the two mass
%                       centers of the MEs connected by the link
%
% Outputs: none (link_data structure is updated)
%    
% Other m-files required: rquat
%                         
% Subfunctions: dot, norm, acos, eye, sign, abs, cross
% 
% MAT-files required: none
%
% See also:
%
% Authors:   
%            Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/08/02
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/08/02 : first version by MD, GS

%#codegen

global ME ME_SR
global link_data
global MATERIAL_LIST

if isempty(ME_SR)
    ME_SR = ME;
end

mat_idx_joint = find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST

nr_weld = link_data(link_idx).geometry{1}(1);
l_weld = link_data(link_idx).geometry{1}(2);
X_dir = link_data(link_idx).geometry{2}(:,1);
Y_dir = link_data(link_idx).geometry{2}(:,2);
filler_E = MATERIAL_LIST(mat_idx_joint).young;
filler_ni = MATERIAL_LIST(mat_idx_joint).ni;

rest=link_data(link_idx).rest(1:3); %"Rest" (=equilibrium configuration) length of link in GLOBAL coordinates

i_1=find([ME.object_ID]==link_data(link_idx).ME_connect(1)); % i_1 is the ME1 index
i_2=find([ME.object_ID]==link_data(link_idx).ME_connect(2)); % i_2 is the ME2 index

cos_alpha = dot(X(1:3),rest)/(norm(X(1:3))*norm(rest));

if cos_alpha > 1
    alpha = acos(1);
elseif cos_alpha < -1
    alpha = acos(-1);
elseif isnan(cos_alpha)
    alpha = 0;
else
    alpha = acos(cos_alpha);
end
if alpha == 0 % parallel
    R_link2glob=eye(3); % no rotation
else
    vect_alpha = cross(X(1:3),rest);
    if norm(vect_alpha)==0 % smaller than the solver tolerance
        R_link2glob=eye(3); % no rotation
    else
        vect_alpha=vect_alpha/norm(vect_alpha);
        qq=[cos(alpha/2),vect_alpha(1)*sin(alpha/2),vect_alpha(2)*sin(alpha/2),vect_alpha(3)*sin(alpha/2)];
        R_link2glob = rquat(qq);
    end
end

G = filler_E/(2*(1+filler_ni));

kxx=(G+filler_E)/2*l_weld*nr_weld;
kxy=0;
kxz=0;
kyx=0;
kyy=G*l_weld*nr_weld;
kyz=0;
kzx=0;
kzy=0;
kzz=(G+filler_E)/2*l_weld*nr_weld;

K_matrix1=[kxx kxy kxz; kyx kyy kyz; kzx kzy kzz];
K_matrix2=[kxx kxy kxz; kyx kyy kyz; kzx kzy kzz]; %[kzx kxy -kzz; kyx kyy kyz; -kxx kzy kxz]; % in joint coordinates

m_bar=(ME_SR(i_1).GEOMETRY_DATA.mass+ME_SR(i_2).GEOMETRY_DATA.mass)/(ME_SR(i_1).GEOMETRY_DATA.mass*ME_SR(i_2).GEOMETRY_DATA.mass);
csi=0.005;
 
C_matrix1=2*csi*sqrt(abs(K_matrix1)).*sqrt(m_bar).*sign(K_matrix1); % in joint coordinates
C_matrix2=2*csi*sqrt(abs(K_matrix2)).*sqrt(m_bar).*sign(K_matrix2); % in joint coordinates

X_dir = X_dir - (dot(X_dir,Y_dir)*norm(X_dir))*Y_dir;
X_dir = X_dir/norm(X_dir); % attention: not to give two parallel directions
% if Y_dir(1)==0
%     X_dir=[0; -Y_dir(3); Y_dir(2)]/norm(Y_dir(2:3));
% elseif Y_dir(2)==0
%     X_dir=1/norm([-Y_dir(3); 0; Y_dir(1)])*[-Y_dir(3); 0; Y_dir(1)];
% else
%     X_dir=[-Y_dir(2); Y_dir(1); 0]/norm(Y_dir(1:2));
% end

Z_dir=cross(X_dir,Y_dir)/(norm(X_dir)*norm(Y_dir));
ROT_joint2glob =R_link2glob*[X_dir,Y_dir,Z_dir]; % R_link2glob*

link_data(link_idx).geometry(3) = {ROT_joint2glob};
K_glob_link1=ROT_joint2glob*K_matrix1*ROT_joint2glob';
C_glob_link1=ROT_joint2glob*C_matrix1*ROT_joint2glob';
K_glob_link2=ROT_joint2glob*K_matrix2*ROT_joint2glob';
C_glob_link2=ROT_joint2glob*C_matrix2*ROT_joint2glob';

K_matrix6=[K_glob_link1, zeros(3); K_glob_link2, zeros(3)];
C_matrix6=[C_glob_link1, zeros(3); C_glob_link2, zeros(3)];
link_data(link_idx).geometry(4) = {K_matrix1}; % local matrix

link_data(link_idx).k_mat = K_matrix6;
link_data(link_idx).c_mat = C_matrix6;

end