%> @file Stiffness_joint_bolt.m
%> @brief creates the stiffness and viscousness matrices for the BOLTED joint
%> @author Dr. M. Duzzi (matteo.duzzi@phd.unipd.it) & Dr. G. Sarego (giulia.sarego@unipd.it)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function Stiffness_joint_bolt(link_idx,X)

% function which creates the stiffness and viscousness matrices in 
% GLOBAL COORDINATES for the BOLTED joint and update them in the 
% link_data structure
%
% Syntax: Stiffness_joint_bolt(link_idx,X)
%
% Inputs:
%           link_idx:   link index
%           X:          position of the ME in GLOBAL COORDINATES
%           
% Outputs: none (link_data structure is updated)
%
% Other m-files required: ME, ME_SR, link_data, material_list, rquat
%
% Subfunctions: find
%
% MAT-files required: none
%
% See also: link_data, force_from_j2i
%
% Authors:   Matteo Duzzi, PhD
%            Center of Studies and Activities for Space "Giuseppe Colombo" (CISAS)
%            University of Padova
%
%            Giulia Sarego, PhD
%            Department of Industrial Engineering (DII)
%            University of Padova
%
% Email address: matteo.duzzi@phd.unipd.it, giulia.sarego@unipd.it
% Date: 2018/07/23
% Revision: 1.0
% Copyright: 2018 CISAS - UNIVERSITY OF PADOVA
%
% HISTORY
% 2018/07/23 : first version by MD, GS

%#codegen


global ME ME_SR
global link_data
global MATERIAL_LIST

if isempty(ME_SR)
    ME_SR = ME;
end

i_1=find([ME.object_ID]==link_data(link_idx).ME_connect(1)); % i_1 is the ME1 index
i_2=find([ME.object_ID]==link_data(link_idx).ME_connect(2)); % i_2 is the ME2 index

m_bar=(ME_SR(i_1).GEOMETRY_DATA.mass+ME_SR(i_2).GEOMETRY_DATA.mass)/(ME_SR(i_1).GEOMETRY_DATA.mass*ME_SR(i_2).GEOMETRY_DATA.mass);
csi=0.01;

mat_idx_joint = find([MATERIAL_LIST.mat_id]==link_data(link_idx).material_ID); % index of link material in MATERIAL_LIST

nr_bolts = link_data(link_idx).geometry{1}(1);
pitch = link_data(link_idx).geometry{1}(2);
diam = link_data(link_idx).geometry{1}(3);
l_screw = link_data(link_idx).geometry{1}(4);
X_dir = link_data(link_idx).geometry{2};

rest=link_data(link_idx).rest(1:3); %"Rest" (=equilibrium configuration) length of link in GLOBAL coordinates


epsilon = norm(X(1:3)-rest)./l_screw; % GLOBAL coordinates
cos_alpha = dot(X(1:3),rest)/(norm(X(1:3))*norm(rest));
if cos_alpha > 1
    alpha = acos(1);
elseif cos_alpha < -1
    alpha = acos(-1);
elseif isnan(cos_alpha)
    alpha = 0;
else
    alpha = acos(cos_alpha);
end
if alpha == 0 % parallel
    R_link2glob=eye(3); % no rotation
else
    vect_alpha = cross(X(1:3),rest);
    if norm(vect_alpha)==0 % smaller than the solver tolerance
        R_link2glob=eye(3); % no rotation
    else
        vect_alpha=vect_alpha/norm(vect_alpha);
        qq=[cos(alpha/2),vect_alpha(1)*sin(alpha/2),vect_alpha(2)*sin(alpha/2),vect_alpha(3)*sin(alpha/2)];
        R_link2glob = rquat(qq);
    end
end

% Screw characteristics
sigmab=MATERIAL_LIST(mat_idx_joint).max_stress-0.65*MATERIAL_LIST(mat_idx_joint).yield_stress; %[Pa] breaking
sigmay=0.35*MATERIAL_LIST(mat_idx_joint).yield_stress; %[Pa] yield
Delta=MATERIAL_LIST(mat_idx_joint).max_strain; % Elongation before break
ni=MATERIAL_LIST(mat_idx_joint).ni;
w_res=0.19635*(2*diam-1.87639*pitch)^2; %[m2] resistive section of the screw

% External conditions
m_sec=1; % number of sections involved in the joint, by default is 1



if isempty(epsilon)
    epsilon=0;
end

if epsilon<=0.002
    E_loc=sigmay/0.002;
elseif epsilon>0.002 && epsilon<=Delta
    sigma_bolt=sign(epsilon)*interp1([0.002 Delta],[sigmay sigmab],abs(epsilon));
    E_loc=sigma_bolt/epsilon;
else
    E_loc=0;
end

kxx=E_loc*nr_bolts*m_sec*w_res/l_screw;
kxy=0;
kxz=0;
kyx=0;
kyy=E_loc*nr_bolts*m_sec*diam + E_loc/(2*(1+ni))*nr_bolts*m_sec*w_res/l_screw;
kyz=0;
kzx=0;
kzy=0;
kzz=kyy;

K_matrix1=[kxx kxy kxz; kyx kyy kyz; kzx kzy kzz]; % in joint coordinates
K_matrix2=[-kxx kxy kxz; kyx kyy kyz; kzx kzy kzz]; % in joint coordinates


C_matrix1=2*csi*sqrt(K_matrix1).*sqrt(m_bar); % in joint coordinates
C_matrix2=2*csi*sqrt(abs(K_matrix2)).*sqrt(m_bar).*sign(K_matrix2); % in joint coordinates

if X_dir(1)==0
    Y_dir=[0; -X_dir(3); X_dir(2)]/norm(X_dir(2:3));
elseif X_dir(2)==0
    Y_dir=1/norm([-X_dir(3); 0; X_dir(1)])*[-X_dir(3); 0; X_dir(1)];
else
    Y_dir=[-X_dir(2); X_dir(1); 0]/norm(X_dir(1:2));
end
Z_dir=cross(X_dir,Y_dir)/(norm(X_dir)*norm(Y_dir));
ROT_joint2glob =R_link2glob*[X_dir,Y_dir,Z_dir];

link_data(link_idx).geometry(3) = {ROT_joint2glob};
K_glob_link1=ROT_joint2glob*K_matrix1*ROT_joint2glob';
C_glob_link1=ROT_joint2glob*C_matrix1*ROT_joint2glob';
K_glob_link2=ROT_joint2glob*K_matrix2*ROT_joint2glob';
C_glob_link2=ROT_joint2glob*C_matrix2*ROT_joint2glob';

K_matrix6=[K_glob_link1, zeros(3); K_glob_link2, zeros(3)];
C_matrix6=[C_glob_link1, zeros(3); C_glob_link2, zeros(3)];
link_data(link_idx).geometry(4) = {K_matrix1}; % in joint coordinates

link_data(link_idx).k_mat = K_matrix6;
link_data(link_idx).c_mat = C_matrix6;

end