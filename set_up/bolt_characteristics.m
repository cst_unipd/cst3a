function [sigmab,sigmay,Delta,matid]=bolt_characteristics(bolt_class,nr_link)

global link_data

switch bolt_class
    case '8.8'
        % Class 8.8
        matid=2;
        
        sigmab=800e6-0.65*640e6; %[Pa] breaking
        sigmay=0.35*640e6; %[Pa] yield
        sigmat=560e6; %[Pa] traction stress
        tau=396e6; %[Pa] shear stress
        
        % Maximum possible stress in screw of class 8.8
        tau_bmax=186.32635e6; %[Pa] shear stress
        sigma_bmax=274.5862e6; %[Pa] traction stress
        Delta=12/100; % Elongation before break
        alphav=0.6;
        
    case '10.9'
        % Class 10.9
        matid=2;

        sigmab=1000e6-0.65*900e6; %[Pa] breaking
        sigmay=0.35*900e6; %[Pa] yield
        sigmat=700e6; %[Pa] traction
        tau=495e6; %[Pa] shear
        
        % Maximum possible stress in screw of class 10.9
        tau_bmax=215.7463e6; %[MPa] shear stress
        sigma_bmax=382.45935e6; %[MPa] traction stress
        Delta=8/100; % Elongation before break
        alphav=0.5;
       
end

link_data(nr_link).geom_link(5) ={[sigmab,sigmay,sigmat,tau,tau_bmax,sigma_bmax,Delta,alphav]};