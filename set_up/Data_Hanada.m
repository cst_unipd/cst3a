% matrix with 10 columns:
% 1:3 position_cm
% 4:6 dimensions_abc (r 0 h)
% 7:9 rotation axis
% 10 alpha rotation
% 11:13 rotation axis 2
% 14 alpha rotation 2
% NB: se cambi l'ordine delle box (le righe), si DEVONO cambiare i link!)
grad2rad=pi/180;
plate_thickness=0.003; %[m]
hc=0.04-2/3*plate_thickness;
data_hanada=[ 0  0                         -0.1-plate_thickness/2             0.2    0.2    plate_thickness      1 0 0  0           1 0 0  0;... %bottom plate % 6 7 8 9 10 11 12 13
              0  0                         -0.1+plate_thickness+hc      0.197  0.197  plate_thickness*2/3  1 0 0  0           1 0 0  0;... %first plate from bottom % 6 7 8 9 10 11 12 13 14 15 16 17
              0  0                         -0.1+5/3*plate_thickness+2*hc      0.197  0.197  plate_thickness*2/3  1 0 0  0           1 0 0  0;... %second plate from bottom % 6 7 8 9 14 15 16 17 18 19 20 21
              0  0                         -0.1+7/3*plate_thickness+3*hc      0.197  0.197  plate_thickness*2/3  1 0 0  0           1 0 0  0;... %third plate from bottom % 6 7 8 9 18 19 20 21 22 23 24 25
              0  0                          0.1-plate_thickness/2             0.197  0.197  plate_thickness  1 0 0  0           1 0 0  0;... %top plate % 6 7 8 9 22 23 24 25
              0 -0.1-plate_thickness/2  0                                 0.2    0.2    plate_thickness      1 0 0  90*grad2rad 1 0 0  0;... %lateral plate 1 (6) % 7 9
              0.1+plate_thickness/2 0       0                                 0.2    0.2    plate_thickness      1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %lateral plate 2 % 8 26
              0  0.1-plate_thickness/2  0                                 0.2    0.2    plate_thickness      1 0 0  90*grad2rad 1 0 0  0;... %lateral plate 3 % 9
             -0.1+plate_thickness/2 0       0                                 0.2    0.2    plate_thickness      1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %lateral plate 4
              0.0585  0.0685               -0.1+plate_thickness/2+hc/2        0.002  0     hc                 1 0 0  0           1 0 0  0;... %column over bot plate (10)
             -0.0585  0.0685               -0.1+plate_thickness/2+hc/2        0.002  0      hc                1 0 0  0           1 0 0  0;... %column over bot plate
             -0.0585 -0.0685               -0.1+plate_thickness/2+hc/2        0.002  0     hc                1 0 0  0           1 0 0  0;... %column over bot plate
              0.0585 -0.0685               -0.1+plate_thickness/2+hc/2        0.002  0     hc                1 0 0  0           1 0 0  0;... %column over bot plate
              0.0585  0.0685               -0.1+7/6*plate_thickness+3/2*hc  0.002  0     hc                 1 0 0  0           1 0 0  0;... %column over first plate (14)
             -0.0585  0.0685               -0.1+7/6*plate_thickness+3/2*hc  0.002  0      hc                 1 0 0  0           1 0 0  0;... %column over first plate
             -0.0585 -0.0685               -0.1+7/6*plate_thickness+3/2*hc  0.002  0      hc                 1 0 0  0           1 0 0  0;... %column over first plate
              0.0585 -0.0685               -0.1+7/6*plate_thickness+3/2*hc  0.002  0      hc                 1 0 0  0           1 0 0  0;... %column over first plate
              0.0585  0.0685               -0.1+11/6*plate_thickness+5/2*hc    0.002  0      hc                 1 0 0  0           1 0 0  0;... %column over second plate (18)
             -0.0585  0.0685               -0.1+11/6*plate_thickness+5/2*hc    0.002  0      hc                1 0 0  0           1 0 0  0;... %column over second plate
             -0.0585 -0.0685               -0.1+11/6*plate_thickness+5/2*hc    0.002  0      hc                1 0 0  0           1 0 0  0;... %column over second plate
              0.0585 -0.0685               -0.1+11/6*plate_thickness+5/2*hc    0.002  0      hc                 1 0 0  0           1 0 0  0;... %column over second plate
              0.0585  0.0685               -0.1+5/2*plate_thickness+4*hc    0.002  0      2*hc                 1 0 0  0           1 0 0  0;... %column over third plate (22)
             -0.0585  0.0685               -0.1+5/2*plate_thickness+4*hc    0.002  0      2*hc                 1 0 0  0           1 0 0  0;... %column over third plate
             -0.0585 -0.0685               -0.1+5/2*plate_thickness+4*hc    0.002  0      2*hc                 1 0 0  0           1 0 0  0;... %column over third plate
              0.0585 -0.0685               -0.1+5/2*plate_thickness+4*hc    0.002  0      2*hc                 1 0 0  0           1 0 0  0;... %column over third plate
              0  0.197/2+plate_thickness/2+0.06/2  0                          0.003  0      0.060                1 0 0  90*grad2rad 1 0 0  0 ... %antenna (26)
            ]; 