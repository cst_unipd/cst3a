function [ME]=Hollow_Explosive_Cylinder(i)

global MATERIAL_LIST

% INITIAL CONDITION TARGET
SHAPE_ID = 5;   %box=1, sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;
dimension_abc=[0.1,0,0.4]; % IF hollow cylinder (SHAPE_ID=5) [R,0, H] thickness=t;
thickness = 0.002; %[m]
position_cm=[0;0;0]; %[m]
Explosive_energy=500; % To be tuned

rot_axis=[0;1;0]; % rotates the ME around this axis of rotation alpha[rad]
alpha=pi/2; %[rad]


material_ID=1; % 1= Al, 2=TBD
mass= pi*(dimension_abc(1)^2*dimension_abc(3)-(dimension_abc(1)-thickness)^2*(dimension_abc(3)-thickness))*MATERIAL_LIST(material_ID).density;       %[kg]

%Default values
isBreakable=1; %1=true, 0=false
EMR= 0; % Energy to mass ratio
Hollow_seeds_distribution_ID = 4;%<<<<=== 0 random,1 Gaussian 3 McKnight
seeds_number = 5000;

quaternions=[cos(alpha/2),sin(alpha/2)*rot_axis(1),sin(alpha/2)*rot_axis(2),sin(alpha/2)*rot_axis(3)];

ME(i)=default_obj(); %Fills all fields with default values

%Fill ME non relevant fields
ME(i).object_ID = i;
ME(i).material_ID = material_ID;                   % 1=Aluminium, only one available
ME(i).GEOMETRY_DATA.shape_ID = SHAPE_ID; % 1=plate, 2=sphere, 4=cylinder
ME(i).GEOMETRY_DATA.dimensions = dimension_abc;
ME(i).GEOMETRY_DATA.thick = thickness;
ME(i).GEOMETRY_DATA.mass0 = mass;
ME(i).GEOMETRY_DATA.mass = ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0 = position_cm;
ME(i).DYNAMICS_DATA.cm_coord = ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
ME(i).DYNAMICS_DATA.quaternions=ME(i).DYNAMICS_INITIAL_DATA.quaternions0;
ME(i).FRAGMENTATION_DATA.threshold0 = EMR;
ME(i).FRAGMENTATION_DATA.threshold = ME(i).FRAGMENTATION_DATA.threshold0;
ME(i).FRAGMENTATION_DATA.breakup_flag = isBreakable;
ME(i).FRAGMENTATION_DATA.c_EXPL=Explosive_energy;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Hollow_seeds_distribution_ID;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = seeds_number;
