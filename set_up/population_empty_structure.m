%> @file  population_empty_structure.m
%> @brief Creating empty structures for populations
%> @author Cinzia Gaicomuzzo (cinzia.giacomuzzo@unipd.it)
%======================================================================
%> @brief Function used to create structures representing MEs, FRAGMENTS and Bubbles 
%>
%> @retval POP population structure (used for ME, FRAGMENTS, BUBBLE)
%======================================================================
function [POP]=population_empty_structure()
%> ----------------------------------------
%>
%> substructure containing population properties described in CST Variables
%> list document
%> Fields are intentially not initialized with default values for debugging
%> purposes
%> Fields description:
%> object_ID = Identification number of each object
%> material_ID = Index from MATERIAL_LIST identifying the material associated to the object
%> 
%> GEOMETRY_DATA
%> shape_ID = Index ref. from SHAPE list
%> dimensions = 3 elem. array setting object dimensions given as [a,b,c]
%> [a,b,c] will have different meaning depending on the shape, i.e.: 
%> box   [a, b, c] 
%> sphere [radius, 0, 0]
%> cylinder [radius,0, height]
%> ...
%> thick = Object thickness; Thickness must be set in the case of plate and 3D hollow shapes. 
%> mass0 = Object mass at time 0
%> mass = Object mass. Object mass will be updated during the simulation at each collision by the ME BREAKUP algorithm
%> A_M_ratio = Area-to-mass ratio
%> c_hull = Convex hull [3xN]  matrix
%> 
%> DYNAMICS_INITIAL_DATA
%> cm_coord0 = 3 elem. array setting object center of mass coordinates at time 0  
%> vel0 = 3 elem. array setting object center of mass velocity wrt x, y, z axes at time 0 
%> quaternions0 = 4 elem. array setting object quaternions at time 0 [q00-real term,q10,q20,q30]
%> w0 = 3 elem. array setting object center of mass angular velocity wrt x, y, z axes at time 0. Attitude is not implemented but this variable could be used for a future development. Data should be given as [wx0, wy0, wz0]
%> 
%> DYNAMICS_DATA
%> cm_coord = 3 elem. array setting object center of mass coordinates
%> vel = 3 elem. array setting object center of mass velocity wrt x, y, z axes
%> quaternions = 4 elem. array setting object quaternions 
%> w = 3 elem. array setting object center of mass angular velocity wrt x, y, z axes
%> virt_momentum = Virtual momentum employed when the object is totally fragmented but momentum is transmitted to the links [Qx Qy Qz]
%> 
%> FRAGMENTATION_DATA
%> failure_ID = Initial failure type from FRAGMENTATION_FAILURE list
%> threshold0 = Initial fixed threshold calculated from FAILURE_ID
%> threshold = Threshold updated after each collision for each object by ME BREAKUP algorithm
%> breakup_flag = Breakup algorithm activation [0 = no breakup; 1 = breakup calculation] 
%> ME_energy_transfer_coef = amount of deformation energy that lowers EMR threshold
%> cMLOSS = Loss momentum coefficient for no fragmentation
%> cELOSS = Loss energy coefficient during impact
%> c_EXPL = Coefficient for pressurized /explosive items (to be implemented)
%> seeds_distribution_ID = ID of the seed distribution associated to the object
%> seeds_distribution_param1 = param 1 of the selected seed distribution. For a random seeds distribution, param 1 is the maximum number of random seeds
%> seeds_distribution_param2 = param 2 of the selected seed distribution. For a random seeds distribution, param2 is the maximum number of loops for  the generation of random seeds
%> param_add1 = TBD
%> param_add2 = TBD


GEOMETRY_DATA_s=struct('shape_ID',{},'dimensions',{},'thick',{},...
	'mass0',{},'mass',{},'A_M_ratio',{},'c_hull',{});

DYNAMICS_INITIAL_DATA_s=struct('cm_coord0',{},'vel0',{},...
	'quaternions0',{}, 'w0',{},'v_exp0',{});

DYNAMICS_DATA_s=struct('cm_coord',{},'vel',{},...
	'quaternions',{},'w',{},'v_exp',{}, 'virt_momentum', {});
FRAGMENTATION_DATA_s=struct('failure_ID',{},'threshold0',{},'threshold',{},...
    'breakup_flag',{}, 'ME_energy_transfer_coef',{}, 'cMLOSS', {}, 'cELOSS', {}, 'c_EXPL',{},...
    'seeds_distribution_ID', {}, 'seeds_distribution_param1' , {},'seeds_distribution_param2', {}, 'param_add1', {}, 'param_add2', {});

POP=struct('object_ID',{},'object_ID_index',{},'material_ID',{},'GEOMETRY_DATA',GEOMETRY_DATA_s,...
	'DYNAMICS_INITIAL_DATA', DYNAMICS_INITIAL_DATA_s,'DYNAMICS_DATA', DYNAMICS_DATA_s,'FRAGMENTATION_DATA', FRAGMENTATION_DATA_s);