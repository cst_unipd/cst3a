function [ME]=generate_ME_target_simple_plate(i,jj,kk,)

% INITIAL CONDITION TARGET
SHAPE_ID = 1;   %box=1, sphere=2, hollow_sphere=3, cylinder=4, hollow cylinder =5;
dimension_abc=[0.2,0.2,0.001]; % For box/plate (SHAPE_ID=1) [L1,L2,L3]
position_cm=[0;0;0]; %[m]

global MATERIAL_LIST
material_ID=1; % 1= Al, 2=TBD
mass= dimension_abc(1)*dimension_abc(2)*dimension_abc(3)*MATERIAL_LIST(material_ID).density;       %[kg]

%Default values
isBreakable=1; %1=true, 0 =false
EMR= 0; % Energy to mass ratio to trigger complete fragmentation
Type_of_seeds_distribution = 1;%<<<<=== 0 random,1 Gaussian 4 for Hollows
number_of_fragements = 5000;


ME(i)=default_obj(); %Fills all fields with default values
%Refresh the ME
ME(i).object_ID=i;
ME(i).material_ID=material_ID;                
ME(i).GEOMETRY_DATA.shape_ID=SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=dimension_abc;
ME(i).GEOMETRY_DATA.mass0=mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragements;

