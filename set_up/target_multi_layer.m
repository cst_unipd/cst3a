function [ME]=target_multi_layer(i)

%Quick set up 
multi_layer_XYdim=[0.2,0.2];
layer1_t=0.001;
layer2_t=0.004;
layer3_t=0.005;

layer1_position= [0;0;0];

%In depth settings
layer1_dimensions=[multi_layer_XYdim,layer1_t];
layer2_dimensions=[multi_layer_XYdim,layer2_t];
layer3_dimensions=[multi_layer_XYdim,layer3_t];

layer2_position=layer1_position+[0;0;1]*(layer1_t/2+layer2_t/2)*2; %[m]
layer3_position=layer2_position+[0;0;1]*(layer2_t/2+layer3_t/2)*2; %[m]


Al_material_ID=1; % 1= Al, 2=TBD
global MATERIAL_LIST
layer1_mass= layer1_dimensions(1)*layer1_dimensions(2)*layer1_dimensions(3)*MATERIAL_LIST(Al_material_ID).density;  
layer2_mass= layer2_dimensions(1)*layer2_dimensions(2)*layer2_dimensions(3)*MATERIAL_LIST(Al_material_ID).density;       %[kg]
layer3_mass= layer3_dimensions(1)*layer3_dimensions(2)*layer3_dimensions(3)*MATERIAL_LIST(Al_material_ID).density;       %[kg]

isBreakable=1;
EMR= 0; % Energy to mass ratio to trigger complete fragmentation
Type_of_seeds_distribution = 1;%<<<<=== 0 random, 4 for Hollows
number_of_fragements = 4000;
Box_SHAPE_ID = 1; 
ME(i)=default_obj(); %Fills all fields with default values


%Layer1
ME(i).object_ID=i;           
ME(i).GEOMETRY_DATA.shape_ID=Box_SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=layer1_dimensions;
ME(i).GEOMETRY_DATA.mass0=layer1_mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=layer1_position;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragements;
ME(i).material_ID=Al_material_ID;   

i=i+1;
ME(i)=default_obj();
%Layer2
ME(i).object_ID=i;      
ME(i).GEOMETRY_DATA.shape_ID=Box_SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=layer2_dimensions;
ME(i).GEOMETRY_DATA.mass0=layer2_mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=layer2_position;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragements;
ME(i).material_ID=Al_material_ID;

i=i+1;
ME(i)=default_obj();
%Layer3
ME(i).object_ID=i;      
ME(i).GEOMETRY_DATA.shape_ID=Box_SHAPE_ID; 
ME(i).GEOMETRY_DATA.dimensions=layer3_dimensions;
ME(i).GEOMETRY_DATA.mass0=layer3_mass;
ME(i).GEOMETRY_DATA.mass=ME(i).GEOMETRY_DATA.mass0;
ME(i).DYNAMICS_INITIAL_DATA.cm_coord0=layer3_position;
ME(i).DYNAMICS_DATA.cm_coord=ME(i).DYNAMICS_INITIAL_DATA.cm_coord0;
ME(i).FRAGMENTATION_DATA.threshold0=EMR;
ME(i).FRAGMENTATION_DATA.threshold=EMR;
ME(i).FRAGMENTATION_DATA.breakup_flag=isBreakable;
ME(i).FRAGMENTATION_DATA.seeds_distribution_ID = Type_of_seeds_distribution;
ME(i).FRAGMENTATION_DATA.seeds_distribution_param1 = number_of_fragements;
ME(i).material_ID=Al_material_ID;  
