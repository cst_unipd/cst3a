function  [ME,FRAGMENTS,BUBBLE]=link_generate_Lan(ii,jj,kk,ME,FRAGMENTS,BUBBLE)
% link_generate_user: links connecting two MEs have to be set here

global link_data;
global link_data0;

%% LINK INIATIALIZATION
% 
% con = {[1 2],[1 3],[1 4],[1 5],[1 29],[1 30],[1 31],[1 32],[1 41],...
%     [1 42],[1 43],[1 44],[1 45],[1 46],[1 47],[1 48],[1 49],[1 53],...
%     [1 57],[1 61],[6 7],[6 8],[6 9],[6 10],[6 11],[6 12],[6 13],...
%     [6 14],[6 15],[6 16],[6 17],[6 18],[6 19],[6 20],[6 21],[6 22],...
%     [6 23],[6 24],[6 25],[6 26],[6 27],[6 28],[6 29],[6 30],[6 31],...
%     [6 32],[6 33],[6 34],[6 35],[6 36],[6 37],[6 38],[6 39],[6 40],...
%     [6 41],[6 42],[6 43],[6 44],[6 45],[6 46],[6 47],[6 48],[6 49],...
%     [6 53],[6 57],[6 61],[27 35],[27 53],[27 65],[28 39],[28 61],...
%     [28 65],[29 43],[29 53],[30 47],[30 61],[31 45],[31 57],[32 41],...
%     [32 49],[33 34],[33 40],[33 65],[34 35],[34 65],[35 38],[35 65],...
%     [36 37],[36 39],[36 65],[37 38],[37 65],[38 65],[39 40],[39 65],...
%     [40 65],[41 42],[41 48],[42 43],[43 46],[44 45],[44 47],[45 46],...
%     [47 48],[49 50],[49 51],[49 52],[49 53],[49 61],[49 65],[53 54],...
%     [53 55],[53 56],[53 57],[53 65],[57 58],[57 59],[57 60],[57 61],...
%     [57 65],[61 62],[61 63],[61 64],[61 65]};        % Pairs of MEs connected by each link
% 
% for i=1:length(con)
%     
%     ID{i} = i;                          % Identification number of the link 
%     mat{i} = 1;                         % Material type for each link: 1 = Al, 2 = steel,
%     geom{i} = 1;                        % Geometrical type of each link: 1 = dot, 2 = line, 3 = surface
%     type{i} = 1;                        % Element type of each link: 1 = bolted, 2 = welded,
%     val_fail{i} = 1;                    % Type of faiulre of the link: 1 = max elongation, 2 = max strain,
%     val_k{i} = eye(6);                  % Inizialization of the matrix for stiffness response
%     val_c{i} = eye(6);                  % Inizialization of the matrix for viscous response
%     val_rest{i} = ones(1,6);            % Inizialization of the equilibrium configuration
%     val_state{i} = 1;                   % Inizialization of the state of the link: 1 = intact, 0 = broken
%     
% end
% 
% % Field Names
% 
% f_id    = 'id';
% f_con   = 'ME_connect';   % Field of the pair of ME connected by the link
% f_mat   = 'material_ID';  % 1=Al, 2=steel, 
% f_geom  = 'geometry';
% f_type  = 'type_ID';      %1 = beam, 2 = surface...
% f_fail  = 'failure_ID';  % Type of faiulre (1=max elongation, 2= max strain...)
% f_k     = 'k_mat';       % 6x6 matrix for stiffness response
% f_c     = 'c_mat';       % 6x6 matrix for viscous response 
% f_rest  = 'rest';     % Equilibrium configuration, when no forces are exchanged
% f_state = 'state';   % 1= intact, 0=broken
% 
% link_data = struct(f_id,ID,f_con,con,f_mat,mat,f_geom,geom,f_type,type,f_fail,val_fail,f_state,val_state,f_k,val_k,f_c,val_c,f_rest,val_rest);
% 
% %Completes the link array with the geometries
load('link_Lan1.mat','link_data')
% set_link_properties();
link_data0=link_data;
% save('link_Lan1.mat','link_data')

end

