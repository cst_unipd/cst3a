% matrix with 10 columns:
% 1:3 position_cm
% 4:6 dimensions_abc (r 0 h)
% 7:9 rotation axis
% 10 alpha rotation
% 11:13 rotation axis 2
% 14 alpha rotation 2
% NB: se cambi l'ordine delle box (le righe), si DEVONO cambiare i link!)
grad2rad=pi/180;
plate_thickness=0.002; %[m]
data_lan=[ 0      0     -0.2+plate_thickness/2       0.4   0.4   plate_thickness 1 0 0  0           1 0 0  0;... %bottom plate  % 2 3 4 5 29 30 31 32 41 42 43 44 45 46 47 48 49 53 57 61
          -0.087 -0.109 -0.2+plate_thickness+0.040/2 0.077 0.055 0.040           0 0 1  45*grad2rad 1 0 0  0;... %box 1 on bottom plate
          -0.087  0.109 -0.2+plate_thickness+0.040/2 0.077 0.055 0.040           0 0 1 -45*grad2rad 1 0 0  0;... %box 2 on bottom plate
           0.087  0.109 -0.2+plate_thickness+0.040/2 0.077 0.055 0.040           0 0 1  45*grad2rad 1 0 0  0;... %box 3 on bottom plate
           0.087 -0.109 -0.2+plate_thickness+0.040/2 0.077 0.055 0.040           0 0 1 -45*grad2rad 1 0 0  0;... %box 4 on bottom plate % (5)
           ...
           0      0      0                           0.4   0.4   plate_thickness 1 0 0  0           1 0 0  0;... %mid plate % 7 8 9 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35 36 37 38 39 40 41 42 43 44 45 46 47 48 49 53 57 61
          -0.130 -0.070  plate_thickness/2+0.058/2   0.054 0.104 0.058           0 0 1  0           1 0 0  0;... %box 1 on mid plate (top)
          -0.130  0.070  plate_thickness/2+0.058/2   0.054 0.104 0.058           0 0 1  0           1 0 0  0;... %box 1 on mid plate (top mirror)
          -0.130 -0.070 -plate_thickness/2-0.058/2   0.054 0.104 0.058           0 0 1  0           1 0 0  0;... %box 1 on mid plate (bot)
          -0.130  0.070 -plate_thickness/2-0.058/2   0.054 0.104 0.058           0 0 1  0           1 0 0  0;... %box 1 on mid plate (bot mirror)
          -0.040 -0.130  plate_thickness/2+0.048/2   0.076 0.060 0.048           1 0 0  0           1 0 0  0;... %box 2 on mid plate (top)
          -0.040  0.130  plate_thickness/2+0.048/2   0.076 0.060 0.048           1 0 0  0           1 0 0  0;... %box 2 on mid plate (top mirror) 
          -0.040 -0.130 -plate_thickness/2-0.048/2   0.076 0.060 0.048           1 0 0  0           1 0 0  0;... %box 2 on mid plate (bot)
          -0.040  0.130 -plate_thickness/2-0.048/2   0.076 0.060 0.048           1 0 0  0           1 0 0  0;... %box 2 on mid plate (bot mirror)            
           0.070 -0.130  plate_thickness/2+0.046/2   0.048 0.074 0.046           0 0 1  0           1 0 0  0;... %box 3 on mid plate (top)
           0.070  0.130  plate_thickness/2+0.046/2   0.048 0.074 0.046           0 0 1  0           1 0 0  0;... %box 3 on mid plate (top mirror)
           0.070 -0.130 -plate_thickness/2-0.046/2   0.048 0.074 0.046           0 0 1  0           1 0 0  0;... %box 3 on mid plate (bot)
           0.070  0.130 -plate_thickness/2-0.046/2   0.048 0.074 0.046           0 0 1  0           1 0 0  0;... %box 3 on mid plate (bot mirror)
           0.150 -0.130  plate_thickness/2+0.045/2   0.046 0.076 0.045           0 0 1  0           1 0 0  0;... %box 4 on mid plate (top) 
           0.150  0.130  plate_thickness/2+0.045/2   0.046 0.076 0.045           0 0 1  0           1 0 0  0;... %box 4 on mid plate (top mirror) 
           0.150 -0.130 -plate_thickness/2-0.045/2   0.046 0.076 0.045           0 0 1  0           1 0 0  0;... %box 4 on mid plate (bot) 
           0.150  0.130 -plate_thickness/2-0.045/2   0.046 0.076 0.045           0 0 1  0           1 0 0  0;... %box 4 on mid plate (bot mirror) 
           0.150 -0.050  plate_thickness/2+0.044/2   0.045 0.071 0.044           0 0 1  0           1 0 0  0;... %box 5 on mid plate (top) 
           0.150  0.050  plate_thickness/2+0.044/2   0.045 0.071 0.044           0 0 1  0           1 0 0  0;... %box 5 on mid plate (top mirror) 
           0.150 -0.050 -plate_thickness/2-0.044/2   0.045 0.071 0.044           0 0 1  0           1 0 0  0;... %box 5 on mid plate (bot) 
           0.150  0.050 -plate_thickness/2-0.044/2   0.045 0.071 0.044           0 0 1  0           1 0 0  0;... %box 5 on mid plate (bot mirror) % (26)
           ...
           0.135  0      plate_thickness/2+0.197/2   0.130 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %vertical plate on mid plate (top) % 35 53 65 
          -0.135  0      plate_thickness/2+0.197/2   0.130 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %vertical plate on mid plate (top mirror) % 39 61 65
           0.135  0     -plate_thickness/2-0.197/2   0.130 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %vertical plate on mid plate (bot) % 43 53
          -0.135  0     -plate_thickness/2-0.197/2   0.130 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %vertical plate on mid plate (bot mirror) % 47 61
           0      0.135 -plate_thickness/2-0.197/2   0.130 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %vertical plate 2 on mid plate (bot) % 45 57 
           0     -0.135 -plate_thickness/2-0.197/2   0.130 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %vertical plate 2 on mid plate (bot mirror) (32) % 41 49
           ...
           0     -0.070  plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %octagon plate 1 on mid plate (top) % 34 40 65
           0.049 -0.049  plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1 -45*grad2rad;... %octagon plate 2 on mid plate (top) % 35 65
           0.070  0      plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %octagon plate 3 on mid plate (top) % 38 65
          -0.049  0.049  plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1 -45*grad2rad;... %octagon plate 4 on mid plate (top) % 37 39 65
           0      0.070  plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %octagon plate 5 on mid plate (top) % 38 65
           0.049  0.049  plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  45*grad2rad;... %octagon plate 6 on mid plate (top) % 65
          -0.070  0      plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %octagon plate 7 on mid plate (top) % 40 65
          -0.049 -0.049  plate_thickness/2+0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  45*grad2rad;... %octagon plate 8 on mid plate (top) % 65
           0     -0.070 -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %octagon plate 1 on mid plate (bot) % 42 48
           0.049 -0.049 -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1 -45*grad2rad;... %octagon plate 2 on mid plate (bot) % 43
           0.070  0     -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %octagon plate 3 on mid plate (bot) % 44 
          -0.049  0.049 -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1 -45*grad2rad;... %octagon plate 4 on mid plate (bot) % 45
           0      0.070 -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %octagon plate 5 on mid plate (bot) % 46
           0.049  0.049 -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  45*grad2rad;... %octagon plate 6 on mid plate (bot) % 47
          -0.070  0     -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  90*grad2rad;... %octagon plate 7 on mid plate (bot) % 48
          -0.049 -0.049 -plate_thickness/2-0.197/2   0.058 0.197 plate_thickness 1 0 0  90*grad2rad 0 0 1  45*grad2rad;... %octagon plate 8 on mid plate (bot) % (48)
           ...
           0     -0.2-plate_thickness/2          0         0.4   0.4   plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %lateral plate 1 %  50 51 52 53 61 65
           0.119 -0.2+plate_thickness/2+0.049/2  0.110     0.070 0.060 0.049           1 0 0  90*grad2rad 1 0 0  0;... %box 1 on lateral plate 1 % 
          -0.091 -0.2+plate_thickness/2+0.062/2  0.110     0.110 0.050 0.062           1 0 0  90*grad2rad 1 0 0  0;... %box 2 on lateral plate 1 % 
          -0.091 -0.2+plate_thickness/2+0.046/2 -0.120     0.060 0.055 0.046           1 0 0  90*grad2rad 1 0 0  0;... %box 3 on lateral plate 1 % (52)
           ...
           0.2+plate_thickness/2          0      0     0.4   0.4   plate_thickness 0 1 0  90*grad2rad 1 0 0  0;... %lateral plate 2 % 54 55 56 57 65
           0.2-plate_thickness/2-0.049/2  0.119  0.110 0.070 0.060 0.049           0 0 1  90*grad2rad 0 1 0  90*grad2rad;... %box 1 on lateral plate 2 % 
           0.2-plate_thickness/2-0.062/2 -0.091  0.110 0.110 0.050 0.062           0 0 1  90*grad2rad 0 1 0  90*grad2rad;... %box 2 on lateral plate 2 % 
           0.2-plate_thickness/2-0.046/2 -0.091 -0.120 0.060 0.055 0.046           0 0 1  90*grad2rad 0 1 0  90*grad2rad;... %box 3 on lateral plate 2 % (56)
           ...
           0      0.2+plate_thickness/2    0         0.4   0.4   plate_thickness 1 0 0  90*grad2rad 1 0 0  0;... %lateral plate 3 % 58 59 60 61 65 
           -0.119  0.2-plate_thickness/2-0.062/2  0.110    0.070 0.060 0.049     1 0 0  90*grad2rad           1 0 0  0;... %box 1 on lateral plate 3 %  
            0.091  0.2-plate_thickness/2-0.049/2  0.110    0.110 0.050 0.062     1 0 0  90*grad2rad           1 0 0  0;... %box 2 on lateral plate 3 %  
           0.091  0.2-plate_thickness/2-0.046/2 -0.120     0.060 0.055 0.046     1 0 0  90*grad2rad           1 0 0  0;... %box 3 on lateral plate 3 %  (60)
           ...
          -0.2-plate_thickness/2          0      0     0.4   0.4   plate_thickness 0 1 0  90*grad2rad 1 0 0  0;... %lateral plate 4 % 62 63 64 65
          -0.2+plate_thickness/2+0.062/2 -0.119  0.110 0.070 0.060 0.049           0 1 0  90*grad2rad 1 0 0  90*grad2rad;... %box 1 on lateral plate 4 %  
          -0.2+plate_thickness/2+0.049/2  0.091  0.110 0.110 0.050 0.062           0 1 0  90*grad2rad 1 0 0  90*grad2rad;... %box 2 on lateral plate 4 %  
          -0.2+plate_thickness/2+0.046/2  0.091 -0.120 0.060 0.055 0.046           0 1 0  90*grad2rad 1 0 0  90*grad2rad;... %box 3 on lateral plate 4 %  (64)
           ...
           0 0 0.2+plate_thickness/2                   0.4   0.4   plate_thickness 1 0 0  0           1 0 0  0;... %top plate %  (65)
         ]; 