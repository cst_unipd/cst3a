function [ME,FRAGMENTS,BUBBLE]=generate_FRAGMENTS_6(ii,j,kk,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

dfo=0.5; %[m] distance form origin
diameter=0.006; %[m]
vel_s=4000;
%X
FRAGMENTS(j).DYNAMICS_INITIAL_DATA.cm_coord0=[+dfo;0;0];
FRAGMENTS(j).DYNAMICS_INITIAL_DATA.vel0=[-vel_s;0;0];
FRAGMENTS(j+1).DYNAMICS_INITIAL_DATA.cm_coord0=[-dfo;0;0];
FRAGMENTS(j+1).DYNAMICS_INITIAL_DATA.vel0=[+vel_s;0;0];
%Y
FRAGMENTS(j+2).DYNAMICS_INITIAL_DATA.cm_coord0=[0;+dfo;0];
FRAGMENTS(j+2).DYNAMICS_INITIAL_DATA.vel0=[0;-vel_s;0];
FRAGMENTS(j+3).DYNAMICS_INITIAL_DATA.cm_coord0=[0;-dfo;0];
FRAGMENTS(j+3).DYNAMICS_INITIAL_DATA.vel0=[0;+vel_s;0];
%Z
FRAGMENTS(j+4).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;+dfo];
FRAGMENTS(j+4).DYNAMICS_INITIAL_DATA.vel0=[0;0;-vel_s];
FRAGMENTS(j+5).DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;-dfo];
FRAGMENTS(j+5).DYNAMICS_INITIAL_DATA.vel0=[0;0;+vel_s];
% INITIAL CONDITION TARGET
for i=j:j+5
    dimension_abc=[diameter,0,0]; % IF box/plate (SHAPE_ID=1) [L1,L2,0], thickness=t;
    thickness = 0; %[m]
    position_cm=[0;0;0]; %[m]
    Initial_vel=[0;0;-2000]; %[m/s]velocity of every ME
    quaternions=[0,1,0,0];
    %Default values
    material_ID=1; % 1= Al, 2=TBD
    mass= 4/3*pi*dimension_abc(1)^3*MATERIAL_LIST(material_ID).density;       %[kg]
    FAIL_TYPE=1; % 1=strian 2= TBD...
    EMR= 0; % Energy to mass ratio
    w=[0;0;0];
    DEFAULT_seeds_distribution_ID = 0;%<<<<=== 0 random, 3 McKnight
    DEFAULT_seeds_distribution_param1 = 100;
    DEFAULT_seeds_distribution_param2 = 5;
    %ME definition
    FRAGMENTS(i).object_ID=i;
    FRAGMENTS(i).material_ID=material_ID;                   % 1=Aluminium, only one available
    FRAGMENTS(i).GEOMETRY_DATA.shape_ID=0; % 1=plate, 2=sphere, 4=cylinder
    FRAGMENTS(i).GEOMETRY_DATA.dimensions=dimension_abc;
    FRAGMENTS(i).GEOMETRY_DATA.thick=thickness;
    FRAGMENTS(i).GEOMETRY_DATA.mass0=mass;
    FRAGMENTS(i).GEOMETRY_DATA.mass=mass;
    %FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0=position_cm;
    FRAGMENTS(i).DYNAMICS_DATA.cm_coord=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.cm_coord0;
    FRAGMENTS(i).DYNAMICS_INITIAL_DATA.quaternions0=quaternions;
    %FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0=Initial_vel;
    FRAGMENTS(i).DYNAMICS_DATA.vel=FRAGMENTS(i).DYNAMICS_INITIAL_DATA.vel0;
    FRAGMENTS(i).DYNAMICS_INITIAL_DATA.w0=w;
    FRAGMENTS(i).DYNAMICS_DATA.quaternions=quaternions;
    FRAGMENTS(i).DYNAMICS_DATA.w=w;
    FRAGMENTS(i).DYNAMICS_DATA.virt_momentum=[0,0,0]';
    FRAGMENTS(i).GEOMETRY_DATA.A_M_ratio=0;
    FRAGMENTS(i).GEOMETRY_DATA.c_hull=[0, 0, 0];
    FRAGMENTS(i).FRAGMENTATION_DATA.threshold0=EMR;
    FRAGMENTS(i).FRAGMENTATION_DATA.threshold=EMR;
    FRAGMENTS(i).FRAGMENTATION_DATA.failure_ID=FAIL_TYPE;
    FRAGMENTS(i).FRAGMENTATION_DATA.breakup_flag=1;
    FRAGMENTS(i).FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
    FRAGMENTS(i).FRAGMENTATION_DATA.cMLOSS=0.2;
    FRAGMENTS(i).FRAGMENTATION_DATA.cELOSS=0.1;
    FRAGMENTS(i).FRAGMENTATION_DATA.c_EXPL=0;
    FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_ID = DEFAULT_seeds_distribution_ID;
    FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param1 = DEFAULT_seeds_distribution_param1;
    FRAGMENTS(i).FRAGMENTATION_DATA.seeds_distribution_param2 = DEFAULT_seeds_distribution_param2;
    FRAGMENTS(i).FRAGMENTATION_DATA.param_add1=0;
    FRAGMENTS(i).FRAGMENTATION_DATA.param_add2=0;
end


