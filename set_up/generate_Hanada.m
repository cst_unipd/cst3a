function [ME,FRAGMENTS,BUBBLE]=generate_Hanada(ii,jj,kk,ME,FRAGMENTS,BUBBLE)

global MATERIAL_LIST

Data_Hanada % matrix with cm, dimensions and orientation of boxes
bb=1;
box=1:9;
mat=[9 8 8 8 9 9 9 9 9];
for name = ii : ii + length(box)-1
    box_index = box(bb);
    position_cm = data_hanada(box_index,1:3)*1.02;%*1.5;%
    dimension_abc = data_hanada(box_index,4:6)*0.96; %*0.6;%
    rot_axis1 = data_hanada(box_index,7:9);
    alpha1 = data_hanada(box_index,10);
    quat1=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
    rot_axis2 = data_hanada(box_index,11:13);
    alpha2 = data_hanada(box_index,14);
    quat2=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat1,quat2);
    else
        quaternions=quatxquat(quat1,quat2);
    end
    ME = generate_box_Hanada(name,dimension_abc,position_cm,quaternions,ME);
    ME(name).object_ID = box_index;
    ME(name).material_ID = mat(ii);
    ME(name).GEOMETRY_DATA.mass0 = dimension_abc(1)*dimension_abc(2)*dimension_abc(3)*MATERIAL_LIST(ME(name).material_ID).density;
    ME(name).GEOMETRY_DATA.mass = dimension_abc(1)*dimension_abc(2)*dimension_abc(3)*MATERIAL_LIST(ME(name).material_ID).density;
    bb = bb+1;
    clear position_cm dimension_abc rot_axis alpha quaternions
end

bb = 1;
cyl = length(box)+1:length(data_hanada);

for name = ii + length(box) : length(data_hanada)
    cyl_index = cyl(bb);
    position_cm = data_hanada(cyl_index,1:3)*1.02;%*1.5;%
    dimension_abc = data_hanada(cyl_index,4:6)*0.96; %*0.6;%
    rot_axis1 = data_hanada(cyl_index,7:9);
    alpha1 = data_hanada(cyl_index,10);
    quat1=[cos(alpha1/2), sin(alpha1/2)*rot_axis1(1), sin(alpha1/2)*rot_axis1(2), sin(alpha1/2)*rot_axis1(3)];
    rot_axis2 = data_hanada(cyl_index,11:13);
    alpha2 = data_hanada(cyl_index,14);
    quat2=[cos(alpha2/2), sin(alpha2/2)*rot_axis2(1), sin(alpha2/2)*rot_axis2(2), sin(alpha2/2)*rot_axis2(3)];
    if exist(fullfile(matlabroot,'toolbox','aero','aero','quatmultiply.m'),'file')
        quaternions=quatmultiply(quat1,quat2);
    else
        quaternions=quatxquat(quat1,quat2);
    end
    ME = generate_cyl_Hanada(name,dimension_abc,position_cm,quaternions,ME);
    ME(name).object_ID = cyl_index;
    bb = bb+1;
    clear position_cm dimension_abc rot_axis alpha quaternions
end
