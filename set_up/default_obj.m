%> @file default_obj.m
%> @brief Initialization of a standard object( may be used for ME,Fragments or Bubble)
%> Copyright (c) 2018 CISAS - UNIVERSITY OF PADOVA
%>======================================================================
function [OBJ]=default_obj()
%Initialization of a standard object( may be used for ME,Fragments or Bubble)

OBJ.object_ID=0;
OBJ.object_ID_index=0;
OBJ.material_ID=1;                   % 1=Aluminium, only one available
OBJ.GEOMETRY_DATA.shape_ID=2;        
% 0=Fragment, 1=box, 2=sphere, 3=hollow sphere, 4=cylinder,5=Hollow cylinder
OBJ.GEOMETRY_DATA.dimensions=[0.005,0,0];
OBJ.GEOMETRY_DATA.thick=0;
OBJ.GEOMETRY_DATA.mass0=0.0015;
OBJ.GEOMETRY_DATA.mass=OBJ.GEOMETRY_DATA.mass0;
OBJ.DYNAMICS_INITIAL_DATA.cm_coord0=[0;0;0];
OBJ.DYNAMICS_DATA.cm_coord=OBJ.DYNAMICS_INITIAL_DATA.cm_coord0;
OBJ.DYNAMICS_INITIAL_DATA.quaternions0=[1,0,0,0];
OBJ.DYNAMICS_DATA.quaternions=OBJ.DYNAMICS_INITIAL_DATA.quaternions0;
OBJ.DYNAMICS_INITIAL_DATA.vel0=[0;0;0]; %m/s
OBJ.DYNAMICS_DATA.vel=OBJ.DYNAMICS_INITIAL_DATA.vel0;
OBJ.DYNAMICS_INITIAL_DATA.w0=[0;0;0];
OBJ.DYNAMICS_DATA.w=OBJ.DYNAMICS_INITIAL_DATA.w0;
OBJ.DYNAMICS_DATA.virt_momentum=[0;0;0];
OBJ.GEOMETRY_DATA.A_M_ratio=0;
OBJ.GEOMETRY_DATA.c_hull=[0, 0, 0];
OBJ.FRAGMENTATION_DATA.threshold0=0;
OBJ.FRAGMENTATION_DATA.threshold=OBJ.FRAGMENTATION_DATA.threshold0;
OBJ.FRAGMENTATION_DATA.failure_ID=1; % 1=strian 2= TBD...
OBJ.FRAGMENTATION_DATA.breakup_flag=1;
OBJ.FRAGMENTATION_DATA.ME_energy_transfer_coef=0.001;
OBJ.FRAGMENTATION_DATA.cMLOSS=0.2;
OBJ.FRAGMENTATION_DATA.cELOSS=500;
OBJ.FRAGMENTATION_DATA.c_EXPL=0;
OBJ.FRAGMENTATION_DATA.seeds_distribution_ID = 0;%<<<<=== 0 random, 1 Gaussian, 4 only for hollow
OBJ.FRAGMENTATION_DATA.seeds_distribution_param1 = 100; % number of seeds
OBJ.FRAGMENTATION_DATA.seeds_distribution_param2 = 5; % Advanced settings
OBJ.FRAGMENTATION_DATA.param_add1=0;
OBJ.FRAGMENTATION_DATA.param_add2=0;
