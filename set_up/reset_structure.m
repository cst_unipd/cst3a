%> @file  reset_structure.m
%> @brief Resetting structure by returning an empty array for each field
%> @author Cinzia Giacomuzzo (cinzia.giacomuzzo@unipd.it)
%======================================================================
%> @brief Function used to cancel values from input structure fields
%>
%>
%> @param struct_s Input structure to be reset
%>
%> @retval struct_s_empty Output structure with empty fields
%>
%======================================================================


function struct_s_empty=reset_structure(struct_s)
n=length(struct_s);
if(n>0)
    for(i=1:n)
        if(isempty(struct_s)==0)
            f=fields(struct_s(i));
            for k=1:numel(f)
                struct_s(i).(f{k})=[];
            end
        end
    end
end
struct_s_empty=struct_s;
