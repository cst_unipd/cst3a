%> @file  path_initialization.m
%> @brief CST path initialization

%======================================================================
%> @brief Function used to initialize the paths to the main CST subfolders
%>
%> This function initializes the following global variables:
%> - Tracking_folder
%> - Structural_Response_folder
%> - Structural_Response_folder
%> - Breakup_folder
%> - neper_folder
%> 
%> It initializes also the global variable hostname
%>
%======================================================================
function path_initialization()

% global variables
global hostname;
global Tracking_folder;
global Structural_Response_folder;
global Breakup_folder;
global set_up_folder;
global draw_subroutines_folder;
global neper_folder;
global cygwin64_folder;
global voropp_folder;
global voropp_interface_folder;
global External_libs_folder;
% add subfolders
Tracking_folder=fullfile('Tracking', filesep);
Structural_Response_folder=fullfile('Structural_Response', filesep);
Breakup_folder=fullfile('Breakup', filesep);
set_up_folder=fullfile('set_up', filesep);
draw_subroutines_folder=fullfile('draw_subroutines', filesep);
utils_folder = fullfile('utils',filesep);
input_check_folder = fullfile('Input_check',filesep);
GUI2Solver_Interface_folder=fullfile('GUI2Solver_Interface', filesep);
% Simulation_cases_folder=fullfile('Simulation_cases', filesep);

addpath(genpath(Tracking_folder));
addpath(genpath(Structural_Response_folder));
addpath(genpath(Breakup_folder));
addpath(genpath(set_up_folder));
addpath(genpath(draw_subroutines_folder));
addpath(genpath(utils_folder));
addpath(genpath(input_check_folder));
addpath(genpath(GUI2Solver_Interface_folder));
% addpath(genpath(Simulation_cases_folder));

[host_stdout,hostname]=system('hostname');
if(ispc)
    External_libs_folder=fullfile(pwd,'..','External_libs',filesep);
    if(strncmp(hostname,'fwfranale-01',12)==1)
%         External_libs_folder=fullfile(pwd,'External_libs',filesep);
        cygwin64_folder=fullfile('C:','cygwin64','bin',filesep);
        voropp_folder=fullfile('E:','Programmi','VORO046_win','usr','local','bin',filesep);
    else
        cygwin64_folder=fullfile(External_libs_folder,'CYGWIN64','bin',filesep);
        voropp_folder=fullfile('VOROPP','usr','local','bin',filesep);
    end
    addpath(genpath(fullfile(External_libs_folder,'BULLET_PHYSICS')));
    % addpath(genpath(fullfile(External_libs_folder,'VOROPP')));
    addpath(genpath(fullfile(External_libs_folder,'x86_64-5.4.0-release-win32-seh-rt_v5-rev0')),'-BEGIN');
end
% voropp_interface_folder
voropp_interface_folder = fullfile('Breakup','Fragmentation_algorithm','voropp_interface',filesep);


% set hostname:
% - lxssg-13 : Cinzia's pc with UNIX OS
% - SurfPro3-Andrea : Andrea's pc with WIN10 OS
% - fwfranale-01 : pc with WIN10 OS and cygwin
%hostname = char( getHostName( java.net.InetAddress.getLocalHost ) );

% set Neper folder
neper_folder = fullfile(pwd, 'Breakup','Fragmentation_algorithm','neper_files',filesep);

% matGeom path
matGeom_Path=fullfile('..','matGeom',filesep);

% setup matGeom
if ~strcmp(hostname,'SurfPro3-Andrea')
    addpath(matGeom_Path);
    run setupMatGeom.m
else
   setupMatGeom_file=fullfile('..','matGeom-master','matGeom','setupMatGeom.m');
   run(setupMatGeom_file)
end


end
