a=dir('E:\AO_8507Catastrophic\CST2_ESTEC\CST2_from_cinzia\Results\WhippleShield_*')
diary('summary_ME_WS.txt')
cd('Results');
for i=1:length(a)
    d=load(fullfile(a(i).name,'data','step_50.mat'));
    sim_name=a(i).name
    ME1_mass0=d.ME(1).GEOMETRY_DATA.mass0
    ME1_mass=d.ME(1).GEOMETRY_DATA.mass
    ME1_pos=d.ME(1).DYNAMICS_DATA.cm_coord
    ME1_vel=d.ME(1).DYNAMICS_DATA.vel
    ME2_mass0=d.ME(2).GEOMETRY_DATA.mass0
    ME2_mass=d.ME(2).GEOMETRY_DATA.mass
    ME2_pos=d.ME(2).DYNAMICS_DATA.cm_coord
    ME2_vel=d.ME(2).DYNAMICS_DATA.vel
    n_HOLES=length(d.HOLES)
    for j=1:n_HOLES
        HOLES_ID=d.HOLES(j).object_ID
        HOLES_ID_index=d.HOLES(j).object_ID_index
    end
end
cd ..
diary off
    