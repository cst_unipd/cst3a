%function A=risultati_Hill
clear all
close all
clc

% Table 5.
%         NASA/MSFC SDIF spherical projectile hypervelocity impact test data
% Index ?,dp (mm),?p (g/cm3),tt (mm),?t (g/cm3),dh (mm),V (km/s)

A=csvread('tableHILL.csv');

% dp/t
A(:,8)= A(:,4)./A(:,2);  % t/dp
A(:,9)= A(:,6)./A(:,2);  % dh/dt
A = sortrows(A,8);

A1=zeros(1,3);
count=1;
for i=1:length(A(:,1))
   if (A(i,8)>0.28 &&  A(i,8)<0.40) && A(i,3)<7.6 && A(i,2)<5
       
        A1(count,1)=A(i,8); %t/dp
        A1(count,2)=A(i,9); %dh/dp
        A1(count,3)=A(i,7); %v/dp
        
        A1(count,4)=A(i,2); % dp
        A1(count,5)=A(i,3); %?p (g/cm3)
        count=count+1;
   end
end


figure('Position', [138 113 766 811]/1.5)
plot(A1(:,3),A1(:,2),'k*');

hold on

dRIS=[2.60 2.56 3.38 4.10];
dp=[1.5 1.5 1.5 1.5];
v=[3434 4617 3398 4440];

plot(v(1:2)/1000,dRIS(1:2)./dp(1:2),'ro','MarkerFaceColor','r')


%% CURVE HILL

dp=1.5;
v=linspace(1.5,5)*1000;
t=0.5;
rho=1800;
ct=6300;

L=length(v);
dH1=zeros(1,L);
dH2=zeros(1,L);

for i=1:L
    d_p=dp;
    V=v(i);
    c_p=6300;
    c_t=ct;
    rhop=2700;
    rhot=rho;
    t_i=t;
    
    d_H1=2.947*d_p*(V/c_p)^0.055*(V/c_t)^0.339*(rhop/rhot)^0.028*(t_i/d_p)^0.414+d_p*0.342;
    dH1(i)=d_H1;
    d_H2=3.309*d_p*(V/c_p)^0.033*(V/c_t)^0.298*(rhop/rhot)^0.022*(t_i/d_p)^0.359;
    dH2(i)=d_H2;
end

hold on
plot(v/1000,dH1/dp,'.-b',v/1000,dH2/dp,'--b')
set(gca,'FontSize',13)
xlabel('Velocity, km/s')
ylabel('d_H_O_L_E / d_p')
title('Holes size for t/d=0.33')
legend('Exp. data,  NASA/MSFC SDIF','Exp. data, CISAS (SP)','Hill model #1','Hill model #2',...
    'Location','southeast')
