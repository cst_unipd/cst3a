v_pro=[3.01 3.09 4.14 4.06 6.62 6.75];
d_pro=[3.2 1.6 3.2 1.6 3.2 1.6];
d_cr=[7.76 3.85 9.25 4.65 12.52 6.18];

plot3(v_pro,d_pro,d_cr,'*r')
grid on
hold on
sf=fit([v_pro',d_pro'],d_cr','poly11','Robust','Bisquare')
plot(sf,[v_pro',d_pro'],d_cr')
