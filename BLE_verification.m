function BLE_verification(penetration_vect)

% 1 GPa = 145.03773773 ksi

sigmaSI=[70 150 240];
sigma = sigmaSI*0.1450377377;
figure()

for ccc=1:length(sigma)
    
    tw = 1.2e-1; % cm , wall
    ts = 1.2e-1; % cm , bumper
    sigma_w = sigma(ccc); % ksi
    
    alpha = 0;
    rho_p = 2790e-3; % g/cm^3
    rho_b = 2790e-3; % g/cm^3
    v = 0.5:0.1:10; % km/s
    S = 10;
    
    vn = v*cos(alpha);
    dp = zeros(1,length(v));
    % dp_3 = 10*power( (tw*(sigma_w/40)^0.5+ts) ./ ( 0.6*(cos(alpha))^(5/6)*rho_p^0.5*power(3/cos(alpha),2/3) ) ,(18/19));
    dp_3 = 10*power( (tw*(sigma_w/40)^0.5+ts) ./ ( 0.6*(cos(alpha))^(5/3)*rho_p^0.5*power(3/cos(alpha),2/3) ) ,(18/19));
    
    dp_7 = 10*3.918*tw^(2/3)*rho_p^(-1/3)*rho_b^(-1/9)*7^(-2/3)*S^(1/3)*(sigma_w/70)^(1/3);
    X_int = [3 7]/cos(alpha);
    Y_int = [dp_3 dp_7];
    for i=1:length(v)
        if vn(i)<=3 % km/s
            %         dp(i) = 10*power( (tw*(sigma_w/40)^0.5+ts) ./ ( 0.6*(cos(alpha))^(5/6)*rho_p^0.5*power(v(i),2/3) ) ,(18/19));
            dp(i) = 10*power( (tw*(sigma_w/40)^0.5+ts) ./ ( 0.6*(cos(alpha))^(5/3)*rho_p^0.5*power(v(i),2/3) ) ,(18/19));
        elseif vn(i)>=7 % km/s
            dp(i) = 10*3.919*tw^(2/3)*rho_p^(-1/3)*rho_b^(-1/9)*vn(i)^(-2/3)*S^(1/3)*(sigma_w/70)^(1/3);
        else
            dp(i) = interp1(X_int,Y_int,v(i));
        end
    end
    
    set(gcf,'color','w');
    h(ccc)=plot(v,dp,'-*'); hold on;
    string{ccc}=(['\sigma_s =' num2str(sigmaSI(ccc)) 'MPa']);
end

v_test = [2.1 3 4 4.8];
dp_cr_test = [1.667 1.29 1.706 2.038];
dp_test_inf = dp_cr_test - 0.3;
dp_test_sup = dp_cr_test + 1.2;
v_test_tot = [v_test v_test 6.5 8 9 10 6.5 8 9 10 6.5 8 9 2.2 3 2.2 3 4 4.8 10 9 2.2 3 4 10 6 8 10 6 8 5 5 7 7 5 3 3 2 4 5 9];
dp_test_tot = [dp_test_inf dp_test_sup  4 3.5 3.238 3 2 2 1.9 1.5 1.5 1.5 1.6 3 3 0.8 0.5 0.8 1 1 1.1 2 1.5 2 2 0.5 0.5 0.5 1 0.25 1 0.5 0.5 1 1.5 1.5 2 2 1.8 1.25 1.5];
test_num_tot = [51:1:97];
% % % penetration_vect = [0 0 0 0 0 0 1 1 1 1 1 1 1 1 1 0 0 0 0 1 1];
% % penetration_vect = [0 0 0 0 1 1 1 1 1 1 1 1 0 1 1 1 0 1 1 1 1]; % new
% % penetration_vect = [0 0 1 0 1 1 1 1 1 1 0 0 1 1 1 1 1 0 1 0 1]; % new CG 270618 senza precision 10% Andrea
% penetration_vect = [0 0 1 0 1 1 1 1 1 1 0 0 1 1 1 1 1 0 1 0 1]; % new CG 270718


for i=1:length(test_num_tot)
    if penetration_vect(i)==0
        plot(v_test_tot(i),dp_test_tot(i),'or');
        text(v_test_tot(i)+0.1,dp_test_tot(i)+0.0,num2str(test_num_tot(i)),'FontSize',14);
    elseif penetration_vect(i)==1
        plot(v_test_tot(i),dp_test_tot(i),'*k');
        text(v_test_tot(i)+0.1,dp_test_tot(i)+0.0,num2str(test_num_tot(i)),'FontSize',14);
    else
        disp(['Test no.', num2str(test_num_tot(i)),' not performed'])
    end
%     text(v_test_tot(i)+0.1,dp_test_tot(i)+0.0,num2str(test_num_tot(i)),'FontSize',14);
end
xlabel('v [km/s]');
ylabel('d_p [mm]');
ylim([0 5]);
set(gca,'FontSize',18);
grid on;
legend(h,string)
saveas(gcf,'BLE','png')